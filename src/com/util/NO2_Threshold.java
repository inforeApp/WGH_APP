package com.util;

import java.util.ArrayList;
import java.util.List;

public class NO2_Threshold {
	private static final double NO2_1H_ONE = 0.0; //二氧化氮污染物项目1小时平均浓度限值
	private static final double NO2_1H_TWO = 100.0;
	private static final double NO2_1H_THREE = 200.0;
	private static final double NO2_1H_FOUR = 700.0;
	private static final double NO2_1H_FIVE = 1200.0;
	private static final double NO2_1H_SIX = 2340.0;
	private static final double NO2_1H_SEVEN = 3090.0;
	private static final double NO2_1H_EIGHT = 3840.0;
	private static final double IAQI_NO2_ONE = 0.0;   //空气质量分指数平均浓度限值
	private static final double IAQI_NO2_TWO = 50.0;
	private static final double IAQI_NO2_THREE = 100.0;
	private static final double IAQI_NO2_FOUR = 150.0;
	private static final double IAQI_NO2_FIVE = 200.0;
	private static final double IAQI_NO2_SIX = 300.0;
	private static final double IAQI_NO2_SEVEN = 400.0;
	private static final double IAQI_NO2_EIGHT = 500.0;
	
	public static List<Double> getNO2_1H_Threshold (double n){
		 List<Double> NO2_threshlod = new ArrayList<Double>();
		if(n>=NO2_1H_ONE && n<=NO2_1H_TWO){
			NO2_threshlod.add(IAQI_NO2_TWO);
			NO2_threshlod.add(IAQI_NO2_ONE);
			NO2_threshlod.add(NO2_1H_TWO);
			NO2_threshlod.add(NO2_1H_ONE);
		}else if(n>NO2_1H_TWO && n<=NO2_1H_THREE){
			NO2_threshlod.add(IAQI_NO2_THREE);
			NO2_threshlod.add(IAQI_NO2_TWO);
			NO2_threshlod.add(NO2_1H_THREE);
			NO2_threshlod.add(NO2_1H_TWO);
		}else if(n>NO2_1H_THREE && n<=NO2_1H_FOUR){
			NO2_threshlod.add(IAQI_NO2_FOUR);
			NO2_threshlod.add(IAQI_NO2_THREE);
			NO2_threshlod.add(NO2_1H_FOUR);
			NO2_threshlod.add(NO2_1H_THREE);
		}else if(n>NO2_1H_FOUR && n<=NO2_1H_FIVE){
			NO2_threshlod.add(IAQI_NO2_FIVE);
			NO2_threshlod.add(IAQI_NO2_FOUR);
			NO2_threshlod.add(NO2_1H_FIVE);
			NO2_threshlod.add(NO2_1H_FOUR);
		}else if(n>NO2_1H_FIVE && n<=NO2_1H_SIX){
			NO2_threshlod.add(IAQI_NO2_SIX);
			NO2_threshlod.add(IAQI_NO2_FIVE);
			NO2_threshlod.add(NO2_1H_SIX);
			NO2_threshlod.add(NO2_1H_FIVE);
		}else if(n>NO2_1H_SIX && n<=NO2_1H_SEVEN){
			NO2_threshlod.add(IAQI_NO2_SEVEN);
			NO2_threshlod.add(IAQI_NO2_SIX);
			NO2_threshlod.add(NO2_1H_SEVEN);
			NO2_threshlod.add(NO2_1H_SIX);
		}else if(n>NO2_1H_SEVEN){
			NO2_threshlod.add(IAQI_NO2_EIGHT);
			NO2_threshlod.add(IAQI_NO2_SEVEN);
			NO2_threshlod.add(NO2_1H_EIGHT);
			NO2_threshlod.add(NO2_1H_SEVEN);
		}else{
			NO2_threshlod.add(0.0);
			NO2_threshlod.add(0.0);
			NO2_threshlod.add(1.0);
			NO2_threshlod.add(0.0);
		}
		return NO2_threshlod;
	}
}
