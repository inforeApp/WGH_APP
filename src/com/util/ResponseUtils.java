package com.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

/**
 * HttpServletResponse帮助�?
 */
public class ResponseUtils {
	public static void outputJson(HttpServletResponse response, Object obj,JsonConfig jsonConfig) {
		response.setCharacterEncoding("utf-8");
		PrintWriter out;
		try {
			out = response.getWriter();
			jsonConfig.registerJsonValueProcessor(Date.class,
					new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
			JSONArray userjson = JSONArray.fromObject(obj,jsonConfig);
			out.println(userjson.optJSONObject(0));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static String changeCharacterEncoding(String str){
		try {
			str = new String(str.getBytes("ISO-8859-1"), "UTF-8");
			return str;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
