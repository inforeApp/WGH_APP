package com.util;

public class Constant {
    public static final String WIND_N = "北";

    public static final String WIND_NNE = "东北北";

    public static final String WIND_NE = "东北";

    public static final String WIND_ENE = "东北东";

    public static final String WIND_E = "东";

    public static final String WIND_ESE = "东南东";

    public static final String WIND_SE = "东南";

    public static final String WIND_SSE = "东南南";

    public static final String WIND_S = "南";

    public static final String WIND_SSW = "西南南";

    public static final String WIND_SW = "西南";

    public static final String WIND_WSW = "西南西";

    public static final String WIND_W = "西";

    public static final String WIND_WNW = "西北西";

    public static final String WIND_NW = "西北";

    public static final String WIND_NNW = "西北北";

}
