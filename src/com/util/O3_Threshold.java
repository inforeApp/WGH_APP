package com.util;

import java.util.ArrayList;
import java.util.List;

public class O3_Threshold {
	private static final double O3_1H_ONE = 0.0; //O3污染物项目24小时平均浓度限值
	private static final double O3_1H_TWO = 160.0;
	private static final double O3_1H_THREE = 200.0;
	private static final double O3_1H_FOUR = 300.0;
	private static final double O3_1H_FIVE = 400.0;
	private static final double O3_1H_SIX = 800.0;
	private static final double O3_1H_SEVEN = 1000.0;
	private static final double O3_1H_EIGHT = 1200.0;
	private static final double IAQI_O3_ONE = 0.0;   //空气质量分指数平均浓度限值
	private static final double IAQI_O3_TWO = 50.0;
	private static final double IAQI_O3_THREE = 100.0;
	private static final double IAQI_O3_FOUR = 150.0;
	private static final double IAQI_O3_FIVE = 200.0;
	private static final double IAQI_O3_SIX = 300.0;
	private static final double IAQI_O3_SEVEN = 400.0;
	private static final double IAQI_O3_EIGHT = 500.0;
	
	public static List<Double> getO3_1H_Threshold(double n){
		List<Double> O3_threshlod = new ArrayList<Double>();
		if(n>=O3_1H_ONE&& n<=O3_1H_TWO){
			O3_threshlod.add(IAQI_O3_TWO);
			O3_threshlod.add(IAQI_O3_ONE);
			O3_threshlod.add(O3_1H_TWO);
			O3_threshlod.add(O3_1H_ONE);
		}else if(n>O3_1H_TWO && n<=O3_1H_THREE){
			O3_threshlod.add(IAQI_O3_THREE);
			O3_threshlod.add(IAQI_O3_TWO);
			O3_threshlod.add(O3_1H_THREE);
			O3_threshlod.add(O3_1H_TWO);
		}else if(n>O3_1H_THREE && n<=O3_1H_FOUR){
			O3_threshlod.add(IAQI_O3_FOUR);
			O3_threshlod.add(IAQI_O3_THREE);
			O3_threshlod.add(O3_1H_FOUR);
			O3_threshlod.add(O3_1H_THREE);
		}else if(n>O3_1H_FOUR && n<=O3_1H_FIVE){
			O3_threshlod.add(IAQI_O3_FIVE);
			O3_threshlod.add(IAQI_O3_FOUR);
			O3_threshlod.add(O3_1H_FIVE);
			O3_threshlod.add(O3_1H_FOUR);
		}else if(n>O3_1H_FIVE && n<=O3_1H_SIX){
			O3_threshlod.add(IAQI_O3_SIX);
			O3_threshlod.add(IAQI_O3_FIVE);
			O3_threshlod.add(O3_1H_SIX);
			O3_threshlod.add(O3_1H_FIVE);
		}else if(n>O3_1H_SIX && n<=O3_1H_SEVEN){
			O3_threshlod.add(IAQI_O3_SEVEN);
			O3_threshlod.add(IAQI_O3_SIX);
			O3_threshlod.add(O3_1H_SEVEN);
			O3_threshlod.add(O3_1H_SIX);
		}else if(n>O3_1H_SEVEN){
			O3_threshlod.add(IAQI_O3_EIGHT);
			O3_threshlod.add(IAQI_O3_SEVEN);
			O3_threshlod.add(O3_1H_EIGHT);
			O3_threshlod.add(O3_1H_SEVEN);
		}else {
			O3_threshlod.add(0.0);
			O3_threshlod.add(0.0);
			O3_threshlod.add(1.0);
			O3_threshlod.add(0.0);
		}
		return O3_threshlod;
	}
}
