package com.util;

import java.util.ArrayList;
import java.util.List;

public class PM10_Threshold {
	private static final double PM10_24H_ONE = 0.0; //PM10污染物项目24小时平均浓度限值
	private static final double PM10_24H_TWO = 100.0;
	private static final double PM10_24H_THREE = 200.0;
	private static final double PM10_24H_FOUR = 700.0;
	private static final double PM10_24H_FIVE = 1200.0;
	private static final double PM10_24H_SIX = 2340.0;
	private static final double PM10_24H_SEVEN = 3090.0;
	private static final double PM10_24H_EIGHT = 3840.0;
	private static final double IAQI_PM10_ONE = 0.0;   //空气质量分指数平均浓度限值
	private static final double IAQI_PM10_TWO = 50.0;
	private static final double IAQI_PM10_THREE = 100.0;
	private static final double IAQI_PM10_FOUR = 150.0;
	private static final double IAQI_PM10_FIVE = 200.0;
	private static final double IAQI_PM10_SIX = 300.0;
	private static final double IAQI_PM10_SEVEN = 400.0;
	private static final double IAQI_PM10_EIGHT = 500.0;
	
	public static List<Double> getPM10_1H_Threshold(double n){
		 List<Double> PM10_threshlod = new ArrayList<Double>();
		if(n>=PM10_24H_ONE && n<=PM10_24H_TWO){
			PM10_threshlod.add(IAQI_PM10_TWO);
			PM10_threshlod.add(IAQI_PM10_ONE);
			PM10_threshlod.add(PM10_24H_TWO);
			PM10_threshlod.add(PM10_24H_ONE);
		}else if(n>PM10_24H_TWO && n<=PM10_24H_THREE){
			PM10_threshlod.add(IAQI_PM10_THREE);
			PM10_threshlod.add(IAQI_PM10_TWO);
			PM10_threshlod.add(PM10_24H_THREE);
			PM10_threshlod.add(PM10_24H_TWO);
		}else if(n>PM10_24H_THREE && n<=PM10_24H_FOUR){
			PM10_threshlod.add(IAQI_PM10_FOUR);
			PM10_threshlod.add(IAQI_PM10_THREE);
			PM10_threshlod.add(PM10_24H_FOUR);
			PM10_threshlod.add(PM10_24H_THREE);
		}else if(n>PM10_24H_FOUR && n<=PM10_24H_FIVE){
			PM10_threshlod.add(IAQI_PM10_FIVE);
			PM10_threshlod.add(IAQI_PM10_FOUR);
			PM10_threshlod.add(PM10_24H_FIVE);
			PM10_threshlod.add(PM10_24H_FOUR);
		}else if(n>PM10_24H_FIVE && n<=PM10_24H_SIX){
			PM10_threshlod.add(IAQI_PM10_SIX);
			PM10_threshlod.add(IAQI_PM10_FIVE);
			PM10_threshlod.add(PM10_24H_SIX);
			PM10_threshlod.add(PM10_24H_FIVE);
		}else if(n>PM10_24H_SIX && n<=PM10_24H_SEVEN){
			PM10_threshlod.add(IAQI_PM10_SEVEN);
			PM10_threshlod.add(IAQI_PM10_SIX);
			PM10_threshlod.add(PM10_24H_SEVEN);
			PM10_threshlod.add(PM10_24H_SIX);
		}else if(n>PM10_24H_SEVEN){
			PM10_threshlod.add(IAQI_PM10_EIGHT);
			PM10_threshlod.add(IAQI_PM10_SEVEN);
			PM10_threshlod.add(PM10_24H_EIGHT);
			PM10_threshlod.add(PM10_24H_SEVEN);
		}else{
			PM10_threshlod.add(0.0);
			PM10_threshlod.add(0.0);
			PM10_threshlod.add(1.0);
			PM10_threshlod.add(0.0);
		}
		return PM10_threshlod;
	}
}
