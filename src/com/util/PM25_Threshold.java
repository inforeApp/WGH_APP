package com.util;

import java.util.ArrayList;
import java.util.List;

public class PM25_Threshold {
	private static final double PM25_24H_ONE = 0.0; //PM25污染物项目24小时平均浓度限值
	private static final double PM25_24H_TWO = 35.0;
	private static final double PM25_24H_THREE = 75.0;
	private static final double PM25_24H_FOUR = 115.0;
	private static final double PM25_24H_FIVE = 150.0;
	private static final double PM25_24H_SIX = 250.0;
	private static final double PM25_24H_SEVEN = 350.0;
	private static final double PM25_24H_EIGHT = 500.0;
	private static final double IAQI_PM25_ONE = 0.0;   //空气质量分指数平均浓度限值
	private static final double IAQI_PM25_TWO = 50.0;
	private static final double IAQI_PM25_THREE = 100.0;
	private static final double IAQI_PM25_FOUR = 150.0;
	private static final double IAQI_PM25_FIVE = 200.0;
	private static final double IAQI_PM25_SIX = 300.0;
	private static final double IAQI_PM25_SEVEN = 400.0;
	private static final double IAQI_PM25_EIGHT = 500.0;
	
	public static List<Double> getPM25_1H_Threshold(double n){
		 List<Double> PM25_threshlod = new ArrayList<Double>();
		if(n>=PM25_24H_ONE && n<=PM25_24H_TWO){
			PM25_threshlod.add(IAQI_PM25_TWO);
			PM25_threshlod.add(IAQI_PM25_ONE);
			PM25_threshlod.add(PM25_24H_TWO);
			PM25_threshlod.add(PM25_24H_ONE);
		}else if(n>PM25_24H_TWO && n<=PM25_24H_THREE){
			PM25_threshlod.add(IAQI_PM25_THREE);
			PM25_threshlod.add(IAQI_PM25_TWO);
			PM25_threshlod.add(PM25_24H_THREE);
			PM25_threshlod.add(PM25_24H_TWO);
		}else if(n>PM25_24H_THREE && n<=PM25_24H_FOUR){
			PM25_threshlod.add(IAQI_PM25_FOUR);
			PM25_threshlod.add(IAQI_PM25_THREE);
			PM25_threshlod.add(PM25_24H_FOUR);
			PM25_threshlod.add(PM25_24H_THREE);
		}else if(n>PM25_24H_FOUR && n<=PM25_24H_FIVE){
			PM25_threshlod.add(IAQI_PM25_FIVE);
			PM25_threshlod.add(IAQI_PM25_FOUR);
			PM25_threshlod.add(PM25_24H_FIVE);
			PM25_threshlod.add(PM25_24H_FOUR);
		}else if(n>PM25_24H_FIVE && n<=PM25_24H_SIX){
			PM25_threshlod.add(IAQI_PM25_SIX);
			PM25_threshlod.add(IAQI_PM25_FIVE);
			PM25_threshlod.add(PM25_24H_SIX);
			PM25_threshlod.add(PM25_24H_FIVE);
		}else if(n>PM25_24H_SIX && n<=PM25_24H_SEVEN){
			PM25_threshlod.add(IAQI_PM25_SEVEN);
			PM25_threshlod.add(IAQI_PM25_SIX);
			PM25_threshlod.add(PM25_24H_SEVEN);
			PM25_threshlod.add(PM25_24H_SIX);
		}else if(n>PM25_24H_SEVEN){
			PM25_threshlod.add(IAQI_PM25_EIGHT);
			PM25_threshlod.add(IAQI_PM25_SEVEN);
			PM25_threshlod.add(PM25_24H_EIGHT);
			PM25_threshlod.add(PM25_24H_SEVEN);
		}else{
			PM25_threshlod.add(0.0);
			PM25_threshlod.add(0.0);
			PM25_threshlod.add(1.0);
			PM25_threshlod.add(0.0);
		}
		return PM25_threshlod;
	}
}
