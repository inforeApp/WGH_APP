package com.util;

import java.util.Map;

public class GetAQI {
	public static void main(String[] args){
//  	 Map<String, Double> map = new HashMap<String, Double>();
//		Map<String, Double> map1 = new HashMap<String, Double>();
//		Map<String, Double> map2 = new HashMap<String, Double>();
//		Map<String, Double> map3 = new HashMap<String, Double>();
//		Map<String, Double> map4 = new HashMap<String, Double>();
//		Map<String, Double> map5 = new HashMap<String, Double>();
//		Map<String, Double> map6 = new HashMap<String, Double>();
//		map.put("SO2", 9.0);
//		map.put("NO2", 45.0);
//		map.put("PM10", 72.0);
//		map.put("CO", 0.96);
//		map.put("O3", 34.0);
//		map.put("PM2.5", 43.0);
//		
//		map1.put("SO2", 7.1);
//		map2.put("NO2", 28.88);
//		map3.put("PM10", 64.09);
//		map4.put("CO", 28.01);
//		map5.put("O3", 12.27);
//		map6.put("PM2.5", 62.27);
//		
//		
//		
//		System.out.println("map的AQI是："+GetAQI.getAQIMap(map));
//		System.out.println("map1的AQI是："+GetAQI.getAQIMap(map1));
//		System.out.println("map2的AQI是："+GetAQI.getAQIMap(map2));
//		System.out.println("map3的AQI是："+GetAQI.getAQIMap(map3));
//		System.out.println("map4的AQI是："+GetAQI.getAQIMap(map4));
//		System.out.println("map5的AQI是："+GetAQI.getAQIMap(map5));
//		System.out.println("map6的AQI是："+GetAQI.getAQIMap(map6));
	}

	
	
	
	public static int getAQIMap(Map<String,Double> map){
		int IAQI_SO2_1H =0;
		int IAQI_NO2_1H =0;
		int IAQI_PM10_1H =0;
		int IAQI_CO_1H =0;
		int IAQI_O3_1H =0;
		int IAQI_PM25_1H =0;
		int IAQI_O3_8H = 0;
		if(map.get("SO2") !=null){
			 IAQI_SO2_1H = GetAQI.getIAQI("SO2",map.get("SO2"));
		}
		if(map.get("NO2") !=null){
			IAQI_NO2_1H = GetAQI.getIAQI("NO2",map.get("NO2"));
		}
		if(map.get("PM10") !=null){
			 IAQI_PM10_1H = GetAQI.getIAQI("PM10",map.get("PM10"));
		}
		if(map.get("CO") !=null){
			 IAQI_CO_1H = GetAQI.getIAQI("CO",map.get("CO"));
		}
		if(map.get("O3") !=null){
			 IAQI_O3_1H = GetAQI.getIAQI("O3",map.get("O3"));
		}
		if(map.get("PM25") !=null){
			 IAQI_PM25_1H = GetAQI.getIAQI("PM2.5",map.get("PM25"));
		}
		if(map.get("O38H") != null){
			IAQI_O3_8H = GetAQI.getIAQI("O38H", map.get("O38H"));
		}
			
		int IAQI = GetAQI.Max(IAQI_SO2_1H, IAQI_NO2_1H, IAQI_PM10_1H, IAQI_CO_1H, IAQI_O3_1H, IAQI_PM25_1H,IAQI_O3_8H);
		return IAQI;
	}
	
	//获取空气质量污染等级
	public static String getPollutionLevel(int AQI){
		String airquality = null;
		if(AQI >=0 && AQI<= 50){
			airquality = "优";
		}else if(AQI >=51 && AQI<= 100){
			airquality = "良";
		}else if(AQI >=101 && AQI<= 150){
			airquality = "轻度污染";
		}else if(AQI >=151 && AQI<= 200){
			airquality = "中度污染";
		}else if(AQI >=201 && AQI<= 300){
			airquality = "重度污染";
		}else if(AQI > 300){
			airquality = "严重污染";
		}
		return airquality;
	}
	
	//获取首要污染物
	public static String getPrimaryPollution(Map<String, Double> map){

		int IAQI_SO2_1H =0;
		int IAQI_NO2_1H =0;
		int IAQI_PM10_1H =0;
		int IAQI_CO_1H =0;
		int IAQI_O3_1H =0;
		int IAQI_PM25_1H =0;
		int IAQI_O3_8H = 0;
		if(map.get("SO2") !=null){
			 IAQI_SO2_1H = GetAQI.getIAQI("SO2",map.get("SO2"));
		}
		if(map.get("NO2") !=null){
			IAQI_NO2_1H = GetAQI.getIAQI("NO2",map.get("NO2"));
		}
		if(map.get("PM10") !=null){
			 IAQI_PM10_1H = GetAQI.getIAQI("PM10",map.get("PM10"));
		}
		if(map.get("CO") !=null){
			 IAQI_CO_1H = GetAQI.getIAQI("CO",map.get("CO"));
		}
		if(map.get("O3") !=null){
			 IAQI_O3_1H = GetAQI.getIAQI("O3",map.get("O3"));
		}
		if(map.get("PM25") !=null){
			 IAQI_PM25_1H = GetAQI.getIAQI("PM2.5",map.get("PM25"));
		}
		if(map.get("O38H") != null){
			IAQI_O3_8H = GetAQI.getIAQI("O38H", map.get("O38H"));
		}
		StringBuffer sBuffer = new StringBuffer("");
		int AQI = GetAQI.Max(IAQI_SO2_1H, IAQI_NO2_1H, IAQI_PM10_1H, IAQI_CO_1H, IAQI_O3_1H, IAQI_PM25_1H,IAQI_O3_8H);
		if(AQI>=50){
			if(AQI !=0 && AQI == IAQI_SO2_1H){
				if(sBuffer.toString().equals("")){
					sBuffer.append("二氧化硫");
				}else{
					sBuffer.append(",二氧化硫");
				}
			}
			if(AQI !=0 && AQI == IAQI_NO2_1H){
				if(sBuffer.toString().equals("")){
					sBuffer.append("二氧化氮");
				}else{
					sBuffer.append(",二氧化氮");
				}
			}
			if(AQI !=0 && AQI == IAQI_CO_1H){
				if(sBuffer.toString().equals("")){
					sBuffer.append("一氧化碳");
				}else{
					sBuffer.append(",一氧化碳");
				}
			}
			if(AQI !=0 && AQI == IAQI_O3_1H){
				if(sBuffer.toString().equals("")){
					sBuffer.append("臭氧1小时");
				}else{
					sBuffer.append(",臭氧1小时");
				}
			}
			if(AQI !=0 && AQI == IAQI_O3_8H){
				if(sBuffer.toString().equals("")){
					sBuffer.append("臭氧8小时");
				}else{
					sBuffer.append(",臭氧8小时");
				}
			}
			if(AQI !=0 && AQI == IAQI_PM10_1H){
				if(sBuffer.toString().equals("")){
					sBuffer.append("颗粒物(PM10)");
				}else{
					sBuffer.append(",颗粒物(PM10)");
				}
			}
			if(AQI !=0 && AQI == IAQI_PM25_1H){
				if(sBuffer.toString().equals("")){
					sBuffer.append("颗粒物(PM2.5)");
				}else{
					sBuffer.append(",颗粒物(PM2.5)");
				}
			}
		}
		return sBuffer.toString();
	
	}
	
	/**
	 * author : Bruce
	 * @param SO2 二氧化硫(SO2)1小时平均浓度值
	 * @param NO2 二氧化氮(NO2)1小时平均浓度值
	 * @param PM10 PM10 1小时平均浓度值
	 * @param CO 一氧化碳(CO) 1小时平均浓度值
	 * @param O3 臭氧(O3) 1小时平均浓度值
	 * @param PM2.5 PM2.5 1小时平均浓度值
	 * @return
	 */
	public static int Max(int SO2, int NO2, int PM10 , int CO, int O3, int PM25,int O38H){
	 	int	max = 0;
	 	if(max <= SO2){
	 		max = SO2;
	 	}
	 	if(max <= NO2){
	 		max = NO2;
	 	}
	 	if(max <= PM10){
	 		max=PM10;
	 	}
	 	if(max <= CO){
	 		max = CO;
	 	}
	 	if(max <= O3){
	 		max =O3;
	 	}
	 	if(max <= PM25){
	 		max=PM25;
	 	}
	 	if(max <= O38H){
	 		max = O38H;
	 	}
	 	return max;
	}
	
	//获取每个因子的IAQI值,并存入Map回调
	public static int getIAQI(String name ,double value){

				if(name.toUpperCase().equals("SO2")){
					return GetAQI.getIAQI_SO2_1H(value);
				}else if(name.toUpperCase().equals("NO2")){
					return  GetAQI.getIAQI_NO2_1H(value);
				}else if(name.toUpperCase().equals("PM10")){
					return GetAQI.getIAQI_PM10_1H(value);
				}else if(name.toUpperCase().equals("CO")){
					return GetAQI.getIAQI_CO_1H(value);
				}else if(name.toUpperCase().equals("O3")){
					return GetAQI.getIAQI_O3_1H(value);
				}else if(name.toUpperCase().equals("O38H")){
					return getIAQI_O3_8H(value);
				}else if(name.toUpperCase().equals("PM25") || name.toUpperCase().equals("PM2.5")){
					return   GetAQI.getIAQI_PM25_1H(value);
				}else{
					return 0;
				}
	}
	
	//二氧化硫1小时计算方法
	private static int getIAQI_SO2_1H(double SO2){
		int IAQI = 0;
		SO2 = Math.round(SO2);
		if (SO2 >= 0 && SO2 < 150){
            IAQI = Linear(50,0,149,0,SO2);
        }else if (SO2 >= 150 && SO2 < 500){
            IAQI = Linear(100,51,499,150,SO2);
        }else if (SO2 >= 500 && SO2 < 650){
            IAQI = Linear(150,101,649,500,SO2);
        }else if (SO2 >= 650 && SO2 <= 800){
            IAQI = Linear(200,151,799,650,SO2);
        }else if (SO2 >= 800 && SO2 <= 2620){
            IAQI = 500;
        }else{
            IAQI = 0;
        }
		return IAQI;
	}
	
//	//二氧化硫24小时计算方法
//	private static int AQISO224hr(double SO2){
//		int IAQI=0;
//		SO2 = Math.round(SO2);
//		if (SO2 >= 0 && SO2 < 50){
//	        IAQI = Linear(50,0,49,0,SO2);
//	    }else if (SO2 >= 50 && SO2 < 150){
//	        IAQI = Linear(100,51,499,150,SO2);
//	    }else if (SO2 >= 150 && SO2 < 475){
//	        IAQI = Linear(150,101,649,500,SO2);
//	    }else if (SO2 >= 475 && SO2 <= 800){
//	        IAQI = Linear(200,151,799,650,SO2);
//	    }else if (SO2 >= 800 && SO2 <= 1600){
//	         IAQI = Linear(300,201,1599,800,SO2);
//	    }else if (SO2 >= 1600 && SO2 < 2100){
//	        IAQI = Linear(400,301,2099,1600,SO2);
//	    }else if (SO2 >= 2100 && SO2 <= 2620){
//	        IAQI = Linear(500,401,2619,2100,SO2);
//	    }else{
//	        IAQI = 500;
//	    }
//		return IAQI;
//	}
	
	//no2 1小时计算方法
	private static int getIAQI_NO2_1H(double NO2){
		int IAQI = 0;
		NO2 = Math.round(NO2);
		if (NO2 >= 0 && NO2 < 100){
            IAQI = Linear(50,0,99,0,NO2);
        }else if (NO2 >= 100 && NO2 < 200){
            IAQI = Linear(100,51,199,100,NO2);
        }else if (NO2 >= 200 && NO2 < 700){
            IAQI = Linear(150,101,699,200,NO2);
        }else if (NO2 >= 700 && NO2 < 1200){
           IAQI = Linear(200,151,1199,700,NO2);
        }else if (NO2 >= 1200 && NO2 < 2340){
            IAQI = Linear(300,201,2339,1200,NO2);
        }else if (NO2 >= 2340 && NO2 < 3090){
            IAQI = Linear(400,301,3089,2340,NO2);
        }else if (NO2 >= 3090 && NO2 <= 3840){
            IAQI = Linear(500,401,3839,3090,NO2);
        }else{
            IAQI = 0;
        }
		return IAQI;
	}
	
	private static int getIAQI_PM10_1H(double PM10){
		int IAQI =0;
		PM10 = Math.round(PM10);
		if (PM10 >= 0 && PM10 < 50){
            IAQI = Linear(50,0,49,0,PM10);
        }else if (PM10 >= 50 && PM10 < 150){
            IAQI = Linear(100,51,149,50,PM10);
        }else if (PM10 >= 150 && PM10 < 250){
            IAQI = Linear(150,101,249,149,PM10);
        }else if (PM10 >= 250 && PM10 < 350){
            IAQI = Linear(200,151,349,250,PM10);
        }else if (PM10 >= 350 && PM10 < 420){
            IAQI = Linear(300,201,419,350,PM10);
        }else if (PM10 >= 420 && PM10 < 500){
            IAQI = Linear(400,301,499,420,PM10);
        }else if (PM10 >= 500 && PM10 < 600){
            IAQI = Linear(500,401,599,500,PM10);
        }else{
            IAQI = 0;
        }
		return IAQI;
	}
	
	private static int getIAQI_CO_1H(double CO){
		int IAQI =0;
		double C = (Math.round(CO*10))/10; 
		if (C >= 0 && C < 5){
            IAQI = Linear(50,0,4.9,0,C);
        }else if (C >= 5 && C < 10){
            IAQI = Linear(100,51,9.9,5,C);
        }else if (C >= 10 && C < 35){
            IAQI = Linear(150,101,34.9,10,C);
        }else if (C >= 35 && C < 60){
            IAQI = Linear(200,151,59.9,35,C);
        }else if (C >= 60 && C < 90){
            IAQI = Linear(300,201,89.9,60,C);
        }else if (C >= 90 && C < 120){
            IAQI = Linear(400,301,119.9,90,C);
        }else if (C >= 120 && C < 150){
            IAQI = Linear(500,401,149.9,120,C);
        }else{
            IAQI = 0;
        }
		return IAQI;
	}
	
	private static int getIAQI_O3_1H(double O3){
		int IAQI =0;
		O3 = Math.round(O3);
		if (O3 >= 0 && O3 < 160){
            IAQI = Linear(50,0,159,0,O3);
        }else if (O3 >= 160 && O3 < 200){
            IAQI = Linear(100,51,199,160,O3);
        }else if (O3 >= 200 && O3 < 300){
            IAQI = Linear(150,101,299,200,O3);
        }else if (O3 >= 300 && O3 < 400){
            IAQI = Linear(200,151,399,300,O3);
        }else if (O3 >= 400 && O3 < 800){
            IAQI = Linear(300,201,799,400,O3);
        }else if (O3 >= 800 && O3 < 1000){
            IAQI = Linear(400,301,999,800,O3);
        }else if (O3 >= 1000 && O3 < 1200){
            IAQI = Linear(500,401,1199,1200,O3);
        }else{
            IAQI = 0;
        }
		return IAQI;
	}
	
	private static int getIAQI_PM25_1H(double PM25){
		int IAQI =0;
		PM25 = (Math.round(PM25*10))/10;
		if (PM25 >= 0 && PM25 < 36){
            IAQI = Linear(50,0,35,0,PM25);
        }else if (PM25 >= 36 && PM25 < 76){
            IAQI = Linear(100,51,75,35,PM25);
        }else if (PM25 >= 76 && PM25 < 116){
            IAQI = Linear(150,101,115,75,PM25);
        }else if (PM25 >= 116 && PM25 < 151){
            IAQI = Linear(200,151,150,115,PM25);
        }else if (PM25 >= 151 && PM25 < 251){
            IAQI = Linear(300,201,250,150,PM25);
        }else if (PM25 >= 251 && PM25 < 351){
            IAQI = Linear(400,301,350,250,PM25);
        }else if (PM25 >= 351 && PM25 < 501){
            IAQI = Linear(500,401,500,350,PM25);
        }else{
            IAQI = 0;
        }
		return IAQI;
	}
	

	private static int getIAQI_O3_8H(double O38H){
		int IAQI = 0;
		O38H = Math.round(O38H);
		if (O38H >= 0 && O38H < 100){
            IAQI = Linear(50,0,99,0,O38H);
        }else if (O38H >= 100 && O38H < 160){
            IAQI = Linear(100,51,159,100,O38H);
        }else if (O38H >= 160 && O38H < 215){
            IAQI = Linear(150,101,214,160,O38H);
        }else if (O38H >= 215 && O38H < 265){
            IAQI = Linear(200,151,299,200,O38H);
        }else if (O38H >= 265 && O38H < 800){
            IAQI = Linear(300,201,799,265,O38H);
        }else{
            IAQI = 0;
        }
		return IAQI;
	}
	
//因子线性化
	private static int Linear(double IAQIhigh, double IAQIlow, double BPhigh, double BPlow, double Concentration){
		double tempIAQI = ((Concentration - BPlow)/(BPhigh-BPlow))*(IAQIhigh-IAQIlow) +IAQIlow;
		int IAQI = (int)Math.round(tempIAQI); 
		return IAQI;
	}
	
}
