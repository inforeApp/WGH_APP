package com.util;

import java.util.ArrayList;
import java.util.List;


public class CO_Threshold {
	private static final double CO_1H_ONE = 0.0; //CO污染物项目24小时平均浓度限值
	private static final double CO_1H_TWO = 5.0;
	private static final double CO_1H_THREE = 10.0;
	private static final double CO_1H_FOUR = 35.0;
	private static final double CO_1H_FIVE = 60.0;
	private static final double CO_1H_SIX = 90.0;
	private static final double CO_1H_SEVEN = 120.0;
	private static final double CO_1H_EIGHT = 150.0;
	private static final double IAQI_CO_ONE = 0.0;   //空气质量分指数平均浓度限值
	private static final double IAQI_CO_TWO = 50.0;
	private static final double IAQI_CO_THREE = 100.0;
	private static final double IAQI_CO_FOUR = 150.0;
	private static final double IAQI_CO_FIVE = 200.0;
	private static final double IAQI_CO_SIX = 300.0;
	private static final double IAQI_CO_SEVEN = 400.0;
	private static final double IAQI_CO_EIGHT = 500.0;
	
	public static List<Double> getCO_1H_Threshold(double n){
		 List<Double> CO_threshlod = new ArrayList<Double>();
		if(n>=CO_1H_ONE && n<=CO_1H_TWO){
			CO_threshlod.add(IAQI_CO_TWO);
			CO_threshlod.add(IAQI_CO_ONE);
			CO_threshlod.add(CO_1H_TWO);
			CO_threshlod.add(CO_1H_ONE);
		}else if(n>CO_1H_TWO && n<=CO_1H_THREE){
			CO_threshlod.add(IAQI_CO_THREE);
			CO_threshlod.add(IAQI_CO_TWO);
			CO_threshlod.add(CO_1H_THREE);
			CO_threshlod.add(CO_1H_TWO);
		}else if(n>CO_1H_THREE && n<=CO_1H_FOUR){
			CO_threshlod.add(IAQI_CO_FOUR);
			CO_threshlod.add(IAQI_CO_THREE);
			CO_threshlod.add(CO_1H_FOUR);
			CO_threshlod.add(CO_1H_THREE);
		}else if(n>CO_1H_FOUR && n<=CO_1H_FIVE){
			CO_threshlod.add(IAQI_CO_FIVE);
			CO_threshlod.add(IAQI_CO_FOUR);
			CO_threshlod.add(CO_1H_FIVE);
			CO_threshlod.add(CO_1H_FOUR);
		}else if(n>CO_1H_FIVE && n<=CO_1H_SIX){
			CO_threshlod.add(IAQI_CO_SIX);
			CO_threshlod.add(IAQI_CO_FIVE);
			CO_threshlod.add(CO_1H_SIX);
			CO_threshlod.add(CO_1H_FIVE);
		}else if(n>CO_1H_SIX && n<=CO_1H_SEVEN){
			CO_threshlod.add(IAQI_CO_SEVEN);
			CO_threshlod.add(IAQI_CO_SIX);
			CO_threshlod.add(CO_1H_SEVEN);
			CO_threshlod.add(CO_1H_SIX);
		}else if(n>CO_1H_SEVEN){
			CO_threshlod.add(IAQI_CO_EIGHT);
			CO_threshlod.add(IAQI_CO_SEVEN);
			CO_threshlod.add(CO_1H_EIGHT);
			CO_threshlod.add(CO_1H_SEVEN);
		}else{
			CO_threshlod.add(0.0);
			CO_threshlod.add(0.0);
			CO_threshlod.add(1.0);
			CO_threshlod.add(0.0);
		}
		return CO_threshlod;
	}
}
