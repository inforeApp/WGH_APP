package com.util;

import java.util.ArrayList;
import java.util.List;

public class SO2_Threshold {
	private static final double SO2_1H_ONE = 0.0; //二氧化硫污染物项目1小时平均浓度限值
	private static final double SO2_1H_TWO = 150.0;
	private static final double SO2_1H_THREE = 500.0;
	private static final double SO2_1H_FOUR = 650.0;
	private static final double SO2_1H_FIVE = 800.0;
//	private static final double SO2_1H_SIX = 0.0;
//	private static final double SO2_1H_SEVEN =0.0;
//	private static final double SO2_1H_EIGHT = 0.0;
//	private static final double SO2_24H_ONE = 0.0; //二氧化硫污染物项目24小时平均浓度限值
//	private static final double SO2_24H_TWO = 50.0;
//	private static final double SO2_24H_THREE = 150.0;
//	private static final double SO2_24H_FOUR = 475.0;
//	private static final double SO2_24H_FIVE = 800.0;
//	private static final double SO2_24H_SIX = 1600.0;
//	private static final double SO2_24H_SEVEN =2100.0;
//	private static final double SO2_24H_EIGHT = 2620.0;
	private static final double IAQI_SO2_ONE = 0.0;   //空气质量分指数平均浓度限值
	private static final double IAQI_SO2_TWO = 50.0;
	private static final double IAQI_SO2_THREE = 100.0;
	private static final double IAQI_SO2_FOUR = 150.0;
	private static final double IAQI_SO2_FIVE = 200.0;
//	private static final double IAQI_SO2_SIX = 300.0;
//	private static final double IAQI_SO2_SEVEN = 400.0;
//	private static final double IAQI_SO2_EIGHT = 500.0;
	
	public static  List<Double> getSO2_1H_Threshold(double n){
		List<Double> SO2_threshold = new  ArrayList<Double>();
		if(n>=SO2_1H_ONE && n<=SO2_1H_TWO){
			SO2_threshold.add(IAQI_SO2_TWO);
			SO2_threshold.add(IAQI_SO2_ONE);
			SO2_threshold.add(SO2_1H_TWO);
			SO2_threshold.add(SO2_1H_ONE);
		}else if(n>SO2_1H_TWO && n<=SO2_1H_THREE){
			SO2_threshold.add(IAQI_SO2_THREE);
			SO2_threshold.add(IAQI_SO2_TWO);
			SO2_threshold.add(SO2_1H_THREE);
			SO2_threshold.add(SO2_1H_TWO);
		}else if(n>SO2_1H_THREE && n<=SO2_1H_FOUR){
			SO2_threshold.add(IAQI_SO2_FOUR);
			SO2_threshold.add(IAQI_SO2_THREE);
			SO2_threshold.add(SO2_1H_FOUR);
			SO2_threshold.add(SO2_1H_THREE);
		}else if (n>SO2_1H_FOUR){
			SO2_threshold.add(IAQI_SO2_FIVE);
			SO2_threshold.add(IAQI_SO2_FOUR);
			SO2_threshold.add(SO2_1H_FIVE);
			SO2_threshold.add(SO2_1H_FOUR);
		}else{
			SO2_threshold.add(0.0);
			SO2_threshold.add(0.0);
			SO2_threshold.add(1.0);
			SO2_threshold.add(0.0);
		}
		return SO2_threshold;
	}
}
