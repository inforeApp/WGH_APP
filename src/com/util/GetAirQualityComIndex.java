/**
 * 
 */
package com.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * @author yuxing
 *
 */
public class GetAirQualityComIndex {
	//各个因子浓度二级限值
	private final static int SO2_LIMITS_2 = 60;
	private final static int NO2_LIMITS_2 = 40;
	private final static int CO_LIMITS_2 = 4;
	private final static int O3_LIMITS_2 = 160;
	private final static int PM10_LIMITS_2 = 70;
	private final static int PM25_LIMITS_2 = 35;
	
	public static void main(String[] args) {
		Map<String, Double> map = new  HashMap<String, Double>();
		map.put("PM25", 100.0);
		System.out.println(getSubIndex(map));
		System.out.println(getPM25_subIndex(100.0));
	}
	
	

	//各单项因子单个指数计算方法
	public static double getSO2_subIndex(double SO2){
		double SO2_subIndex = new BigDecimal(SO2/SO2_LIMITS_2).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		return SO2_subIndex;
	}
	
	public static double getNO2_subIndex(double NO2){
		double NO2_subIndex = new BigDecimal(NO2/NO2_LIMITS_2).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		return NO2_subIndex;
	}
	
	public static double getCO_subIndex(double CO){
		double CO_subIndex = new BigDecimal(CO/CO_LIMITS_2).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		return CO_subIndex;
	}
	
	public static double getO3_subIndex(double O3){
		double O3_subIndex = new BigDecimal(O3/O3_LIMITS_2).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		return O3_subIndex;
	}
	
	public static double getPM10_subIndex(double PM10){
		double PM10_subIndex = new BigDecimal(PM10/PM10_LIMITS_2).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		return PM10_subIndex;
	}
	
	public static double getPM25_subIndex(double PM25){
		double PM25_subIndex = new BigDecimal(PM25/PM25_LIMITS_2).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		return PM25_subIndex;
	}
	
	//环境质量综合指数
	public static double getAQCI(Map<String, Double> map){
		double AQCI = 0;
		double ISO2 = 0;
		double INO2 = 0;
		double ICO = 0;
		double IO3 = 0;
		double IPM10 = 0;
		double IPM25 = 0;
		if(map.get("SO2") != null){
			ISO2 = getSO2_subIndex(map.get("SO2"));
		}
		if(map.get("NO2") != null){
			INO2 = getNO2_subIndex(map.get("NO2"));
		}
		if(map.get("CO") != null){
			ICO = getCO_subIndex(map.get("CO"));
		}
		if(map.get("O3") != null){
			IO3 = getO3_subIndex(map.get("O3"));
		}
		if(map.get("PM10") != null){
			IPM10 = getPM10_subIndex(map.get("PM10"));
		}
		if(map.get("PM25") != null){
			IPM25 = getPM25_subIndex(map.get("PM25"));
		}
		AQCI = new BigDecimal(ISO2+INO2+ICO+IO3+IPM10+IPM25).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		return AQCI;
	}
	
	
	public static double getSubIndex(Map<String, Double> map){
		double IAQI_SO2_1H =0;
		double IAQI_NO2_1H =0;
		double IAQI_PM10_1H =0;
		double IAQI_CO_1H =0;
		double IAQI_O3_1H =0;
		double IAQI_PM25_1H =0;
		double IAQI_O3_8H = 0;
		if(map.get("SO2") !=null){
			 IAQI_SO2_1H = getSO2_subIndex(map.get("SO2"));
		}
		if(map.get("NO2") !=null){
			IAQI_NO2_1H = getNO2_subIndex(map.get("NO2"));
		}
		if(map.get("PM10") !=null){
			 IAQI_PM10_1H = getPM10_subIndex(map.get("PM10"));
		}
		if(map.get("CO") !=null){
			 IAQI_CO_1H = getCO_subIndex(map.get("CO"));
		}
		if(map.get("O3") !=null){
			 IAQI_O3_1H = getO3_subIndex(map.get("O3"));
		}
		if(map.get("PM25") !=null){
			 IAQI_PM25_1H = getPM25_subIndex(map.get("PM25"));
		}
		if(map.get("O38H") != null){
			IAQI_O3_8H = getO3_subIndex(map.get("O38H"));
		}
		
		double IAQCI = Max(IAQI_SO2_1H, IAQI_NO2_1H, IAQI_PM10_1H, IAQI_CO_1H, IAQI_O3_1H, IAQI_PM25_1H,IAQI_O3_8H);
		return IAQCI;
	}
	
	
	public static double Max(double ISO2, double INO2, double IPM10 , double ICO, double IO3, double IPM25,double IO38H){
	 	double	max = 0;
	 	if(max < ISO2){
	 		max = ISO2;
	 	}
	 	if(max < INO2){
	 		max = INO2;
	 	}
	 	if(max < IPM10){
	 		max=IPM10;
	 	}
	 	if(max < ICO){
	 		max = ICO;
	 	}
	 	if(max < IO3){
	 		max =IO3;
	 	}
	 	if(max < IPM25){
	 		max=IPM25;
	 	}
	 	if(max < IO38H){
	 		max = IO38H;
	 	}
	 	return max;
	}
	
}
