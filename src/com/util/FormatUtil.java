package com.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

/**
 * 日历工具�?
 * @author zwt
 *
 */
public class FormatUtil {
	public static SimpleDateFormat second=new SimpleDateFormat("yyyyMMddHHmmss");
	public static SimpleDateFormat minute=new SimpleDateFormat("yyyyMMddHHmm");
	public static SimpleDateFormat hour=new SimpleDateFormat("yyyyMMddHH");
	public static SimpleDateFormat day=new SimpleDateFormat("yyyyMMdd");
	public static SimpleDateFormat month=new SimpleDateFormat("yyyyMM");
	public static SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat YMDHM=new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public static SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat secondZ=new SimpleDateFormat("yyyy-MM-dd 00:00:00");
	public static SimpleDateFormat secondE=new SimpleDateFormat("yyyy-MM-dd 23:00:00");
	public static SimpleDateFormat millisecond=new SimpleDateFormat("yyyyMMddHHmmssSSS");
	public static SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
	public static SimpleDateFormat historymonth=new SimpleDateFormat("yyyy-MM");
	
	public static SimpleDateFormat daily=new SimpleDateFormat("yyyy��MM��dd��");
	/**
	 * 除法运算
	 * @Title: divided
	 * @Description: TODO
	 * @date 2016�?�?0�?下午4:55:46
	 * @author  周文�?
	 */
	public static Double divided( double dividend, double divisor, int digit ) {
		Double result = 0.0;
		if ( divisor != 0L ) {
			result = dividend / divisor;
		}
		if ( digit < 0 ) {
			digit = 0;
		}
		BigDecimal bd = new BigDecimal(result);
		return bd.setScale(digit, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	/**
	 * 获取适当小数位数
	 * @Title: scale
	 * @Description: TODO
	 * @date 2016�?�?1�?下午5:53:58
	 * @author  周文�?
	 */
	public static Double scale(Double value,Integer digit){
		if(digit==null){
			digit=new Integer(4);
		}
		BigDecimal bd = new BigDecimal(value);
		return bd.setScale(digit, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
	public static void main(String[] args) {
		System.out.println(divided(9,10,2));
	}
    public static String windDireTransfer(double direction){

        if(direction > 0 && direction <=  11.25){

            return Constant.WIND_N;

        }else if(direction <=33.75){

            return Constant.WIND_NNE;

        }else if(direction <= 56.25){

            return  Constant.WIND_NE ;

        } else if (direction <= 78.75){

            return  Constant.WIND_ENE ;

        } else if (direction <= 101.25){

            return Constant.WIND_E ;

        } else if (direction <= 123.75){

            return Constant.WIND_ESE ;

        }else if (direction <= 146.25){

            return Constant.WIND_SE ;

        } else if (direction <= 168.75){

            return Constant.WIND_SSE;

        } else if (direction <= 191.25){

            return Constant.WIND_S;

        } else if(direction <= 213.75){

            return Constant.WIND_SSW;

        } else if (direction <= 236.25){

            return Constant.WIND_SW;

        } else if (direction <= 258.75) {

            return Constant.WIND_WSW;

        } else if (direction <= 281.25) {

            return Constant.WIND_W;

        } else if (direction <= 303.75) {

            return Constant.WIND_WNW ;

        } else if (direction <= 326.25) {

            return Constant.WIND_NW ;

        } else if (direction <= 348.75) {

            return Constant.WIND_NNW;

        } else if(direction <= 360){

            return Constant.WIND_N;

        }else {

         return "";

        }

    }

}
