package com.json;

/**
 * 
 * 
 */
public class StatusCode {

	public static int CODE_SUCCESS = 1;

	public static int CODE_ERROR = 0;

	public static String CODE_ERROR_PARAMETER = "0002";

	public static String CODE_ERROR_PROGRAM = "0003";

	public static String CODE_ERROR_NO_LOGIN_OR_TIMEOUT = "0004";

	public static String CODE_ERROR_EXIST_OPERATION = "0005";

}
