package com.dao;


import java.util.List;
import java.util.Map;

import com.model.User;

public interface UserDao {
	public User findUserByUserName( String userName);
	
	public List<User> findAllUser(String userId);
	public List<User> queryPersonPlace(String userId);
	public int  queryPlace(String lnt,String lat,String userId,String address,String area);
	public List<Map<String, Object>> queryPollutionStation(String userId);
	public Object queryGrid(String userId);

	public Object getAccessToken();

	public Object queryNewStationTree(String userId);
	
	public Object queryStationTree(String userId);


	public Object queryRealInfoRank(String factorType, String stationType,
			String userId);

	public Object queryTrack(String time, String userId);

	public Object queryRecentlyData(String mn);
}