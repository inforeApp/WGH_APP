package com.dao;


import org.springframework.web.multipart.MultipartFile;

public interface WorkDao {
	public int  newQuertionReport(String quertionTitle, String quertionNum,
			String releaseTime, String gridNum, String questionAddress,
			MultipartFile[] file,String questionContent,String userId);

	public Object queryQuestionReport(String userId);

	public Object searchDischargePollution(String state, String keyword,String userId);

	public Object searchPollutionByMn(String mn);

	public Object searchDischargePollutionByMn(String mn);

	public int newMission(String title, String mn, String issuePsonId,
			String issueTime, String lastTime, String handlePsonId,
			String discrib, String isApproved, String approvePson,
			MultipartFile[] voice, MultipartFile[] file,String leaderId);

	public Object queryGtasks(String userId, String type,String keyword,String state,String startTime,String endTime);

	public Object searchPollution(String keyword, String state,String userId);

	public int ResearchAskRecord(String RecorderJC, String NotesJC,
			String NotesAsk, String obtainEvidence,
			MultipartFile[] qySignature, MultipartFile[] zfSignature,
			String missionId, String userId, String mn,MultipartFile[] files,String lat,String lng,String address);

	public int SceneEvidence(String lat, String lnt, String address,
			String pson, String mn, MultipartFile[] pic, MultipartFile[] voice,
			MultipartFile[] video);

	public int checkMission(String mission_id, String state, String describ,
			String time);

	public int changeMissionStatus(String mission_id, String userId,String describ);

	public Object queryProcess(String mission_id);

	public Object queryLedger(String userId,String pageNum, String mn, String keyword);

	public Object queryDetailsById(String id, String state);

	public int deleteMission(String userId, String missionId);

	public Object queryCheckMission(String userId, String state);

	public Object queryLedgerById(String userId, String ledgerId);

	public Object queryRecordList(String userId,String dataType,String pageNum,String recordType);

	public int newRecord(String userId, String happenTime, String mn,
			String dangerLevel, String factor, String describ, String dispatcher,String recordTitle);

	public int handleRecord(String userId, String recordId,
			String handleDescrib, MultipartFile[] files, String lat, String lnt, String address);

	public Object queryRecordDetails(String userId, String recordId);

	public int queryRecordVerify(String userId, String recordId);

	public int deleteRecord(String userId, String recordId);

	public int updateRecord(String recordId, String userId, String happenTime,
			String mn, String dangerLevel, String factor, String describ,
			String dispatcher, String deadLine,String recordTitle);

	public Object handleRecordList(String userId, String pageNum);

	public Object sreachRecordList(String userId, String pageNum,
			String keyword, String dataType);

	public Object sreachHandleRecordList(String userId, String pageNum,
			String keyword);
}
