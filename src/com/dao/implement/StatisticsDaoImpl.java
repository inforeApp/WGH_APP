package com.dao.implement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.dao.StatisticsDao;
import com.model.FactorRank;
import com.model.NewPoint;
import com.model.Point;

@Repository
public class StatisticsDaoImpl implements StatisticsDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;


	@Override
	public Object queryContrastAnalyze(String time, String stationString,
			String userId, String dataType) {
		try {
			SimpleDateFormat si = new SimpleDateFormat("yyyy-MM");
			Date date = si.parse(time);
			Calendar ca = Calendar.getInstance();
			ca.setTime(date);
			String sstime = si.format(ca.getTime());
			if(stationString!=null){
				String[] stationsStrings = stationString.split(",");
				stationString = "";
				for (int i = 0,length = stationsStrings.length; i < length; i++) {
					stationString += "'" + stationsStrings[i] + "',";
				}
				if (stationsStrings.length == 0) {
					stationString = "";
				} else {
					stationString = stationString.substring(0,
							stationString.length() - 1);
				}
			}else{
				String sql = "select * from Base_STATION where institution_BH = (select institution_BH from XT_YHXX where id = ?)  and rownum =1";
				List<Map<String,Object>> list =jdbcTemplate.queryForList(sql,new Object[]{userId});
				stationString = "";
				for (int i = 0,length = list.size(); i < length; i++) {
					if(i!=length-1){
						stationString += "'" + list.get(i).get("mn") + "',";
					}else{
						stationString += "'" + list.get(i).get("mn") + "'";
					}
					
				}
			}
			String stime = "";
			if ("0".equals(dataType)) {
				ca.add(Calendar.YEAR, -1);
				SimpleDateFormat ysi = new SimpleDateFormat("yyyy-MM");
				stime = ysi.format(ca.getTime());
			} else {
				ca.add(Calendar.MONTH, -1);
				stime = si.format(ca.getTime());
			}
			String sql = " select a.mn,a.name,decode(b.PM10,0,0,round(nvl((a.PM10-b.PM10)/b.PM10,0),4)) PM10,nvl(a.PM10,0) CPM10,nvl(b.PM10,0) PPM10,decode(b.CO,0,0,round(nvl((a.CO-b.CO)/b.CO,0),4)) CO,nvl(a.CO,0) CCO,nvl(b.CO,0) PCO,decode(b.NO2,0,0,round(nvl((a.NO2-b.NO2)/b.NO2,0),4)) NO2,nvl(a.NO2,0) CNO2,nvl(b.NO2,0) PNO2,decode(b.SO2,0,0,round(nvl((a.SO2-b.SO2)/b.SO2,0),4)) SO2,nvl(a.SO2,0) CSO2,nvl(b.SO2,0) PSO2,decode(b.O3,0,0,round(nvl((a.O3-b.O3)/b.O3,0),4)) O3,nvl(a.O3,0) CO3,nvl(b.O3,0) PO3,decode(b.PM25,0,0,round(nvl((a.PM25-b.PM25)/b.PM25,0),4)) PM25,nvl(a.PM25,0) CPM25,nvl(b.PM25,0) PPM25  from (select nvl(nvl(a.mn,c.mn),d.mn) as mn,nvl(nvl(a.name,c.name),d.name) as name,b.CO,b.NO2,b.SO2,b.O3,b.PM25,b.PM10  from (select stationcode,CO,NO2,SO2,O3,PM25,PM10 from t_cal_month where to_char(receivetime,'YYYY-MM') ='"
					+ sstime
					+ "') b  left join base_site c on b.stationcode=c.mn left join t_bas_gkz d on b.stationcode=d.mn left join base_station a on b.stationcode = a.mn where (a.mn in ("+stationString+") or c.mn in("+stationString+") or d.mn in ("+stationString+"))) a left join (select nvl(nvl(a.mn,c.mn),d.mn) as mn,nvl(nvl(a.name,c.name),d.name) as name,b.CO,b.NO2,b.SO2,b.O3,b.PM25,b.PM10 from  (select stationcode,CO,NO2,SO2,O3,PM25,PM10 from t_cal_month where to_char(receivetime,'YYYY-MM') = '"
					+ stime
					+ "') b  left join base_site c on b.stationcode=c.mn left join t_bas_gkz d on b.stationcode=d.mn left join base_station a on b.stationcode = a.mn where (a.mn in ("+stationString+") or c.mn in("+stationString+") or d.mn in ("+stationString+"))) b on a.mn=b.mn ";
			List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
			List<Map<String,Object>> datalist = jdbcTemplate.queryForList(sql);
			for(int i=0,size = datalist.size();i<size;i++){
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("stationName", datalist.get(i).get("name"));
				map.put("mn", datalist.get(i).get("mn"));
				List<String> factor = new ArrayList<String>();
				factor.add("PM10");
				factor.add("PM25");
				factor.add("SO2");
				factor.add("CO");
				factor.add("O3");
				factor.add("NO2");
				map.put("factor", factor);
				List<Double> current = new ArrayList<Double>();
				current.add(Double.parseDouble(datalist.get(i).get("CPM10").toString()));
				current.add(Double.parseDouble(datalist.get(i).get("CPM25").toString()));
				current.add(Double.parseDouble(datalist.get(i).get("CSO2").toString()));
				current.add(Double.parseDouble(datalist.get(i).get("CCO").toString()));
				current.add(Double.parseDouble(datalist.get(i).get("CO3").toString()));
				current.add(Double.parseDouble(datalist.get(i).get("CNO2").toString()));
				map.put("current", current);
				List<Double> pass = new ArrayList<Double>();
				pass.add(Double.parseDouble(datalist.get(i).get("PPM10").toString()));
				pass.add(Double.parseDouble(datalist.get(i).get("PPM25").toString()));
				pass.add(Double.parseDouble(datalist.get(i).get("PSO2").toString()));
				pass.add(Double.parseDouble(datalist.get(i).get("PCO").toString()));
				pass.add(Double.parseDouble(datalist.get(i).get("PO3").toString()));
				pass.add(Double.parseDouble(datalist.get(i).get("PNO2").toString()));
				map.put("pass", pass);
				List<Double> contrast = new ArrayList<Double>();
				contrast.add(Double.parseDouble(datalist.get(i).get("PM10").toString()));
				contrast.add(Double.parseDouble(datalist.get(i).get("PM25").toString()));
				contrast.add(Double.parseDouble(datalist.get(i).get("SO2").toString()));
				contrast.add(Double.parseDouble(datalist.get(i).get("CO").toString()));
				contrast.add(Double.parseDouble(datalist.get(i).get("O3").toString()));
				contrast.add(Double.parseDouble(datalist.get(i).get("NO2").toString()));
				map.put("contrast", contrast);
				list.add(map);
			}
			return list;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object querySearchPointAnalyze(String dataType, String startTime,
			String endTime, String stationString, String userId) {
		List<Map<String, Object>> nslist = null;
		try {
			String sql = "";
			String bString = "";
			String fString = "";
			if(stationString!=null){
				
			
			String[] stationsStrings = stationString.split(",");
			stationString = "";
			for (int i = 0; i < stationsStrings.length; i++) {
				stationString += "'" + stationsStrings[i] + "',";
			}
			if (stationsStrings.length == 0) {
				stationString = "";
			} else {
				stationString = stationString.substring(0,
						stationString.length() - 1);
			}
			}else{
				sql = "select * from Base_STATION where institution_BH = (select institution_BH from XT_YHXX where id = ?)  and   rownum in (1,2)";
				List<Map<String,Object>> list =jdbcTemplate.queryForList(sql,new Object[]{userId});
				stationString = "";
				for (int i = 0,length = list.size(); i < length; i++) {
					if(i!=length-1){
						stationString += "'" + list.get(i).get("mn") + "',";
					}else{
						stationString += "'" + list.get(i).get("mn") + "'";
					}
					
				}
			}
			if ("0".equals(dataType)) {// 小时
				SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
				Date startDate = historydate.parse(startTime);
				Date endDate = historydate.parse(endTime);
				startTime = historydate.format(startDate);
				endTime = historydate.format(endDate);
				fString = "yyyy-mm-dd HH24";
				sql = "select to_char(TO_Date('"
						+ historydate.format(startDate)
						+ "','yyyy-mm-dd HH24')+(ROWNUM-1)/24,'yyyy-mm-dd HH24') AS time from dual CONNECT BY ROWNUM < "
						+ (endDate.getTime() - startDate.getTime()) / 3600000L;
				bString = "t_cal_hour";
			} else if ("1".equals(dataType)) {// 天
				SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
				Date startDate = secondA.parse(startTime);
				Date endDate = secondA.parse(endTime);
				startTime = secondA.format(startDate);
				endTime = secondA.format(endDate);
				fString = "yyyy-mm-dd";
				sql = "select to_char(TO_Date('"
						+ secondA.format(startDate)
						+ "','yyyy-mm-dd')+ROWNUM-1,'yyyy-mm-dd') AS time from dual CONNECT BY ROWNUM < "
						+ (endDate.getTime() - startDate.getTime())
						/ (24 * 3600000L);
				bString = "t_cal_day";
			} else if ("2".equals(dataType)) {// 月
				SimpleDateFormat historymonth=new SimpleDateFormat("yyyy-MM");
				Date startDate = historymonth.parse(startTime);
				Date endDate = historymonth.parse(endTime);
				fString = "yyyy-mm";
				startTime = historymonth.format(startDate);
				endTime = historymonth.format(endDate);
				sql = "select to_char(TO_Date('"
						+ historymonth.format(startDate)
						+ "','yyyy-mm')+(ROWNUM-1)*30,'yyyy-mm') AS time from dual CONNECT BY ROWNUM <"
						+ (endDate.getTime() - startDate.getTime())
						/ (30 * 24 * 3600000L);
				bString = "t_cal_month";
			}
			final List<Object> timeList = new ArrayList<Object>();
			jdbcTemplate.query(sql, new RowCallbackHandler() {
				@Override
				public void processRow(ResultSet rs) throws SQLException {
					timeList.add(rs.getString("time"));
				}
			});
			sql = "select nvl(nvl(a.name,c.name),d.name) as name,max(b.PM10) PM10,max(b.PM25) PM25,max(b.SO2) SO2,max(b.NO2) NO2,max(b.CO) CO,max(b.O3) O3,max(b.AQI) AQI,max(b.TEMPERATURE) temperature,max(b.HUMIDITY) humidity,max(b.GUSTSPEED) speed,to_char(b.receivetime,'"
					+ fString
					+ "') receivetime from  "
					+ bString
					+ " b  left join base_station a on a.mn = b.stationcode left join base_site c on c.mn=b.stationcode left join T_bas_gkz d on d.mn=b.stationcode  where b.receivetime>=to_date('"
					+ startTime
					+ "','"
					+ fString
					+ "') and b.receivetime<=to_date('"
					+ endTime
					+ "','"
					+ fString
					+ "') and (a.mn in ("
					+ stationString
					+ ") or c.mn in ("
					+ stationString
					+ ") or d.mn in ("
					+ stationString
					+ ")) group by nvl(nvl(a.name,c.name),d.name),b.receivetime  order by nvl(nvl(a.name,c.name),d.name),b.receivetime";
			final List<Map<String, Object>> slist = new ArrayList<Map<String, Object>>();
			jdbcTemplate.query(sql, new RowCallbackHandler() {
				@Override
				public void processRow(ResultSet rs) throws SQLException {
					boolean flag = false;
					for (int i = 0,size = slist.size(); i < size; i++) {
						if (slist.get(i).get("name")
								.equals(rs.getString("name"))) {
							flag = true;
							List<Point> pList = (List<Point>) (slist.get(i)
									.get("vlist"));
							for (int j = 0; j < pList.size(); j++) {
								if (pList.get(j).getFactorName().equals("PM10")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("PM10"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("PM2.5")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("PM25"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("SO2")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("SO2"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("NO2")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("NO2"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("CO")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("CO"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("O3")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("O3"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("AQI")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("AQI"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("温度")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("temperature"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("湿度")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("humidity"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("风速")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("speed"));
									pList.get(j).getFactorList().add(nPoint);
								}
							}
						}
					}
					if (!flag) {
						Map<String, Object> sMap = new HashMap<String, Object>();
						List<Point> vlist = new ArrayList<Point>();
						List<NewPoint> factorlist = new ArrayList<NewPoint>();
						sMap.put("name", rs.getString("name"));
						NewPoint nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("PM10"));
						Point point = new Point();
						point.setFactorName("PM10");
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("PM25"));
						point = new Point();
						point.setFactorName("PM2.5");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("SO2"));
						point = new Point();
						point.setFactorName("SO2");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("NO2"));
						point = new Point();
						point.setFactorName("NO2");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("CO"));
						point = new Point();
						point.setFactorName("CO");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("O3"));
						point = new Point();
						point.setFactorName("O3");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("AQI"));
						point = new Point();
						point.setFactorName("AQI");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("temperature"));
						point = new Point();
						point.setFactorName("温度");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("humidity"));
						point = new Point();
						point.setFactorName("湿度");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("speed"));
						point = new Point();
						point.setFactorName("风速");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						sMap.put("vlist", vlist);
						slist.add(sMap);
					}
				}
			});
			nslist = slist;
			for (int j = 0; j < nslist.size(); j++) {
				List<Point> list = (List<Point>) (nslist.get(j).get("vlist"));
				for (int z = 0; z < list.size(); z++) {
					List<NewPoint> nplist = list.get(z).getFactorList();
					for (int i = 0; i < timeList.size(); i++) {
						boolean flag = false;
						for (int k = 0; k < nplist.size(); k++) {
							if (nplist.get(k).getTime().equals(timeList.get(i))) {
								flag = true;
							}
						}
						if (!flag) {
							NewPoint nPoint = new NewPoint();
							nPoint.setTime(timeList.get(i).toString());
							list.get(z).getFactorList().add(nPoint);
						}
					}
				}
			}
			for (int i = 0,size = nslist.size(); i < size; i++) {
				List<Point> pList = (List<Point>) (nslist.get(i).get("vlist"));
				for (int j = 0; j < pList.size(); j++) {
					Collections.sort((pList.get(j).getFactorList()));
				}

			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return nslist;
	}

	@Override
	public Object queryPointData(String dataType, String userId,
			String pageNum, String modelType) {
		String sql = "";
		String bString = "";
		String fString = "";
		sql = "select a.mn \"mn\",a.name \"name\",b.mc as \"grid\"  from T_bas_gkz a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')";
		}
		List<Map<String, Object>> gkzList = new ArrayList<Map<String, Object>>();
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_station a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')";
		}
		List<Map<String, Object>> sList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_site a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')";
		}
		List<Map<String, Object>> siteList = jdbcTemplate.queryForList(sql);
		String stationString = "";
		for (int i = 0; i < siteList.size(); i++) {
			stationString += "'" + siteList.get(i).get("mn") + "',";
		}
		for (int i = 0; i < sList.size(); i++) {
			stationString += "'" + sList.get(i).get("mn") + "',";
		}
		for (int i = 0; i < gkzList.size(); i++) {
			stationString += "'" + gkzList.get(i).get("mn") + "',";
		}
		if (!"".equals(stationString)) {
			stationString = stationString.substring(0,
					stationString.length() - 1);
		}
		String endTime = "";
		String startTime = "";
		if ("0".equals(dataType)) {// 分钟
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.HOUR, -4);
			SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
			startTime = historydate.format(c.getTime());
			endTime = historydate.format(date);
			SimpleDateFormat month=new SimpleDateFormat("yyyyMM");
			bString = "t_cal_minutes_" + month.format(date);
			fString = "yyyy-mm-dd HH24:mi";
		} else if ("1".equals(dataType)) {// 小时
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.HOUR, -48);
			SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
			startTime = historydate.format(c.getTime());
			endTime = historydate.format(date);
			bString = "t_cal_hour";
			fString = "yyyy-mm-dd HH24";
		} else if ("2".equals(dataType)) {// 天
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.MONTH, -1);
			SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
			startTime = secondA.format(c.getTime());
			endTime = secondA.format(date);
			bString = "t_cal_day";
			fString = "yyyy-mm-dd";
		}
		final List<Object> timeList = new ArrayList<Object>();
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				timeList.add(rs.getString("time"));
			}
		});
		sql = "select name,round(avg(PM10),0) PM10,round(avg(PM25),0) PM25,round(avg(SO2),0) SO2,round(avg(NO2),0) NO2,round(avg(CO),0) CO,round(avg(O3),0) O3,round(avg(AQI),0) AQI,round(avg(TEMPERATURE),0) \"temperature\",round(avg(HUMIDITY),0) \"humidity\",round(avg(GUSTSPEED),0) \"speed\",round(avg(GUSTDIR),0) \"speedDir\",round(avg(pressure),0) \"pressure\",to_char(receivetime,'"
				+ fString
				+ "') receivetime from (select nvl(nvl(a.name,c.name),d.name) as name,max(b.PM10) PM10,max(b.PM25) PM25,max(b.SO2) SO2,max(b.NO2) NO2,max(b.CO) CO,max(b.O3) O3,max(b.AQI) AQI,max(b.TEMPERATURE) TEMPERATURE,max(b.HUMIDITY) HUMIDITY,max(b.GUSTSPEED) GUSTSPEED,max(b.GUSTDIR) GUSTDIR,max(b.pressure) pressure,b.receivetime from  "
				+ bString
				+ " b  left join base_station a on a.mn = b.stationcode left join base_site c on c.mn=b.stationcode left join T_bas_gkz d on d.mn=b.stationcode  where b.receivetime>=to_date('"
				+ startTime
				+ "','"
				+ fString
				+ "') and b.receivetime<=to_date('"
				+ endTime
				+ "','"
				+ fString
				+ "') and (a.mn in ("
				+ stationString
				+ ") or c.mn in ("
				+ stationString
				+ ") or d.mn in ("
				+ stationString
				+ ")) group by b.receivetime,nvl(nvl(a.name,c.name),d.name) order by b.receivetime) group by to_char(receivetime,'"
				+ fString + "'),name order by name,receivetime";
		List<Map<String, Object>> slist = jdbcTemplate.queryForList(sql);
		return slist;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object queryDataAnalysis(String dataType, String startTime,
			String endTime, String stationString, String userId) {
		List<Map<String, Object>> nslist = null;
		try {
			String sql = "";
			String bString = "";
			String fString = "";
			if(stationString!=null){
			String[] stationsStrings = stationString.split(",");
			stationString = "";
			for (int i = 0; i < stationsStrings.length; i++) {
				stationString += "'" + stationsStrings[i] + "',";
			}
			if (stationsStrings.length == 0) {
				stationString = "";
			} else {
				stationString = stationString.substring(0,
						stationString.length() - 1);
			}
			}else{
				sql = "select * from Base_STATION where institution_BH = (select institution_BH from XT_YHXX where id = ?)  and   rownum in (1,2)";
				List<Map<String,Object>> list =jdbcTemplate.queryForList(sql,new Object[]{userId});
				stationString = "";
				for (int i = 0,length = list.size(); i < length; i++) {
					if(i!=length-1){
						stationString += "'" + list.get(i).get("mn") + "',";
					}else{
						stationString += "'" + list.get(i).get("mn") + "'";
					}
					
				}
			}
			if ("1".equals(dataType)) {// 小时
				SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
				Date startDate = historydate.parse(startTime);
				Date endDate = historydate.parse(endTime);
				startTime = historydate.format(startDate);
				endTime = historydate.format(endDate);
				fString = "yyyy-mm-dd HH24";
				sql = "select to_char(TO_Date('"
						+ historydate.format(startDate)
						+ "','yyyy-mm-dd HH24')+(ROWNUM-1)/24,'yyyy-mm-dd HH24') AS time from dual CONNECT BY ROWNUM < "
						+ (endDate.getTime() - startDate.getTime()) / 3600000L;
				bString = "t_cal_hour";
			} else if ("2".equals(dataType)) {// 天
				SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
				Date startDate = secondA.parse(startTime);
				Date endDate = secondA.parse(endTime);
				startTime = secondA.format(startDate);
				endTime = secondA.format(endDate);
				fString = "yyyy-mm-dd";
				sql = "select to_char(TO_Date('"
						+ secondA.format(startDate)
						+ "','yyyy-mm-dd')+ROWNUM-1,'yyyy-mm-dd') AS time from dual CONNECT BY ROWNUM < "
						+ (endDate.getTime() - startDate.getTime())
						/ (24 * 3600000L);
				bString = "t_cal_day";
			} else if ("3".equals(dataType)) {// 月
				SimpleDateFormat historymonth=new SimpleDateFormat("yyyy-MM");
				Date startDate = historymonth.parse(startTime);
				Date endDate = historymonth.parse(endTime);
				fString = "yyyy-mm";
				startTime = historymonth.format(startDate);
				endTime = historymonth.format(endDate);
				// startTime = startTime+"-01";
				// endTime = endTime+"-01";
				sql = "select to_char(TO_Date('"
						+ historymonth.format(startDate)
						+ "','yyyy-mm')+(ROWNUM-1)*30,'yyyy-mm') AS time from dual CONNECT BY ROWNUM <"
						+ (endDate.getTime() - startDate.getTime())
						/ (30 * 24 * 3600000L);
				bString = "t_cal_month";
			}
			final List<Object> timeList = new ArrayList<Object>();
			jdbcTemplate.query(sql, new RowCallbackHandler() {
				@Override
				public void processRow(ResultSet rs) throws SQLException {
					timeList.add(rs.getString("time"));
				}
			});
			sql = "select name,round(avg(PM10),0) PM10,round(avg(PM25),0) PM25,round(avg(SO2),0) SO2,round(avg(NO2),0) NO2,round(avg(CO),0) CO,round(avg(O3),0) O3,to_char(receivetime,'"
					+ fString
					+ "') receivetime from (select nvl(nvl(a.name,c.name),d.name) as name,max(b.PM10) PM10,max(b.PM25) PM25,max(b.SO2) SO2,max(b.NO2) NO2,max(b.CO) CO,max(b.O3) O3,b.receivetime from  "
					+ bString
					+ " b  left join base_station a on a.mn = b.stationcode left join base_site c on c.mn=b.stationcode left join T_bas_gkz d on d.mn=b.stationcode  where b.receivetime>=to_date('"
					+ startTime
					+ "','"
					+ fString
					+ "') and b.receivetime<=to_date('"
					+ endTime
					+ "','"
					+ fString
					+ "') and (a.mn in ("
					+ stationString
					+ ") or c.mn in ("
					+ stationString
					+ ") or d.mn in ("
					+ stationString
					+ ")) group by b.receivetime,nvl(nvl(a.name,c.name),d.name) order by b.receivetime) group by to_char(receivetime,'"
					+ fString + "'),name order by name,receivetime";
			final List<Map<String, Object>> slist = new ArrayList<Map<String, Object>>();
			jdbcTemplate.query(sql, new RowCallbackHandler() {
				@Override
				public void processRow(ResultSet rs) throws SQLException {
					boolean flag = false;
					for (int i = 0; i < slist.size(); i++) {
						if (slist.get(i).get("name")
								.equals(rs.getString("name"))) {
							flag = true;
							List<Point> pList = (List<Point>) (slist.get(i)
									.get("vlist"));
							for (int j = 0; j < pList.size(); j++) {
								if (pList.get(j).getFactorName().equals("PM10")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("PM10"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("PM2.5")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("PM25"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("SO2")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("SO2"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("NO2")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("NO2"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("CO")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("CO"));
									pList.get(j).getFactorList().add(nPoint);
								} else if (pList.get(j).getFactorName()
										.equals("O3")) {
									NewPoint nPoint = new NewPoint();
									nPoint.setTime(rs.getString("receivetime"));
									nPoint.setValue(rs.getDouble("O3"));
									pList.get(j).getFactorList().add(nPoint);
								}
							}
						}
					}
					if (!flag) {
						Map<String, Object> sMap = new HashMap<String, Object>();
						List<Point> vlist = new ArrayList<Point>();
						List<NewPoint> factorlist = new ArrayList<NewPoint>();
						sMap.put("name", rs.getString("name"));
						NewPoint nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("PM10"));
						Point point = new Point();
						point.setFactorName("PM10");
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("PM25"));
						point = new Point();
						point.setFactorName("PM2.5");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("SO2"));
						point = new Point();
						point.setFactorName("SO2");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("NO2"));
						point = new Point();
						point.setFactorName("NO2");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("CO"));
						point = new Point();
						point.setFactorName("CO");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						nPoint = new NewPoint();
						nPoint.setTime(rs.getString("receivetime"));
						nPoint.setValue(rs.getDouble("O3"));
						point = new Point();
						point.setFactorName("O3");
						factorlist = new ArrayList<NewPoint>();
						factorlist.add(nPoint);
						point.setFactorList(factorlist);
						vlist.add(point);

						sMap.put("vlist", vlist);
						slist.add(sMap);
					}
				}
			});
			nslist = slist;
			for (int j = 0; j < nslist.size(); j++) {
				List<Point> list = (List<Point>) (nslist.get(j).get("vlist"));
				for (int z = 0; z < list.size(); z++) {
					List<NewPoint> nplist = list.get(z).getFactorList();
					for (int i = 0; i < timeList.size(); i++) {
						boolean flag = false;
						for (int k = 0; k < nplist.size(); k++) {
							if (nplist.get(k).getTime().equals(timeList.get(i))) {
								flag = true;
							}
						}
						if (!flag) {
							NewPoint nPoint = new NewPoint();
							nPoint.setTime(timeList.get(i).toString());
							list.get(z).getFactorList().add(nPoint);
						}
					}
				}
			}
			for (int i = 0; i < nslist.size(); i++) {
				List<Point> pList = (List<Point>) (nslist.get(i).get("vlist"));
				for (int j = 0; j < pList.size(); j++) {
					Collections.sort((pList.get(j).getFactorList()));
				}

			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return nslist;
	}

	@Override
	public Object queryPointAnalyze(String factorType, String time,
			String stationString,String userId) {
		if(stationString!=null){
			String[] stationsStrings = stationString.split(",");
			stationString = "";
			for (int i = 0; i < stationsStrings.length; i++) {
				stationString += "'" + stationsStrings[i] + "',";
			}
			if (stationsStrings.length == 0) {
				stationString = "";
			} else {
				stationString = stationString.substring(0,
						stationString.length() - 1);
			}
		}else{
			String sql = "select * from Base_STATION where institution_BH = (select institution_BH from XT_YHXX where id = ?)  and   rownum in (1,2)";
			List<Map<String,Object>> list =jdbcTemplate.queryForList(sql,new Object[]{userId});
			stationString = "";
			for (int i = 0,length = list.size(); i < length; i++) {
				if(i!=length-1){
					stationString += "'" + list.get(i).get("mn") + "',";
				}else{
					stationString += "'" + list.get(i).get("mn") + "'";
				}
			}
		}
		final List<Map<String, Object>> fList = new ArrayList<Map<String, Object>>();
		final String factor = factorType;
		String sql = "select name,round(avg("
				+ factorType
				+ "),0) "
				+ factorType
				+ ",to_char(receivetime,'HH24') receivetime from (select nvl(nvl(a.name,c.name),d.name) as name,max(b."
				+ factorType
				+ ") "
				+ factorType
				+ ",b.receivetime from t_cal_hour b left join base_station a on a.mn = b.stationcode left join base_site c on b.stationCode=c.mn left join T_bas_gkz d on d.mn=b.stationcode  where to_char(b.receivetime,'YYYY-MM-DD HH24') like '%"
				+ time
				+ "%'  and (a.mn in ("
				+ stationString
				+ ") or c.mn in ("
				+ stationString
				+ ") or d.mn in ("
				+ stationString
				+ ")) group by b.receivetime,nvl(nvl(a.name,c.name),d.name) order by b.receivetime) group by to_char(receivetime,'HH24'),name order by name,receivetime";
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@SuppressWarnings("unchecked")
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				boolean flag = false;
				for (int i = 0; i < fList.size(); i++) {
					if (fList.get(i).get("name").equals(rs.getString("NAME"))) {
						flag = true;
						Map<String, Object> vMap = new HashMap<String, Object>();
						vMap.put("time", rs.getString("receivetime") + ":00");
						vMap.put("value", rs.getDouble(factor));
						((List<Map<String, Object>>) (fList.get(i).get("vlist")))
								.add(vMap);
					}
				}
				if (!flag) {
					Map<String, Object> fMap = new HashMap<String, Object>();
					fMap.put("name", rs.getString("NAME"));
					Map<String, Object> vMap = new HashMap<String, Object>();
					vMap.put("time", rs.getString("receivetime") + ":00");
					vMap.put("value", rs.getDouble(factor));
					List<Map<String, Object>> vList = new ArrayList<Map<String, Object>>();
					vList.add(vMap);
					fMap.put("vlist", vList);
					fList.add(fMap);
				}
			}
		});
		sql = "select to_char(TO_Date('2017-08-03 00','yyyy-mm-dd HH24')+(ROWNUM-1)/24,'HH24:mi') AS time from dual   CONNECT BY ROWNUM <=24";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		for (int i = 0; i < fList.size(); i++) {
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> vList = (List<Map<String, Object>>) fList
					.get(i).get("vlist");
			int z = 0;
			List<Map<String, Object>> nvList = new ArrayList<Map<String, Object>>();
			for (int j = 0; j < list.size(); j++) {
				Map<String, Object> vMap = new HashMap<String, Object>();
				if (vList.size() > z
						&& (vList.size() != 0 && vList.get(z).get("time")
								.equals(list.get(j).get("time")))) {
					vMap.put("time", vList.get(z).get("time"));
					vMap.put("value", vList.get(z).get("value"));
					nvList.add(vMap);
					z++;
				} else {
					vMap.put("time", list.get(j).get("time"));
					vMap.put("value", null);
					nvList.add(vMap);
				}
			}
			fList.get(i).put("vlist", nvList);
		}
		return fList;
	}

	@Override
	public Object queryHisDataRank(String factorType, String time,
			String historytype, String userId) {
		List<FactorRank> frlist = new ArrayList<FactorRank>();
		final List<FactorRank> mlist = new ArrayList<FactorRank>();
		final String factorString = factorType;
		try {
			SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
			SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
			Date dta = (Integer.parseInt(historytype) == 1) ? secondA
					.parse(time) : historydate.parse(time);
			time = (Integer.parseInt(historytype) == 1) ? secondA
					.format(dta) : historydate.format(dta);
			String timetypeString = (Integer.parseInt(historytype) == 1) ? "YYYY-MM-DD"
					: "YYYY-MM-DD HH24";
			String sql = "select a.*,B.NAME AS STATIONNAME,B.LNT AS STATIONLNT,B.LAT AS STATIONLAT,C.NAME AS SITENAME,C.LNT AS SITELNT,C.LAT AS SITELAT,D.NAME AS GKZNAME,D.LNT AS GKZLNT,D.LAT AS GKZLAT,B.ADDRESS AS BADD,C.ADDRESS AS CADD,D.ADDRESS AS DADD,e.recieve_time  from "
					+ (Integer.parseInt(historytype) == 1 ? "T_cal_day"
							: "T_cal_hour")
					+ " A  LEFT JOIN BASE_STATION B ON A.stationCode=B.MN LEFT JOIN BASE_SITE C ON A.stationCode=C.MN LEFT JOIN T_BAS_GKZ D ON A.stationCode=D.MN  left join data_real e on a.stationCode=e.mn where  to_char(a.receivetime,'"
					+ timetypeString + "') like '%" + time + "%'";
			if (userId != null) {
				String usql = "select INSTITUTION_BH from XT_YHXX where id='"
						+ userId + "'";
				List<Map<String, Object>> list = jdbcTemplate
						.queryForList(usql);
				if (list != null && list.size() != 0) {
					sql += " and ( b.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"
							+ userId
							+ "') or c.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"
							+ userId
							+ "') or  d.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"
							+ userId + "') )";
				}
			}
			jdbcTemplate.query(sql, new RowCallbackHandler() {
				@Override
				public void processRow(ResultSet rs) throws SQLException {
					FactorRank factorRank = new FactorRank();
					Calendar c1 = Calendar.getInstance();
					factorRank.setOnline(c1.getTimeInMillis()
							- rs.getTimestamp("RECIEVE_TIME").getTime() <= (3*60*60*1000));
					factorRank.setMn(rs.getString("stationcode"));
					if ("PM25".equals(factorString)) {
						factorRank.setFactorName("PM2.5");
						factorRank.setValue(rs.getDouble("PM25"));
						factorRank.setFactorTypeId(2);
					} else if ("PM10".equals(factorString)) {
						factorRank.setFactorName("PM10");
						factorRank.setValue(rs.getDouble("PM10"));
						factorRank.setFactorTypeId(1);
					} else if ("CO".equals(factorString)) {
						factorRank.setFactorName("CO");
						factorRank.setValue(rs.getDouble("CO"));
						factorRank.setFactorTypeId(5);
					} else if ("O3".equals(factorString)) {
						factorRank.setFactorName("O3");
						factorRank.setValue(rs.getDouble("O3"));
						factorRank.setFactorTypeId(6);
					} else if ("SO2".equals(factorString)) {
						factorRank.setFactorName("SO2");
						factorRank.setValue(rs.getDouble("SO2"));
						factorRank.setFactorTypeId(3);
					} else if ("NO2".equals(factorString)) {
						factorRank.setFactorName("NO2");
						factorRank.setValue(rs.getDouble("NO2"));
						factorRank.setFactorTypeId(4);
					} else if ("AQI".equals(factorString)) {
						factorRank.setFactorName("AQI");
						factorRank.setValue(rs.getDouble("AQI"));
						factorRank.setFactorTypeId(0);
					}
					if (rs.getString("STATIONNAME") != null) {
						factorRank.setStationName(rs.getString("STATIONNAME"));
						factorRank.setStationType(0);
					} else if (rs.getString("SITENAME") != null) {
						factorRank.setStationName(rs.getString("SITENAME"));
						factorRank.setStationType(1);
					} else if (rs.getString("GKZNAME") != null) {
						factorRank.setStationName(rs.getString("GKZNAME"));
						factorRank.setStationType(2);
					}
					mlist.add(factorRank);
				}
			});
			frlist = mlist;
			Collections.sort(frlist);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return frlist;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object queryStationTree(String userId) {
		String nsql = "select a.id,a.mn,a.name,a.video_xh,b.mn gkmn,b.name gkname,b.video_xh gkvideo_xh,b.id gkid from base_station a full join t_bas_gkz b on a.fmn = b.mn ";
		if (userId != null) {
			nsql += "where (a.institution_bh = (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId
					+ "') or b.institution_bh = (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')) ";
		}
		nsql += " order by a.fmn";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(nsql);
		List<Map<String, Object>> l = new ArrayList<Map<String, Object>>();
		Map<String, Object> map1 = null, map2 = null;
		Map<String, Object> m = new HashMap<String, Object>();// 记录syname是否存在
		for (Map<String, Object> map : list) {
			String id = (String) map.get("ID");
			String gkid = (String) map.get("GKID");
			String gkname = (String) map.get("GKNAME");
			String name = (String) map.get("NAME");
			String bh = (String) map.get("MN");
			String gkbh = (String) map.get("GKMN");
			if (m.get(gkname) == null) {
				map1 = new HashMap<String, Object>();
				map2 = new HashMap<String, Object>();
				List<Map<String, Object>> l1 = new ArrayList<Map<String, Object>>();
				if (gkname == null) {
					map2.put("id", id);
					map2.put("name", name);
					map2.put("mn", bh);
					l.add(map2);
				} else {
					map1.put("id", gkid);
					map1.put("name", gkname);
					map1.put("mn", gkbh);
					if (name != null || bh != null) {
						map2.put("id", id);
						map2.put("name", name);
						map2.put("mn", bh);
						l1 = new ArrayList<Map<String, Object>>();
						l1.add(map2);
						map1.put("stationList", l1);
					}
					m.put(gkname, map1);
					l.add(map1);
				}
			} else {
				List<Map<String, Object>> l1 = (List<Map<String, Object>>) ((Map<String, Object>) m
						.get(gkname)).get("stationList");
				map1 = new HashMap<String, Object>();
				map1.put("id", id);
				map1.put("name", name);
				map1.put("mn", bh);
				l1.add(map1);
			}
		}
		return l;
	}

	@Override
	public Object queryWarningData(String userId, String startTime,
			String endTime) {
		try {
			SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
			 SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String sql = "select a.*,nvl(b.name,nvl(c.name,d.name)) name,nvl(f.mn,nvl(g.mn,h.mn)) fmn from event_record a left join Base_station b on a.mn=b.mn left join Base_site e on b.fmn=e.mn left join T_Bas_gkz f on e.fmn=f.mn left join T_bas_gkz h on e.fmn=h.mn left join Base_site c on a.mn=c.mn left join T_Bas_gkz g on c.fmn=g.mn left join T_Bas_gkz d on a.mn=d.mn where (b.institution_bh=(select institution_bh  from XT_yhxx where id=?) or c.institution_bh=(select institution_bh  from XT_yhxx where id=?) or d.institution_bh=(select institution_bh  from XT_yhxx where id=?)) and to_char(a.receivetime,'YYYY-MM-DD HH24:mi:ss') between ? and ? order by a.mn";
			List<Map<String, Object>> alllist = jdbcTemplate.queryForList(
					sql,
					new Object[] {
							userId,
							userId,
							userId,
							secondG.format(secondA
									.parse(startTime)),
							secondG.format(secondA
									.parse(endTime)) });
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < alllist.size(); i++) {
				List<Map<String, Object>> datalist = new ArrayList<Map<String, Object>>();
				if (alllist.get(i).get("fmn") != null) {
					SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
					sql = "select "
							+ alllist.get(i).get("factor")
							+ " from T_CAL_HOUR where stationcode=? and to_char(receivetime,'YYYY-MM-DD HH24:mi:ss')  like '"
							+ historydate.format(alllist.get(i).get(
									"receivetime")) + "%'";
					datalist = jdbcTemplate.queryForList(sql,
							new Object[] { alllist.get(i).get("fmn") });
				}
				boolean flag = false;
				for (int j = 0; j < list.size(); j++) {
					if (list.get(j).get("mn").equals(alllist.get(i).get("mn"))) {
						if (datalist.size() != 0) {
							if (Double.parseDouble(alllist.get(i).get("value")
									.toString()) >= (Double
									.parseDouble(datalist.get(0)
											.get(alllist.get(i).get("factor"))
											.toString()) * 2)) {
								list.get(j)
										.put("over100",
												Integer.parseInt(list.get(j)
														.get("over100")
														.toString()) + 1);
							}
							if (Double.parseDouble(alllist.get(i).get("value")
									.toString()) >= (Double
									.parseDouble(datalist.get(0)
											.get(alllist.get(i).get("factor"))
											.toString()) * 2.5)) {
								list.get(j)
										.put("over150",
												Integer.parseInt(list.get(j)
														.get("over150")
														.toString()) + 1);
							}
							if (Double.parseDouble(alllist.get(i).get("value")
									.toString()) >= (Double
									.parseDouble(datalist.get(0)
											.get(alllist.get(i).get("factor"))
											.toString()) * 3)) {
								list.get(j)
										.put("over200",
												Integer.parseInt(list.get(j)
														.get("over200")
														.toString()) + 1);
							}
						} else {
							list.get(j).put(
									"over100",
									Integer.parseInt(list.get(j).get("over100")
											.toString()) + 1);
							list.get(j).put(
									"over150",
									Integer.parseInt(list.get(j).get("over150")
											.toString()) + 1);
							list.get(j).put(
									"over200",
									Integer.parseInt(list.get(j).get("over200")
											.toString()) + 1);
						}
						flag = true;
					}
				}
				if (!flag) {
					if (datalist.size() != 0) {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("stationName", alllist.get(i).get("name"));
						map.put("mn", alllist.get(i).get("mn"));
						if (Double.parseDouble(alllist.get(i).get("value")
								.toString()) >= (Double.parseDouble(datalist
								.get(0).get(alllist.get(i).get("factor"))
								.toString()) * 2)) {
							map.put("over100", 1);
						} else {
							map.put("over100", 0);
						}
						if (Double.parseDouble(alllist.get(i).get("value")
								.toString()) >= (Double.parseDouble(datalist
								.get(0).get(alllist.get(i).get("factor"))
								.toString()) * 2.5)) {
							map.put("over150", 1);
						} else {
							map.put("over150", 0);
						}
						if (Double.parseDouble(alllist.get(i).get("value")
								.toString()) >= (Double.parseDouble(datalist
								.get(0).get(alllist.get(i).get("factor"))
								.toString()) * 3)) {
							map.put("over200", 1);
						} else {
							map.put("over200", 0);
						}
						list.add(map);
					} else {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("stationName", alllist.get(i).get("name"));
						map.put("mn", alllist.get(i).get("mn"));
						map.put("over100", 1);
						map.put("over150", 1);
						map.put("over200", 1);
						list.add(map);
					}
				}

			}
			return list;
		} catch (DataAccessException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object queryHeatData(String userId, String mn, String time,
			String factorType) {
		try {
			if(mn==null){
				String sql = "select * from Base_STATION where institution_BH = (select institution_BH from XT_YHXX where id = ?)  and   rownum in (1)";
				List<Map<String,Object>> list =jdbcTemplate.queryForList(sql,new Object[]{userId});
				mn = list.get(0).get("mn").toString();
			}
			SimpleDateFormat historymonth = new SimpleDateFormat("yyyy-MM");
			Date date = historymonth.parse(time);
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			int num = c.getActualMaximum(Calendar.DAY_OF_MONTH);
			Date da = new Date();
			String now = historymonth.format(da);
			if (now.equals(time)) {
				c.setTime(da);
				num = c.get(Calendar.DAY_OF_MONTH);
			}
			if (date.getTime() > da.getTime()) {
				num = 0;
			}
			String sql = "select a.name \"name\" from base_site a where a.mn=? union select b.name \"name\" from base_station b where  b.mn=? union select c.name \"name\" from  T_Bas_GKZ c  where c.mn=? ";
			List<Map<String, Object>> namelist = jdbcTemplate.queryForList(sql,new Object[]{mn,mn,mn});
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for (int i = 1; i <= num; i++) {
				sql = "Select to_char(b.time,'YYYY-MM-DD HH24:mi:ss') \"time\",nvl(a."
						+ factorType
						+ ",0) \"value\" from  (select TO_Date('"
						+ time
						+ "-"
						+ (String.valueOf(i).length() == 1 ? "0" + i : i)
						+ "','yyyy-mm-dd HH24')+(ROWNUM-1)/24 AS time from dual CONNECT BY ROWNUM <=24) b left join T_CAL_HOUr a on (a.receivetime=b.time and a.stationcode=?)";
				List<Map<String, Object>> monthlist = jdbcTemplate
						.queryForList(sql, mn);
				Map<String, Object> map = new HashMap<String,Object>();
				map.put("vlist",monthlist);
				map.put("name",namelist.get(0).get("name"));
				list.add(map);
			}
			return list;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
