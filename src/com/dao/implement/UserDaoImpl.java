package com.dao.implement;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dao.UserDao;
import com.model.MapInfo;
import com.model.User;
import com.util.HttpUtil;

@Transactional
@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public User findUserByUserName(String userName) {
		String sqlStr = "select a.*,b.MC from XT_YHXX A LEFT JOIN WGH_WGHF B on a.wgbh=b.bh where A.ZH=?";
		final User user = new User();
		jdbcTemplate.query(sqlStr, new Object[] { userName },
				new RowCallbackHandler() {
					@Override
					public void processRow(ResultSet rs) throws SQLException {
						user.setAccount(rs.getString("ZH"));
						user.setPassword(rs.getString("MM"));
						user.setRealName(rs.getString("XM"));
						user.setGrid(rs.getString("MC"));
						user.setUserId(rs.getString("ID"));
						user.setLat(rs.getDouble("LAT"));
						user.setLnt(rs.getDouble("LNT"));
						user.setAddress(rs.getString("DZ"));
						user.setPhone(rs.getString("SJ"));
					}
				});
		return user;
	}

	@Override
	public List<User> findAllUser(String userId) {
		String sqlStr = "SELECT a.*,b.mc FROM XT_YHXX a left join WGH_WGHF B on a.WGBH=b.bh left join XT_YHXX c on a.INSTITUTION_BH=c.INSTITUTION_BH";
		if(userId!=null){
			sqlStr +=" where c.id='"+userId+"'";
		}
		final List<User> users = new ArrayList<User>();
		jdbcTemplate.query(sqlStr, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				User user = new User();
				user.setAccount(rs.getString("ZH"));
				user.setPassword(rs.getString("MM"));
				user.setRealName(rs.getString("XM"));
				user.setGrid(rs.getString("MC"));
				user.setUserId(rs.getString("ID"));
				user.setLat(rs.getDouble("LAT"));
				user.setLnt(rs.getDouble("LNT"));
				user.setAddress(rs.getString("DZ"));
				user.setPhone(rs.getString("SJ"));
				users.add(user);
			}
		});
		return users;
	}

	@Override
	public List<User> queryPersonPlace(String userId) {
		String sqlStr = "SELECT a.*,b.mc FROM XT_YHXX a left join WGH_WGHF B on a.WGBH=b.bh left join XT_YHXX c on a.INSTITUTION_BH=c.INSTITUTION_BH";
		if(userId!=null){
			sqlStr +=" where c.id='"+userId+"'";
		}
		// return jdbcTemplate.query(sqlStr, new
		// BeanPropertyRowMapper<User>(User.class));
		final List<User> users = new ArrayList<User>();
		jdbcTemplate.query(sqlStr, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				User user = new User();
				user.setUpDateTime(rs.getDate("UPDATETM")==null?"":secondG.format(rs.getDate("UPDATETM")));
				user.setAccount(rs.getString("ZH"));
				user.setPassword(rs.getString("MM"));
				user.setRealName(rs.getString("XM"));
				user.setGrid(rs.getString("MC"));
				user.setUserId(rs.getString("ID"));
				user.setLat(rs.getDouble("LAT"));
				user.setLnt(rs.getDouble("LNT"));
				user.setAddress(rs.getString("DZ"));
				user.setPhone(rs.getString("SJ"));
				String sql = "select distinct a.* from event_record a  where dispatcher='"
						+ rs.getString("ID") + "' and a.flag = 2";
				final List<Map<String, Object>> missionList = new ArrayList<Map<String, Object>>();
				jdbcTemplate.query(sql, new RowCallbackHandler() {
					@Override
					public void processRow(ResultSet rs1) throws SQLException {
						SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("id", rs1.getString("id"));
						map.put("title", rs1.getString("eventname"));
						map.put("startTime",
								rs1.getTimestamp("receivetime") == null ? ""
										: secondG.format(rs1
												.getTimestamp("receivetime")));
						map.put("discrib", rs1.getString("descriptions"));
						missionList.add(map);
					}
				});
				user.setMissionList(missionList);
				users.add(user);
			}
		});
		return users;
	}

	@Override
	public int queryPlace(String lnt, String lat, String userId, String address,String area) {
		SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sqlStr = "update XT_YHXX set lnt=?,lat=?,DZ=?,txdz=?,updatetm=to_date('"+secondG.format(new Date())+"','YYYY-MM-DD HH24:mi:ss') where id=?";
		int result = jdbcTemplate.update(sqlStr, lnt, lat, address,area ,userId);
		sqlStr = "insert into WGH_TRACK (lnt,lat,address,tm,userId)values ('"+lnt+"','"+lat+"','"+address+"',to_date('"+secondG.format(new Date())+"','YYYY-MM-DD HH24:mi:ss'),'"+userId+"')";
		result+=jdbcTemplate.update(sqlStr);
		return result;
	}

	@Override
	public List<Map<String, Object>> queryPollutionStation(String userId) {
		String sqlStr = "SELECT distinct a.*,b.mc from base_site a left join WGH_WGHF b on a.area_bh=b.bh  left join XT_YHXX c on a.INSTITUTION_BH=c.INSTITUTION_BH";
		if(userId!=null){
			sqlStr +=" where c.id='"+userId+"'";
		}
		List<Map<String, Object>> pList = new ArrayList<Map<String, Object>>();
		pList = jdbcTemplate.queryForList(sqlStr);
		return pList;
	}

	@Override
	public Object queryGrid(String userId) {
		String sqlStr = "SELECT A.ID,A.MC,A.BH,A.FBH,B.ID as pid,B.XM FROM WGH_WGHF A LEFT JOIN XT_YHXX B ON A.BH=B.WGBH  left join XT_YHXX c on a.INSTITUTION_BH=c.INSTITUTION_BH WHERE A.DH=4";
		if(userId!=null){
			sqlStr +=" and c.id='"+userId+"'";
		}
		final List<Map<String, Object>> communityList = new ArrayList<Map<String, Object>>();
		jdbcTemplate.query(sqlStr, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> mapbean;
				mapbean = new HashMap<String, Object>();
				mapbean.put("id", rs.getString("ID"));
				mapbean.put("name", rs.getString("MC"));
				mapbean.put("bh", rs.getString("BH"));
				mapbean.put("fbh", rs.getString("fbh"));
				mapbean.put("psonId",
						rs.getString("pid") == null ? "" : rs.getString("pid"));
				mapbean.put("psonName",
						rs.getString("xm") == null ? "" : rs.getString("xm"));
				communityList.add(mapbean);
			}
		});
		sqlStr = "SELECT  A.ID,A.MC,A.BH,A.FBH,B.ID as pid,B.XM FROM WGH_WGHF A LEFT JOIN XT_YHXX B ON A.BH=B.WGBH  left join XT_YHXX c on a.INSTITUTION_BH=c.INSTITUTION_BH WHERE A.DH=3";
		if(userId!=null){
			sqlStr +=" and c.id='"+userId+"'";
		}
		final List<Map<String, Object>> quList = new ArrayList<Map<String, Object>>();
		jdbcTemplate.query(sqlStr, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> mapbean;
				mapbean = new HashMap<String, Object>();
				mapbean.put("id", rs.getString("ID"));
				mapbean.put("name", rs.getString("MC"));
				mapbean.put("bh", rs.getString("BH"));
				mapbean.put("fbh", rs.getString("fbh"));
				mapbean.put("psonId",
						rs.getString("pid") == null ? "" : rs.getString("pid"));
				mapbean.put("psonName",
						rs.getString("xm") == null ? "" : rs.getString("xm"));
				quList.add(mapbean);
			}
		});
		sqlStr = "SELECT A.ID,A.MC,A.BH,A.FBH,B.ID as pid,B.XM FROM WGH_WGHF A LEFT JOIN XT_YHXX B ON A.BH=B.WGBH  left join XT_YHXX c on a.INSTITUTION_BH=c.INSTITUTION_BH WHERE A.DH=2";
		if(userId!=null){
			sqlStr +=" and c.id='"+userId+"'";
		}
		final List<Map<String, Object>> streetList = new ArrayList<Map<String, Object>>();
		jdbcTemplate.query(sqlStr, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> mapbean;
				mapbean = new HashMap<String, Object>();
				mapbean.put("id", rs.getString("ID"));
				mapbean.put("name", rs.getString("MC"));
				mapbean.put("bh", rs.getString("BH"));
				mapbean.put("fbh", rs.getString("fbh"));
				mapbean.put("psonId",
						rs.getString("pid") == null ? "" : rs.getString("pid"));
				mapbean.put("psonName",
						rs.getString("xm") == null ? "" : rs.getString("xm"));
				streetList.add(mapbean);
			}
		});
		sqlStr = "SELECT A.ID,A.MC,A.BH,A.FBH,B.ID as pid,B.XM FROM WGH_WGHF A LEFT JOIN XT_YHXX B ON A.BH=B.WGBH  left join XT_YHXX c on a.INSTITUTION_BH=c.INSTITUTION_BH WHERE A.DH=1";
		if(userId!=null){
			sqlStr +=" and c.id='"+userId+"'";
		}
		final List<Map<String, Object>> sList = new ArrayList<Map<String, Object>>();
		jdbcTemplate.query(sqlStr, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> mapbean;
				mapbean = new HashMap<String, Object>();
				mapbean.put("id", rs.getString("ID"));
				mapbean.put("name", rs.getString("MC"));
				mapbean.put("bh", rs.getString("BH"));
				mapbean.put("fbh", rs.getString("fbh"));
				mapbean.put("psonId",
						rs.getString("pid") == null ? "" : rs.getString("pid"));
				mapbean.put("psonName",
						rs.getString("xm") == null ? "" : rs.getString("xm"));
				sList.add(mapbean);
			}

		});
		for (int i = 0, quLength = quList.size(); i < quLength; i++) {
			List<Map<String, Object>> beanList = new ArrayList<Map<String, Object>>();
			for (int j = 0, communityLength = communityList.size(); j < communityLength; j++) {
				if (communityList.get(j).get("fbh")
						.equals(quList.get(i).get("bh"))) {
					beanList.add(communityList.get(j));
				}
			}
			quList.get(i).put("beanList", beanList);
		}
		for (int i = 0, streetLength = streetList.size(); i < streetLength; i++) {
			List<Map<String, Object>> beanList = new ArrayList<Map<String, Object>>();
			for (int j = 0, quLength = quList.size(); j < quLength; j++) {
				if (quList.get(j).get("fbh")
						.equals(streetList.get(i).get("bh"))) {
					beanList.add(quList.get(j));
				}
			}
			streetList.get(i).put("beanList", beanList);
		}
		for (int i = 0, sLength = sList.size(); i < sLength; i++) {
			List<Map<String, Object>> beanList = new ArrayList<Map<String, Object>>();
			for (int j = 0, quLength = streetList.size(); j < quLength; j++) {
				if (streetList.get(j).get("fbh").equals(sList.get(i).get("bh"))) {
					beanList.add(streetList.get(j));
				}
			}
			sList.get(i).put("beanList", beanList);
		}
		return sList.get(0);
	}

	@SuppressWarnings("static-access")
	@Override
	public Object getAccessToken() {

		Map<String, Object> nmap = new HashMap<String, Object>();
		try {
			Map<String, String> mapAccessToken = new HashMap<String, String>();
			Properties prop = new Properties();
			// 读取属性文件a.properties
			InputStream in = getClass().getResourceAsStream(
					"//YsRadio.properties");
			prop.load(in);
			// 加载属性列表
			Iterator<String> it = prop.stringPropertyNames().iterator();
			while (it.hasNext()) {
				String key = it.next();
				if ("appKey".equals(key)) {
					mapAccessToken.put("appKey", prop.getProperty(key));
				}
				if ("appSecret".equals(key)) {
					mapAccessToken.put("appSecret", prop.getProperty(key));
				}
			}
			in.close();
			String accessToken = HttpUtil.postRequest(
					"https://open.ys7.com/api/lapp/token/get", mapAccessToken);
			JSONObject josn = new JSONObject();
			JSONObject nobj1 = josn.fromObject(accessToken);
			Map<String, Object> map = new HashMap<String, Object>();
			JSONObject data1 = nobj1.getJSONObject("data");
			Map<String, Object> tokenmap = new HashMap<String, Object>();
			if ("200".equals(nobj1.get("code").toString())) {
				accessToken = data1.getString("accessToken");
				map.put("message", "获取accessToken成功");
				map.put("status", 1);
				tokenmap.put("accessToken", accessToken);
				nmap.put("bean", tokenmap);
				map.put("data", nmap);
			} else {
				map.put("message", "获取accessToken失败");
				map.put("status", 0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nmap;
	}

	@Override
	public Object queryNewStationTree(String userId) {
		String sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_station a left join wgh_wghf b on a.area_bh=b.bh ";
		if(userId!=null){
			sql+="where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%' and a.DISPLAY=1";
		}
		System.out.println(sql);
		final List<Map<String, Object>> sList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_site a left join wgh_wghf b on a.area_bh=b.bh ";
		if(userId!=null){
			sql+="where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%' and a.DISPLAY=1";
		}
		final List<Map<String, Object>> siteList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",b.mc as \"grid\"  from T_bas_gkz a left join wgh_wghf b on a.area_bh=b.bh ";
		if(userId!=null){
			sql+="where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%' and a.DISPLAY=1";
		}
		final List<Map<String, Object>> gkzList = new ArrayList<Map<String, Object>>();
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> smapMap = new HashMap<String, Object>();
				smapMap.put("mn", rs.getString("mn"));
				smapMap.put("name", rs.getString("name"));
				smapMap.put("grid", rs.getString("grid"));
				smapMap.put("isFa", 0);
				List<Map<String, Object>> nsiteList = new ArrayList<Map<String, Object>>();
				List<Integer> num = new ArrayList<Integer>();
				for (int i = 0; i < siteList.size(); i++) {
					siteList.get(i).put("isFa", 0);
					if (rs.getString("mn").equals(siteList.get(i).get("fmn"))) {
						smapMap.put("isFa", 1);
						List<Map<String, Object>> sslList = new ArrayList<Map<String, Object>>();
						for (int j = 0; j < sList.size(); j++) {
							if(sList.get(j).get("fmn")!=null){
							if (sList.get(j).get("fmn")
									.equals(siteList.get(i).get("mn"))) {
								num.add(i);
								siteList.get(i).put("isFa", 1);
								sList.get(j).put("isUse", true);
								sslList.add(sList.get(j));
							}
							}
						}
						siteList.get(i).put("stationList", sslList);
						nsiteList.add(siteList.get(i));
					}
				}
				
				for(int i=0;i<num.size();i++){
					siteList.get(num.get(i)).put("isFa", 1);
				}
				for (int j = 0; j < sList.size(); j++) {
					if(sList.get(j).get("fmn")!=null){
					if (rs.getString("mn").equals(sList.get(j).get("fmn"))) {
							smapMap.put("isFa", 1);
							sList.get(j).put("isUse", true);
							nsiteList.add(sList.get(j));
					}
					}
				}
				smapMap.put("stationList", nsiteList);
				gkzList.add(smapMap);
			}});
		for(int j = 0; j < sList.size(); j++){
			sList.get(j).put("stationList", new ArrayList<Map<String, Object>>());
			sList.get(j).put("isFa", 0);
			if(sList.get(j).get("isUse")==null){
				sList.get(j).put("isUse", true);
				gkzList.add(sList.get(j));
			}
		}
		Map<String,Object> map =new HashMap<String,Object>();
		map.put("mn", "123456789");
		map.put("name", "ceshi1");
		map.put("grid", "雨花区");
		map.put("isFa", 1);
		List<Map<String, Object>> ceList = new ArrayList<Map<String, Object>>();
		Map<String,Object> cemap =new HashMap<String,Object>();
		cemap.put("mn", "12345678");
		cemap.put("name", "ceshi2");
		cemap.put("grid", "雨花区");
		cemap.put("isFa", 1);
		
//		map.put("stationList", ceList);
		List<Map<String, Object>> ceList1 = new ArrayList<Map<String, Object>>();
		Map<String,Object> cemap1 =new HashMap<String,Object>();
		cemap1.put("mn", "12345679");
		cemap1.put("name", "ceshi3");
		cemap1.put("grid", "雨花区");
		cemap1.put("stationList", new ArrayList<Map<String, Object>>());
		ceList1.add(cemap1);
		cemap.put("stationList", ceList1);
		ceList.add(cemap);
		
		Map<String,Object> ncemap =new HashMap<String,Object>();
		ncemap.put("mn", "123456");
		ncemap.put("name", "ceshi21");
		ncemap.put("grid", "雨花区");
		ncemap.put("isFa", 1);
		
		
		List<Map<String, Object>> nceList1 = new ArrayList<Map<String, Object>>();
		Map<String,Object> ncemap1 =new HashMap<String,Object>();
		ncemap1.put("mn", "1234567");
		ncemap1.put("name", "ceshi22");
		ncemap1.put("grid", "雨花区");
		ncemap1.put("stationList", new ArrayList<Map<String, Object>>());
		nceList1.add(ncemap1);
		Map<String,Object> ncemap2 =new HashMap<String,Object>();
		ncemap2.put("mn", "1234579");
		ncemap2.put("name", "ceshi23");
		ncemap2.put("grid", "雨花区");
		ncemap2.put("stationList", new ArrayList<Map<String, Object>>());
		nceList1.add(ncemap2);
		ncemap.put("stationList", nceList1);
		ceList.add(ncemap);
		map.put("stationList", ceList);
		gkzList.add(map);
		return gkzList;
	}

	@Override
	public Object queryRealInfoRank(String factorType, String stationType,
			String userId) {
		final List<MapInfo> miList = new ArrayList<MapInfo>();
		String sql = "SELECT B.RECIEVE_TIME,a.MN,B.PM25,B.PM10,B.CO,B.O3,B.SO2,B.AQI,B.NO2,A.nAME,A.ADDRESS,A.LAT,A.LNT,A.TYPE FROM (SELECT NAME,MN,LAT,LNT,INSTITUTION_BH,ADDRESS,0 type,DISPLAY FROM BASE_STATION UNION SELECT NAME,MN,LAT,LNT,INSTITUTION_BH,ADDRESS,2 type,DISPLAY FROM T_BAS_GKZ) A LEFT JOIN" 
				+" DATA_REAL b  ON A.MN=B.MN ";
		if(userId!=null){
			String usql = "select INSTITUTION_BH from XT_YHXX where id='"+userId+"'";
			List<Map<String,Object>> list = jdbcTemplate.queryForList(usql);
			if(list!=null&&list.size()!=0){
			sql+=" WHERE A.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%'  and a.DISPLAY=1";
			}
		}
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				MapInfo mapInfo = new MapInfo();
				mapInfo.setOnline(false);
				if(rs.getTimestamp("RECIEVE_TIME")!=null){
				mapInfo.setTime(secondG.format(rs
						.getTimestamp("RECIEVE_TIME")));
				Calendar c1 = Calendar.getInstance();
				mapInfo.setOnline(c1.getTimeInMillis()-rs
						.getTimestamp("RECIEVE_TIME").getTime()<=(3*60*60*1000));
				}
				mapInfo.setMn(rs.getString("MN"));
				mapInfo.setPM25(rs.getDouble("PM25"));
				mapInfo.setPM10(rs.getDouble("PM10"));
				mapInfo.setCO(rs.getDouble("CO"));
				mapInfo.setO3(rs.getDouble("O3"));
				mapInfo.setSO2(rs.getDouble("SO2"));
				mapInfo.setNO2(rs.getDouble("NO2"));
				mapInfo.setAQI(rs.getDouble("AQI"));
				mapInfo.setName(rs.getString("NAME"));
				mapInfo.setLnt(rs.getDouble("LNT"));
				mapInfo.setLat(rs.getDouble("LAT"));
				mapInfo.setAddress(rs.getString("ADDRESS") == null ? "" : rs
						.getString("ADDRESS"));
				 mapInfo.setStationType(rs.getInt("TYPE"));
//				if (rs.getString("STATIONNAME") != null) {
//					mapInfo.setName(rs.getString("STATIONNAME"));
//					mapInfo.setLnt(rs.getDouble("STATIONLNT"));
//					mapInfo.setLat(rs.getDouble("STATIONLAT"));
//					mapInfo.setAddress(rs.getString("BADD") == null ? "" : rs
//							.getString("BADD"));
//					 mapInfo.setStationType(0);
//				} else if (rs.getString("SITENAME") != null) {
//					mapInfo.setName(rs.getString("SITENAME"));
//					mapInfo.setLnt(rs.getDouble("SITELNT"));
//					mapInfo.setLat(rs.getDouble("SITELAT"));
//					mapInfo.setAddress(rs.getString("CADD") == null ? "" : rs
//							.getString("CADD"));
//					 mapInfo.setStationType(1);
//				} else if (rs.getString("GKZNAME") != null) {
//					mapInfo.setName(rs.getString("GKZNAME"));
//					mapInfo.setLnt(rs.getDouble("GKZLNT"));
//					mapInfo.setLat(rs.getDouble("GKZLAT"));
//					mapInfo.setAddress(rs.getString("DADD") == null ? "" : rs
//							.getString("DADD"));
//					 mapInfo.setStationType(2);
//				}
				miList.add(mapInfo);
			}});
		return miList;
	}

	@Override
	public Object queryTrack(String time, String userId) {
		String sqlString="select id as \"id\",lnt as \"lnt\",lat as \"lat\",address as \"address\",to_char(tm,'YYYY-MM-DD HH24:mi:ss') as \"time\",userId as \"userId\",tzid as \"special\" from WGH_TRACK where userID='"+userId+"' and to_char(tm,'YYYY-MM-DD HH24:mi:ss') like '%"
				+ time
				+ "%' order by tm";
		List<Map<String, Object>> list =jdbcTemplate.queryForList(sqlString);
		return list;
	}

	@Override
	public Object queryRecentlyData(String mn) {
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();  
		calendar.setTime(date); 
		calendar.add(Calendar.HOUR_OF_DAY, -24);
		SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sql = "select stationCode as \"mn\",CO,NO2,SO2,O3,PM25,PM10,AQI,to_char(receivetime,'YYYY-MM-DD HH24:mi:ss') as \"time\" from T_cal_hour where stationcode=? and receivetime>=to_date('"+secondG.format(calendar.getTime())+"','YYYY-MM-DD HH24:mi:ss')";
		List<Map<String,Object>> list = jdbcTemplate.queryForList(sql,new Object[]{mn});
		return list;
	}
	
	public Object queryStationTree(String userId) {
		String sql = "select a.mn \"mn\",a.name \"name\",nvl(c.fmn,a.fmn) \"fmn\",b.mc as \"grid\"  from base_station a left join wgh_wghf b on a.area_bh=b.bh left join base_site c on a.fmn=c.mn ";
		if(userId!=null){
			sql+="where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%' and a.DISPLAY=1";
		}
		final List<Map<String, Object>> sList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",b.mc as \"grid\"  from T_bas_gkz a left join wgh_wghf b on a.area_bh=b.bh ";
		if(userId!=null){
			sql+="where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%' and a.DISPLAY=1";
		}
		final List<Map<String, Object>> gkzList = new ArrayList<Map<String, Object>>();
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> smapMap = new HashMap<String, Object>();
				smapMap.put("mn", rs.getString("mn"));
				smapMap.put("name", rs.getString("name"));
				smapMap.put("grid", rs.getString("grid"));
				smapMap.put("isFa", 0);
				List<Map<String, Object>> nsiteList = new ArrayList<Map<String, Object>>();
				for (int j = 0; j < sList.size(); j++) {
					if(sList.get(j).get("fmn")!=null){
					if (rs.getString("mn").equals(sList.get(j).get("fmn"))) {
							smapMap.put("isFa", 1);
							sList.get(j).put("isUse", true);
							nsiteList.add(sList.get(j));
					}
					}
				}
				smapMap.put("stationList", nsiteList);
				gkzList.add(smapMap);
			}});
		for(int j = 0; j < sList.size(); j++){
			sList.get(j).put("stationList", new ArrayList<Map<String, Object>>());
			sList.get(j).put("isFa", 0);
			if(sList.get(j).get("isUse")==null){
				sList.get(j).put("isUse", true);
				gkzList.add(sList.get(j));
			}
		}
		return gkzList;
	}
}