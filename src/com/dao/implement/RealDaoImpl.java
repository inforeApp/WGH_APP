package com.dao.implement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.dao.RealDao;
import com.model.FactorRank;
import com.util.FormatUtil;
import com.util.GetAirQualityComIndex;

@Repository
public class RealDaoImpl implements RealDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public Object queryRealDataByMn(String currentId,String mn) {
		String factorSql = "SELECT a.FACTOR_CODE,a.FACTOR_NAME from XT_YZPZ a left join XT_YHXX b on a.YHZH=b.zh where b.id= ?";
		Map<String,Object> factorMap = jdbcTemplate.queryForMap(factorSql, new Object[]{currentId});
		String name = factorMap.get("FACTOR_NAME").toString();
		String code = factorMap.get("FACTOR_CODE").toString();
		String[] names = name.split(",");
		String[] codes = code.split(",");
		StringBuffer nameSb=new StringBuffer();
		for(int i=0;i<names.length;i++){
			if(i==(names.length-1)){
				nameSb.append("'"+names[i].trim()+"'");
            }else{
            	nameSb.append("'"+names[i].trim()+"',");
            }
		}
		StringBuffer codeSb=new StringBuffer();
		for(int i=0;i<codes.length;i++){
			if(i==(codes.length-1)){
				codeSb.append("a."+codes[i]);
            }else{
            	codeSb.append("a."+codes[i]+",");
            }
		}
		String fsql = "SELECT NAME,UNIT FROM BASE_FACTOR where name in ("+nameSb.toString()+")";
		System.out.println(fsql);
		final List<Map<String, Object>> flist = new ArrayList<Map<String, Object>>();
		jdbcTemplate.query(fsql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("name", rs.getString("NAME"));
				map.put("unit", rs.getString("UNIT"));
//				if("大气压".equals(rs.getString("NAME"))){
//					
//				}else{
					flist.add(map);
//				}
			}
		});
		Map<String, Object> aqimap = new HashMap<String, Object>();
		aqimap.put("name", "AQI");
		aqimap.put("unit", "");
		flist.add(aqimap);
		System.out.println(flist.toString());
		String sql = "SELECT A.RECIEVE_TIME,A.MN,"+codeSb+",a.AQI,B.NAME AS STATIONNAME,B.LNT AS STATIONLNT,B.LAT AS STATIONLAT,C.NAME AS SITENAME,C.LNT AS SITELNT,C.LAT AS SITELAT,D.NAME AS GKZNAME,D.LNT AS GKZLNT,D.LAT AS GKZLAT,nvl(f.id,nvl(h.id,j.id)) psonId,nvl(f.xm,nvl(h.xm,j.xm)) name,nvl(f.sj,nvl(h.sj,j.sj)) phone,nvl(f.txdz,nvl(h.txdz,j.txdz)) address FROM  DATA_REAL A LEFT JOIN BASE_STATION B ON A.MN=B.MN LEFT JOIN BASE_SITE C ON A.MN=C.MN LEFT JOIN T_BAS_GKZ D ON A.MN=D.MN left join WGH_WGHF e on b.area_bh=e.bh left join  xt_yhxx f on e.zjzrrid=f.id left join WGH_WGHF g on g.bh=c.area_bh left join xt_yhxx h on g.zjzrrid=h.id left join WGH_WGHF i on d.area_bh=i.bh left join xt_yhxx j on i.zjzrrid=j.id where a.mn='"
				+ mn + "'";
		final Map<String, Object> beanmap = new HashMap<String, Object>();
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				beanmap.put("time",secondG.format(rs
						.getTimestamp("RECIEVE_TIME")));
				for (int i = 0,size=flist.size(); i < size; i++) {
					String s = (String) flist.get(i).get("name");
					if ("PM25".equals(s)) {
						flist.get(i).put("value", rs.getDouble("PM25"));
					} else if ("PM10".equals(s)) {
						flist.get(i).put("value", rs.getDouble("PM10"));
					} else if ("CO".equals(s)) {
						flist.get(i).put("value", rs.getDouble("CO"));
					} else if ("O3".equals(s)) {
						flist.get(i).put("value", rs.getDouble("O3"));
					} else if ("SO2".equals(s)) {
						flist.get(i).put("value", rs.getDouble("SO2"));
					} else if ("NO2".equals(s)) {
						flist.get(i).put("value", rs.getDouble("NO2"));
					} else if ("AQI".equals(s)) {
						flist.get(i)
								.put("value",
										rs.getDouble("AQI"));
					}else if("大气压".equals(s)){
						flist.get(i)
						.put("value",
								rs.getDouble("PRESSURE"));
					}else if("温度".equals(s)){
						flist.get(i)
						.put("value",
								rs.getDouble("TEMPERATURE"));
					}else if("湿度".equals(s)){
						flist.get(i)
						.put("value",
								rs.getDouble("HUMIDITY"));
					}else if("风向".equals(s)){
						flist.get(i)
						.put("value",
								rs.getDouble("GUSTDIR"));
					}else if("风速".equals(s)){
						flist.get(i)
						.put("value",
								rs.getDouble("GUSTSPEED"));
					}
				}
				if (rs.getString("STATIONNAME") != null) {
					beanmap.put("stationName", rs.getString("STATIONNAME"));
				} else if (rs.getString("SITENAME") != null) {
					beanmap.put("stationName", rs.getString("SITENAME"));
				} else if (rs.getString("GKZNAME") != null) {
					beanmap.put("stationName", rs.getString("GKZNAME"));
				}
				beanmap.put(
						"psonId",
						rs.getString("psonId") == null ? "" : rs
								.getString("psonId"));
				beanmap.put(
						"phone",
						rs.getString("phone") == null ? "" : rs
								.getString("phone"));
				beanmap.put("psonName", rs.getString("name") == null ? ""
						: rs.getString("name"));
				beanmap.put("address", rs.getString("address") == null ? ""
						: rs.getString("address"));
				beanmap.put("factorList", flist);
			}
		});
		return beanmap;
	}

	@Override
	public Object queryStationDetails(String mn) {
		final Map<String, Object> smap = new HashMap<String, Object>();
		String sql = "select a.mn,a.name,'国控站' stationSourceType,a.ADDRESS,f.mc,a.remark,f.id  from T_BAS_GKZ a left join WGH_WGHF f on a.area_bh=f.bh where a.mn = '"+mn+"' union select a.mn,a.name,'微型站' stationSourceType,a.ADDRESS,f.mc,a.remark,f.id  from BASE_STATION a left join WGH_WGHF f on a.area_bh=f.bh where a.mn = '"+mn+"' union select a.mn,a.name,'污染源' stationSourceType,a.ADDRESS,f.mc,a.remark,f.id  from BASE_SITE a left join WGH_WGHF f on a.area_bh=f.bh where a.mn = '"+mn+"'";
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				smap.put("mn", rs.getString("mn"));
				smap.put("stationSourceType", rs.getString("stationSourceType"));
				smap.put("name", rs.getString("name"));
				smap.put(
							"address",
							rs.getString("address")==null?"":rs.getString("address"));
				smap.put("grid", rs.getString("mc"));
				smap.put("stationType", "空气站");
				smap.put("stationRemark",  rs.getString("remark")==null?"": rs.getString("remark"));
				String sql = "select b.xm gridPson,b.sj gridPsonPhone,d.xm upGridPson,d.sj upGridPsonPhone from WGH_WGHF a left join XT_YHXX b on a.zjzrrid=b.id left join WGH_WGHF c on a.fbh=c.bh left join XT_YHXX d on c.zjzrrid=d.id where a.id=?";
				List<Map<String,Object>> list = jdbcTemplate.queryForList(sql,new Object[]{rs.getString("id")});
				if(list.size()!=0){
					smap.put("gridPson", list.get(0).get("gridPson"));
					smap.put("gridPsonPhone", list.get(0).get("gridPsonPhone"));
					smap.put("upGridPson", list.get(0).get("upGridPson"));
					smap.put("upGridPsonPhone", list.get(0).get("upGridPsonPhone"));
				}else{
					smap.put("gridPson", "");
					smap.put("gridPsonPhone", "");
					smap.put("upGridPson", "");	
					smap.put("upGridPsonPhone", "");
				}
				
				smap.put("gridRemark", "无");
			}
		});
		return smap;
	}

	@Override
	public Object queryRealDataRank(String factorType, String userId) {
		final String factorString = factorType;
		final List<FactorRank> mlist = new ArrayList<FactorRank>();
		String fsql = "SELECT A.RECIEVE_TIME,A.MN,A.PM25,A.PM10,A.CO,A.O3,A.SO2,A.NO2,A.AQI,B.NAME AS STATIONNAME,B.LNT AS STATIONLNT,B.LAT AS STATIONLAT,C.NAME AS SITENAME,C.LNT AS SITELNT,C.LAT AS SITELAT,D.NAME AS GKZNAME,D.LNT AS GKZLNT,D.LAT AS GKZLAT,B.ADDRESS AS BADD,C.ADDRESS AS CADD,D.ADDRESS AS DADD FROM DATA_REAL A LEFT JOIN BASE_STATION B ON A.MN=B.MN LEFT JOIN BASE_SITE C ON A.MN=C.MN LEFT JOIN T_BAS_GKZ D ON A.MN=D.MN";
		if (userId != null) {
			String usql = "select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "'";
			List<Map<String, Object>> list = jdbcTemplate.queryForList(usql);
			if (list != null && list.size() != 0) {
				fsql += " where (b.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%' and b.DISPLAY=1) or (c.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%'  and c.DISPLAY=1) or  (d.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%'  and d.DISPLAY=1)";
			}
		}
		if (!"AQI".equals(factorType)) {
			fsql += " order by A." + factorType + " desc";
		}
		jdbcTemplate.query(fsql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				FactorRank mapInfo = new FactorRank();
				mapInfo.setMn(rs.getString("MN"));
				if ("PM25".equals(factorString)) {
					mapInfo.setFactorName("PM2.5");
					mapInfo.setValue(rs.getDouble("PM25"));
					mapInfo.setFactorTypeId(2);
				} else if ("PM10".equals(factorString)) {
					mapInfo.setFactorName("PM10");
					mapInfo.setValue(rs.getDouble("PM10"));
					mapInfo.setFactorTypeId(1);
				} else if ("CO".equals(factorString)) {
					mapInfo.setFactorName("CO");
					mapInfo.setValue(rs.getDouble("CO"));
					mapInfo.setFactorTypeId(5);
				} else if ("O3".equals(factorString)) {
					mapInfo.setFactorName("O3");
					mapInfo.setValue(rs.getDouble("O3"));
					mapInfo.setFactorTypeId(6);
				} else if ("SO2".equals(factorString)) {
					mapInfo.setFactorName("SO2");
					mapInfo.setValue(rs.getDouble("SO2"));
					mapInfo.setFactorTypeId(3);
				} else if ("NO2".equals(factorString)) {
					mapInfo.setFactorName("NO2");
					mapInfo.setValue(rs.getDouble("NO2"));
					mapInfo.setFactorTypeId(4);
				} else if ("AQI".equals(factorString)) {
					mapInfo.setFactorName("AQI");
					mapInfo.setValue(rs.getDouble("AQI"));
					mapInfo.setFactorTypeId(0);
				}
				Calendar c1 = Calendar.getInstance();
				mapInfo.setOnline(c1.getTimeInMillis()-rs
						.getTimestamp("RECIEVE_TIME").getTime()<=(3*60*60*1000));
				if (rs.getString("STATIONNAME") != null) {
					mapInfo.setStationName(rs.getString("STATIONNAME"));
					mapInfo.setStationType(0);
				} else if (rs.getString("SITENAME") != null) {
					mapInfo.setStationName(rs.getString("SITENAME"));
					mapInfo.setStationType(1);
				} else if (rs.getString("GKZNAME") != null) {
					mapInfo.setStationName(rs.getString("GKZNAME"));
					mapInfo.setStationType(2);
				}
				mlist.add(mapInfo);
			}
		});
		Collections.sort(mlist);
		return mlist;
	}

	@Override
	public Object searchRealWarning(String userId, String warningLevel,
			String keyword, String type, String pageNum, String warningLevel_1,
			String warningLevel_2, String warningLevel_3, String warningLevel_4) {
		final List<Map<String, Object>> miList = new ArrayList<Map<String, Object>>();
		String sql = "select A.ID,A.EVEN_TIME,A.MN,A.EM_DEGREE,A.DESCRIPTIONS,a.BNAME,a.BMN,a.CNAME,a.CMN,a.DNAME,a.DMN from (SELECT rownum as no,A.ID,A.EVEN_TIME,A.MN,A.EM_DEGREE,A.DESCRIPTIONS,B.NAME AS BNAME,B.MN AS BMN,C.NAME AS CNAME,C.MN AS CMN,D.NAME AS DNAME,D.MN AS DMN FROM EVENT_RECORD A LEFT JOIN BASE_STATION B ON A.MN=B.MN LEFT JOIN BASE_SITE C ON A.MN=C.MN LEFT JOIN T_BAS_GKZ D ON A.MN=D.MN  WHERE (B.NAME LIKE '%"
				+ keyword
				+ "%' OR C.NAME LIKE '%"
				+ keyword
				+ "%' OR D.NAME LIKE '%"
				+ keyword
				+ "%' OR  A.DESCRIPTIONS LIKE '%" + keyword + "%')";
		warningLevel_1 = ("0".equals(warningLevel_1)) ? "" : "一级";
		warningLevel_2 = ("0".equals(warningLevel_2)) ? "" : "二级";
		warningLevel_3 = ("0".equals(warningLevel_3)) ? "" : "三级";
		warningLevel_4 = ("0".equals(warningLevel_4)) ? "" : "四级";
		sql += " and A.EM_DEGREE in ('" + warningLevel_1 + "','"
				+ warningLevel_2 + "','" + warningLevel_3 + "','"
				+ warningLevel_4 + "') ";
		if(userId!=null){
			String usql = "select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "'";
			List<Map<String, Object>> list = jdbcTemplate.queryForList(usql);
			if (list != null && list.size() != 0) {
				sql+=" and ( b.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%'  or c.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%' or  d.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"+userId+"')||'%')";
			}
		}
		sql+=" order by a.EVEN_TIME desc) a";
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, Calendar.HOUR - 1);
		sql += " where a.no<=" + Integer.parseInt(pageNum) * 20 + " and a.no>"
				+ (Integer.parseInt(pageNum) - 1) * 20;
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> mapbean = new HashMap<String, Object>();
				mapbean.put("id", rs.getString("ID"));
				if (rs.getString("BNAME") != null) {
					mapbean.put("stationName", rs.getString("BNAME"));
					mapbean.put("mn", rs.getString("BMN"));
				} else if (rs.getString("CNAME") != null) {
					mapbean.put("stationName", rs.getString("CNAME"));
					mapbean.put("mn", rs.getString("CMN"));
				} else if (rs.getString("DNAME") != null) {
					mapbean.put("stationName", rs.getString("DNAME"));
					mapbean.put("mn", rs.getString("DMN"));
				}
				SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				mapbean.put("time",
						secondG.format(rs.getTimestamp("EVEN_TIME")));
				mapbean.put(
						"warningLevel",
						rs.getString("EM_DEGREE") == null ? "" : rs
								.getString("EM_DEGREE"));
				mapbean.put("discrib", rs.getString("DESCRIPTIONS"));
				miList.add(mapbean);
			}
		});
		return miList;
	}

	@Override
	public Object queryRealWarning(String userId, String warningLevel,
			String keyword, String type, String pageNum) {
		String sql = "select A.ID,A.EVEN_TIME,A.MN,A.EM_DEGREE,A.DESCRIPTIONS,a.BNAME,a.BMN,a.CNAME,a.CMN,a.DNAME,a.DMN from (SELECT  rownum as no,A.ID,A.EVEN_TIME,A.MN,A.EM_DEGREE,A.DESCRIPTIONS,B.NAME AS BNAME,B.MN AS BMN,C.NAME AS CNAME,C.MN AS CMN,D.NAME AS DNAME,D.MN AS DMN FROM EVENT_RECORD A LEFT JOIN BASE_STATION B ON A.MN=B.MN LEFT JOIN BASE_SITE C ON A.MN=C.MN LEFT JOIN T_BAS_GKZ D ON A.MN=D.MN WHERE 1=1 ";
		String str = "";
		Date date = new Date();
		SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
		if("1".equals(type)){
			str+= " and to_char(a.EVEN_TIME,'yyyy-mm-dd hh24:mi:ss') like '"+secondA.format(date)+"%'";
		}else{
			
		}
		if (!"5".equals(warningLevel)) {
			if ("1".equals(warningLevel)) {
				warningLevel = "一级";
			} else if ("2".equals(warningLevel)) {
				warningLevel = "二级";
			} else if ("3".equals(warningLevel)) {
				warningLevel = "三级";
			} else if ("4".equals(warningLevel)) {
				warningLevel = "四级";
			}
			str += " and A.EM_DEGREE = '" + warningLevel + "'";
		}
		if(userId!=null){
			String usql = "select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "'";
			List<Map<String, Object>> list = jdbcTemplate.queryForList(usql);
			if (list != null && list.size() != 0) {
				str+=" and ( b.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"+userId+"') or c.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"+userId+"') or  d.INSTITUTION_BH =(select INSTITUTION_BH from XT_YHXX where id='"+userId+"') )";
			}
		}
		sql += str + " order by a.EVEN_TIME desc) a";
		// if(userId!=null){
		// sql+=" and (b.INSTITUTION_BH=(select INSTITUTION_BH from XT_YHXX where id='"+userId+"') or c.INSTITUTION_BH=(select INSTITUTION_BH from XT_YHXX where id='"+userId+"') or d.INSTITUTION_BH=(select INSTITUTION_BH from XT_YHXX where id='"+userId+"'))";
		// }
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR, Calendar.HOUR - 1);
		sql += " where a.no<=" + Integer.parseInt(pageNum) * 20 + " and a.no>"
				+ (Integer.parseInt(pageNum) - 1) * 20;
		final List<Map<String, Object>> miList = new ArrayList<Map<String, Object>>();
		jdbcTemplate.query(sql, new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				Map<String, Object> mapbean = new HashMap<String, Object>();
				mapbean.put("id", rs.getString("ID"));
				if (rs.getString("BNAME") != null) {
					mapbean.put("stationName", rs.getString("BNAME"));
					mapbean.put("mn", rs.getString("BMN"));
				} else if (rs.getString("CNAME") != null) {
					mapbean.put("stationName", rs.getString("CNAME"));
					mapbean.put("mn", rs.getString("CMN"));
				} else if (rs.getString("DNAME") != null) {
					mapbean.put("stationName", rs.getString("DNAME"));
					mapbean.put("mn", rs.getString("DMN"));
				}
				SimpleDateFormat secondG=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				mapbean.put("time",
						secondG.format(rs.getTimestamp("EVEN_TIME")));
				mapbean.put(
						"warningLevel",
						rs.getString("EM_DEGREE") == null ? "" : rs
								.getString("EM_DEGREE"));
				mapbean.put("discrib", rs.getString("DESCRIPTIONS"));
				miList.add(mapbean);
			}
		});
		return miList;
	}

	@Override
	public Object queryPhotoByMn(String userId, String mn, String time) {
		String sql = "select nvl('http://120.77.41.22:8080/wghUrl'||(translate(url,'\\','/')),'') as \"url\",to_char(receivetime,'yyyy-mm-dd hh24:mi:ss') as \"time\" from T_CAL_PHOTO where mn=? and to_char(receivetime,'yyyy-mm-dd hh24:mi:ss') like '"+time+"%'";
		List<Map<String,Object>> list = jdbcTemplate.queryForList(sql,new Object[]{mn});
		return list;
	}

	@Override
	public Object queryRecentlyByMn(String userId, String mn) {
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR, -24);
		String sql = "select to_char(a.receivetime,'yyyy-mm-dd hh24:mi:ss') as \"time\",a.CO,a.NO2,a.SO2,a.O3,a.PM25,a.PM10,a.AQI from T_CAl_Hour a where a.stationcode=? and a.receivetime between ? and ? order by a.receivetime";
		List<Map<String,Object>> list = jdbcTemplate.queryForList(sql,new Object[]{mn,c.getTime(),date});
		List<Map<String,Object>> Colist = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> NO2list = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> SO2list = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> O3list = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> PM25list = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> PM10list = new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> AQIlist = new ArrayList<Map<String,Object>>();
		for(int i=0,size=list.size();i<size;i++){
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("value", list.get(i).get("CO"));
				map.put("time", list.get(i).get("time"));
				Colist.add(map);
				map = new HashMap<String,Object>();
				map.put("time", list.get(i).get("time"));
				map.put("value", list.get(i).get("NO2"));
				NO2list.add(map);
				map = new HashMap<String,Object>();
				map.put("time", list.get(i).get("time"));
				map.put("value", list.get(i).get("SO2"));
				SO2list.add(map);
				map = new HashMap<String,Object>();
				map.put("time", list.get(i).get("time"));
				map.put("value", list.get(i).get("O3"));
				O3list.add(map);
				map = new HashMap<String,Object>();
				map.put("time", list.get(i).get("time"));
				map.put("value", list.get(i).get("PM25"));
				PM25list.add(map);
				map = new HashMap<String,Object>();
				map.put("time", list.get(i).get("time"));
				map.put("value", list.get(i).get("PM10"));
				PM10list.add(map);
				map = new HashMap<String,Object>();
				map.put("time", list.get(i).get("time"));
				map.put("value", list.get(i).get("AQI"));
				AQIlist.add(map);
		}
		List<Map<String,Object>> datalist = new ArrayList<Map<String,Object>>();
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("name", "CO");
		map.put("vlist", Colist);
		datalist.add(map);
		map = new HashMap<String,Object>();
		map.put("name", "NO2");
		map.put("vlist", NO2list);
		datalist.add(map);
		 map = new HashMap<String,Object>();
		map.put("name", "SO2");
		map.put("vlist", SO2list);
		datalist.add(map);
		 map = new HashMap<String,Object>();
		map.put("name", "O3");
		map.put("vlist", O3list);
		datalist.add(map);
		 map = new HashMap<String,Object>();
		map.put("name", "PM25");
		map.put("vlist", PM25list);
		datalist.add(map);
		 map = new HashMap<String,Object>();
		map.put("name", "PM10");
		map.put("vlist", PM10list);
		datalist.add(map);
		 map = new HashMap<String,Object>();
		map.put("name", "AQI");
		map.put("vlist", AQIlist);
		datalist.add(map);
		return datalist;
	}

	@Override
	public Object queryRealDispatchByAQI(String userId, String dataType,
			String pageNum) {
		List<Map<String, Object>> nslist = new ArrayList<Map<String, Object>>();
		String sql = "";
		String bString = "";
		String fString = "";
		sql = "select a.mn \"mn\",a.name \"name\",b.mc as \"grid\"  from T_bas_gkz a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> gkzList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_station a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> sList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_site a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> siteList = jdbcTemplate.queryForList(sql);
		String stationString = "";
		for (int i = 0,size=siteList.size(); i < size; i++) {
			stationString += "'" + siteList.get(i).get("mn") + "',";
		}
		for (int i = 0,size=sList.size(); i < size; i++) {
			stationString += "'" + sList.get(i).get("mn") + "',";
		}
		for (int j = 0,gkzsize = gkzList.size(); j < gkzsize; j++) {
			stationString += "'" + gkzList.get(j).get("mn") + "',";
		}
		if (!"".equals(stationString)) {
			stationString = stationString.substring(0,
					stationString.length() - 1);
		}
		String endTime = "";
		String startTime = "";
		String sString = "";
		if ("0".equals(dataType)) {// 分钟
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.HOUR, -4);
			SimpleDateFormat YMDHM=new SimpleDateFormat("yyyy-MM-dd HH:mm");
			startTime = YMDHM.format(c.getTime());
			endTime = YMDHM.format(date);
			SimpleDateFormat month=new SimpleDateFormat("yyyyMM");
			bString = "t_cal_minutes_" + month.format(date);
			fString = "yyyy-mm-dd HH24:mi";
			sString = "HH24:mi";
		} else if ("1".equals(dataType)) {// 小时
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.HOUR, -24);
			SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
			startTime = historydate.format(c.getTime());
			endTime = historydate.format(date);
			bString = "t_cal_hour";
			fString = "yyyy-mm-dd HH24";
			sString = "mm-dd HH24";
		} else if ("2".equals(dataType)) {// 天
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.MONTH, -1);
			SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
			startTime = secondA.format(c.getTime());
			endTime = secondA.format(date);
			bString = "t_cal_day";
			fString = "yyyy-mm-dd";
			sString = "mm-dd";
		}
		sql = "(select mn,name,PM10 PM10,PM25 PM25,SO2 SO2,NO2 NO2,CO CO,O3 O3,AQI AQI,TEMPERATURE \"temperature\",HUMIDITY \"humidity\",GUSTSPEED \"speed\",GUSTDIR \"speedDir\",pressure \"pressure\",to_char(receivetime,'"
				+ sString
				+ "') receivetime from (select nvl(nvl(a.mn,c.mn),d.mn) as mn,nvl(nvl(a.name,c.name),d.name) as name,max(b.PM10) PM10,max(b.PM25) PM25,max(b.SO2) SO2,max(b.NO2) NO2,max(b.CO) CO,max(b.O3) O3,max(b.AQI) AQI,max(b.TEMPERATURE) TEMPERATURE,max(b.HUMIDITY) HUMIDITY,max(b.GUSTSPEED) GUSTSPEED,max(b.GUSTDIR) GUSTDIR,max(b.pressure) pressure,b.receivetime from  "
				+ bString
				+ " b  left join base_station a on a.mn = b.stationcode left join base_site c on c.mn=b.stationcode left join T_bas_gkz d on d.mn=b.stationcode  where b.receivetime>=to_date('"
				+ startTime
				+ "','"
				+ fString
				+ "') and b.receivetime<=to_date('"
				+ endTime
				+ "','"
				+ fString
				+ "') and (a.mn in ("
				+ stationString
				+ ") or c.mn in ("
				+ stationString
				+ ") or d.mn in ("
				+ stationString
				+ ")) group by b.receivetime,nvl(nvl(a.name,c.name),d.name),nvl(nvl(a.mn,c.mn),d.mn) order by b.receivetime)"; 
		System.out.println(sql);
		sql +="union select a.mn,nvl(b.name,c.name) name,a.PM10 PM10,a.PM25 PM25,a.SO2 SO2,a.NO2 NO2,a.CO CO,a.O3 O3,a.AQI AQI,a.TEMPERATURE \"temperature\",a.HUMIDITY \"humidity\",a.GUSTSPEED \"speed\",a.GUSTDIR \"speedDir\",a.pressure \"pressure\",to_char(a.recieve_time,'"+sString+"') receivetime  from data_real a left join base_station b on a.mn=b.mn left join base_site c on a.mn=c.mn  where a.mn in("+stationString+") and nvl(b.name,c.name) is not null)order by mn,receivetime ";
		System.out.println(sql);
		 List<Map<String, Object>> slist = jdbcTemplate.queryForList(sql);
		 for(int i=0,size = slist.size();i<size;i++){
			 boolean flag = true;
			 for(int j=0;j< nslist.size();j++){
				 if(nslist.get(j).get("mn").equals(slist.get(i).get("mn"))){
					 flag = false;
					 @SuppressWarnings("unchecked")
					List<Map<String,Object>> dataList = (List<Map<String, Object>>) nslist.get(j).get("dataList");
					 Map<String,Object> map = new HashMap<String,Object>();
					 map.put("time", slist.get(i).get("RECEIVETIME"));
					 map.put("PM10", slist.get(i).get("PM10"));
					 map.put("PM25", slist.get(i).get("PM25"));
					 map.put("SO2", slist.get(i).get("SO2"));
					 map.put("NO2", slist.get(i).get("NO2"));
					 map.put("CO", slist.get(i).get("CO"));
					 map.put("O3", slist.get(i).get("O3"));
					 map.put("AQI", slist.get(i).get("AQI"));
					 map.put("humidity", slist.get(i).get("humidity"));
					 map.put("speed", slist.get(i).get("speed"));
					 map.put("speedDir", slist.get(i).get("speedDir"));
					 dataList.add(map);
				 }
			 }
			 if(flag){
				 Map<String,Object> mapall = new HashMap<String,Object>();
				 mapall.put("stationName", slist.get(i).get("name"));
				 mapall.put("mn", slist.get(i).get("mn"));
				 List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				 Map<String,Object> map = new HashMap<String,Object>();
				 map.put("time", slist.get(i).get("RECEIVETIME"));
				 map.put("PM10", slist.get(i).get("PM10"));
				 map.put("PM25", slist.get(i).get("PM25"));
				 map.put("SO2", slist.get(i).get("SO2"));
				 map.put("NO2", slist.get(i).get("NO2"));
				 map.put("CO", slist.get(i).get("CO"));
				 map.put("O3", slist.get(i).get("O3"));
				 map.put("AQI", slist.get(i).get("AQI"));
				 map.put("humidity", slist.get(i).get("humidity"));
				 map.put("speed", slist.get(i).get("speed"));
				 map.put("speedDir", slist.get(i).get("speedDir")); 
				 dataList.add(map);
				 mapall.put("dataList",dataList);
				 nslist.add(mapall);
			 }
		 }
//		 System.out.println(((List)nslist.get(3).get("dataList")).size());
		 List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		 for(int i=0,size = nslist.size();i<size;i++){
			 if (i >= ((Integer.parseInt(pageNum)) * 10)
						 && i < ((Integer.parseInt(pageNum) + 1) * 10)){
				 list.add(nslist.get(i));
			 }
		 }
		return list;
	}

	@Override
	public Object queryRealDispatchBySynthesis(String userId, String dataType,
			String pageNum) {
		List<Map<String, Object>> nslist = new ArrayList<Map<String, Object>>();
		String sql = "";
		String bString = "";
		String fString = "";
		sql = "select a.mn \"mn\",a.name \"name\",b.mc as \"grid\"  from T_bas_gkz a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> gkzList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_station a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> sList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_site a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> siteList = jdbcTemplate.queryForList(sql);
		String stationString = "";
		for (int i = 0,size = siteList.size(); i < size; i++) {
			stationString += "'" + siteList.get(i).get("mn") + "',";
		}
		for (int i = 0,size = sList.size(); i < size; i++) {
			stationString += "'" + sList.get(i).get("mn") + "',";
		}
		for (int i = 0,size = gkzList.size(); i < size; i++) {
			stationString += "'" + gkzList.get(i).get("mn") + "',";
		}
		if (!"".equals(stationString)) {
			stationString = stationString.substring(0,
					stationString.length() - 1);
		}
		String endTime = "";
		String startTime = "";
		String sString = "";
		if ("0".equals(dataType)) {// 分钟
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.HOUR, -4);
			SimpleDateFormat YMDHM=new SimpleDateFormat("yyyy-MM-dd HH:mm");
			startTime = YMDHM.format(c.getTime());
			endTime = YMDHM.format(date);
			SimpleDateFormat month=new SimpleDateFormat("yyyyMM");
			bString = "t_cal_minutes_" + month.format(date);
			fString = "yyyy-mm-dd HH24:mi";
			sString = "HH24:mi";
		} else if ("1".equals(dataType)) {// 小时
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.HOUR, -24);
			SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
			startTime = historydate.format(c.getTime());
			endTime = historydate.format(date);
			bString = "t_cal_hour";
			fString = "yyyy-mm-dd HH24";
			sString = "mm-dd HH24";
		} else if ("2".equals(dataType)) {// 天
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.MONTH, -1);
			SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
			startTime = secondA.format(c.getTime());
			endTime = secondA.format(date);
			bString = "t_cal_day";
			fString = "yyyy-mm-dd";
			sString = "mm-dd";
		}
		sql = "select mn,name,avg(PM10) PM10,avg(PM25) PM25,avg(SO2) SO2,avg(NO2) NO2,avg(CO) CO,avg(O3) O3,avg(AQI) AQI,avg(TEMPERATURE) \"temperature\",avg(HUMIDITY) \"humidity\",avg(GUSTSPEED) \"speed\",avg(GUSTDIR) \"speedDir\",avg(pressure) \"pressure\",to_char(receivetime,'"
				+ sString
				+ "') receivetime from (select nvl(nvl(a.mn,c.mn),d.mn) as mn,nvl(nvl(a.name,c.name),d.name) as name,max(b.PM10) PM10,max(b.PM25) PM25,max(b.SO2) SO2,max(b.NO2) NO2,max(b.CO) CO,max(b.O3) O3,max(b.AQI) AQI,max(b.TEMPERATURE) TEMPERATURE,max(b.HUMIDITY) HUMIDITY,max(b.GUSTSPEED) GUSTSPEED,max(b.GUSTDIR) GUSTDIR,max(b.pressure) pressure,b.receivetime from  "
				+ bString
				+ " b  left join base_station a on a.mn = b.stationcode left join base_site c on c.mn=b.stationcode left join T_bas_gkz d on d.mn=b.stationcode  where b.receivetime>=to_date('"
				+ startTime
				+ "','"
				+ fString
				+ "') and b.receivetime<=to_date('"
				+ endTime
				+ "','"
				+ fString
				+ "') and (a.mn in ("
				+ stationString
				+ ") or c.mn in ("
				+ stationString
				+ ") or d.mn in ("
				+ stationString
				+ ")) group by b.receivetime,nvl(nvl(a.name,c.name),d.name),nvl(nvl(a.mn,c.mn),d.mn) order by b.receivetime) group by to_char(receivetime,'"
				+ sString + "'),name,mn order by name,receivetime";
		 List<Map<String, Object>> slist = jdbcTemplate.queryForList(sql);
		 for(int i=0,size = slist.size();i<size;i++){
			 boolean flag = true;
			 double pm10 = GetAirQualityComIndex.getPM10_subIndex(Double.parseDouble(slist.get(i).get("PM10")==null?"0":slist.get(i).get("PM10").toString()));
			 double pm25 = GetAirQualityComIndex.getPM25_subIndex(Double.parseDouble(slist.get(i).get("PM25")==null?"0":slist.get(i).get("PM25").toString()));
			 double so2 = GetAirQualityComIndex.getSO2_subIndex(Double.parseDouble(slist.get(i).get("SO2")==null?"0":slist.get(i).get("SO2").toString()));
			 double no2 = GetAirQualityComIndex.getNO2_subIndex(Double.parseDouble(slist.get(i).get("NO2")==null?"0":slist.get(i).get("NO2").toString()));
			 double co = GetAirQualityComIndex.getCO_subIndex(Double.parseDouble(slist.get(i).get("CO")==null?"0":slist.get(i).get("CO").toString()));
			 double o3 = GetAirQualityComIndex.getO3_subIndex(Double.parseDouble(slist.get(i).get("O3")==null?"0":slist.get(i).get("O3").toString()));
			 for(int j=0;j<nslist.size();j++){
				 if(nslist.get(j).get("mn").equals(slist.get(i).get("mn"))){
					 flag = false;
					 @SuppressWarnings("unchecked")
					 List<Map<String,Object>> dataList = (List<Map<String, Object>>) nslist.get(j).get("dataList");
					 Map<String,Object> map = new HashMap<String,Object>();
					 map.put("time", slist.get(i).get("RECEIVETIME"));
					 Map<String,Object> mapvalue = new HashMap<String,Object>();
					 mapvalue.put("value", Double.parseDouble(slist.get(i).get("PM10")==null?"0":slist.get(i).get("PM10").toString()));
					 mapvalue.put("index", pm10);
					 map.put("PM10", mapvalue);
					 
					 mapvalue = new HashMap<String,Object>();
					 mapvalue.put("value", Double.parseDouble(slist.get(i).get("PM25")==null?"0":slist.get(i).get("PM25").toString()));
					 mapvalue.put("index", pm25);
					 map.put("PM25", mapvalue);
					 
					 mapvalue = new HashMap<String,Object>();
					 mapvalue.put("value", Double.parseDouble(slist.get(i).get("SO2")==null?"0":slist.get(i).get("SO2").toString()));
					 mapvalue.put("index", so2);
					 map.put("SO2", mapvalue);
					 
					 mapvalue = new HashMap<String,Object>();
					 mapvalue.put("value", Double.parseDouble(slist.get(i).get("NO2")==null?"0":slist.get(i).get("NO2").toString()));
					 mapvalue.put("index", no2);
					 map.put("NO2", mapvalue);
					 
					 mapvalue = new HashMap<String,Object>();
					 mapvalue.put("value", Double.parseDouble(slist.get(i).get("CO")==null?"0":slist.get(i).get("CO").toString()));
					 mapvalue.put("index", co);
					 map.put("CO", mapvalue);
					 
					 mapvalue = new HashMap<String,Object>();
					 mapvalue.put("value", Double.parseDouble(slist.get(i).get("O3")==null?"0":slist.get(i).get("O3").toString()));
					 mapvalue.put("index", o3);
					 map.put("O3", mapvalue);
					 
					 mapvalue = new HashMap<String,Object>();
					 mapvalue.put("value", Double.parseDouble(slist.get(i).get("AQI")==null?"0":slist.get(i).get("AQI").toString()));
					 mapvalue.put("index", o3+co+no2+so2+pm25+pm10);
					 map.put("API", mapvalue);
					 
					 map.put("humidity", slist.get(i).get("humidity"));
					 map.put("speed", slist.get(i).get("speed"));
					 map.put("speedDir", slist.get(i).get("speedDir"));
					 dataList.add(map);
				 }
			 }
			 if(flag){
				 Map<String,Object> mapall = new HashMap<String,Object>();
				 mapall.put("stationName", slist.get(i).get("name"));
				 mapall.put("mn", slist.get(i).get("mn"));
				 List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				 Map<String,Object> map = new HashMap<String,Object>();
				 map.put("time", slist.get(i).get("RECEIVETIME"));
				 
				 Map<String,Object> mapChild = new HashMap<String,Object>();
				 mapChild.put("index", Double.parseDouble(slist.get(i).get("PM10")==null?"0":slist.get(i).get("PM10").toString()));
				 mapChild.put("value", pm10);
				 map.put("PM10", mapChild);
				 
				 mapChild = new HashMap<String,Object>();
				 mapChild.put("index", Double.parseDouble(slist.get(i).get("PM25")==null?"0":slist.get(i).get("PM25").toString()));
				 mapChild.put("value", pm25);
				 map.put("PM25", mapChild);
				 
				 mapChild = new HashMap<String,Object>();
				 mapChild.put("index", Double.parseDouble(slist.get(i).get("SO2")==null?"0":slist.get(i).get("SO2").toString()));
				 mapChild.put("value", so2);
				 map.put("SO2", mapChild);
				 
				 mapChild = new HashMap<String,Object>();
				 mapChild.put("index", Double.parseDouble(slist.get(i).get("NO2")==null?"0":slist.get(i).get("NO2").toString()));
				 mapChild.put("value", no2);
				 map.put("NO2", mapChild);
				 
				 mapChild = new HashMap<String,Object>();
				 mapChild.put("index", Double.parseDouble(slist.get(i).get("CO")==null?"0":slist.get(i).get("CO").toString()));
				 mapChild.put("value", co);
				 map.put("CO", mapChild);
				 
				 mapChild = new HashMap<String,Object>();
				 mapChild.put("index", Double.parseDouble(slist.get(i).get("O3")==null?"0":slist.get(i).get("O3").toString()));
				 mapChild.put("value", o3);
				 map.put("O3", mapChild);
				 
				 mapChild = new HashMap<String,Object>();
				 mapChild.put("index", Double.parseDouble(slist.get(i).get("API")==null?"0":slist.get(i).get("API").toString()));
				 mapChild.put("value", o3+co+no2+so2+pm25+pm10);
				 map.put("API", mapChild);
				 map.put("humidity", slist.get(i).get("humidity"));
				 map.put("speed", slist.get(i).get("speed"));
				 map.put("speedDir", slist.get(i).get("speedDir")); 
				 dataList.add(map);
				 mapall.put("dataList",dataList);
				 nslist.add(mapall);
			 }
		 }
		 List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		 for(int i=0,size = nslist.size();i<size;i++){
			 if (i >= ((Integer.parseInt(pageNum)) * 10)
						 && i < ((Integer.parseInt(pageNum) + 1) * 10)){
				 list.add(nslist.get(i));
			 }
		 }
		return list;
	}

	@Override
	public Object queryRealDispatchByWeather(String userId, String dataType,
			String pageNum) {
		List<Map<String, Object>> nslist = new ArrayList<Map<String, Object>>();
		String sql = "";
		String bString = "";
		String fString = "";
		sql = "select a.mn \"mn\",a.name \"name\",b.mc as \"grid\"  from T_bas_gkz a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> gkzList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_station a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> sList = jdbcTemplate.queryForList(sql);
		sql = "select a.mn \"mn\",a.name \"name\",a.fmn,b.mc as \"grid\"  from base_site a left join wgh_wghf b on a.area_bh=b.bh ";
		if (userId != null) {
			sql += "where a.INSTITUTION_BH like (select INSTITUTION_BH from XT_YHXX where id='"
					+ userId + "')||'%' and a.display = 1";
		}
		List<Map<String, Object>> siteList = jdbcTemplate.queryForList(sql);
		String stationString = "";
		for (int i = 0,size = siteList.size(); i < size; i++) {
			stationString += "'" + siteList.get(i).get("mn") + "',";
		}
		for (int i = 0,size = sList.size(); i < size; i++) {
			stationString += "'" + sList.get(i).get("mn") + "',";
		}
		for (int i = 0,size = gkzList.size(); i < size; i++) {
			stationString += "'" + gkzList.get(i).get("mn") + "',";
		}
		if (!"".equals(stationString)) {
			stationString = stationString.substring(0,
					stationString.length() - 1);
		}
		String endTime = "";
		String startTime = "";
		String sString = "";
		if ("0".equals(dataType)) {// 分钟
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.HOUR, -4);
			SimpleDateFormat YMDHM=new SimpleDateFormat("yyyy-MM-dd HH:mm");
			startTime = YMDHM.format(c.getTime());
			endTime = YMDHM.format(date);
			SimpleDateFormat month=new SimpleDateFormat("yyyyMM");
			bString = "t_cal_minutes_" + month.format(date);
			fString = "yyyy-mm-dd HH24:mi";
			sString = "HH24:mi";
		} else if ("1".equals(dataType)) {// 小时
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.HOUR, -24);
			SimpleDateFormat historydate=new SimpleDateFormat("yyyy-MM-dd HH");
			startTime = historydate.format(c.getTime());
			endTime = historydate.format(date);
			bString = "t_cal_hour";
			fString = "yyyy-mm-dd HH24";
			sString = "mm-dd HH24";
		} else if ("2".equals(dataType)) {// 天
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.MONTH, -1);
			SimpleDateFormat secondA=new SimpleDateFormat("yyyy-MM-dd");
			startTime = secondA.format(c.getTime());
			endTime = secondA.format(date);
			bString = "t_cal_day";
			fString = "yyyy-mm-dd";
			sString = "mm-dd";
		}
		sql = "select mn,name,avg(PM10) PM10,avg(PM25) PM25,avg(SO2) SO2,avg(NO2) NO2,avg(CO) CO,avg(O3) O3,avg(AQI) AQI,avg(TEMPERATURE) \"temperature\",avg(HUMIDITY) \"humidity\",avg(GUSTSPEED) \"speed\",avg(GUSTDIR) \"speedDir\",avg(pressure) \"pressure\",to_char(receivetime,'"
				+ sString
				+ "') receivetime from (select nvl(nvl(a.mn,c.mn),d.mn) as mn,nvl(nvl(a.name,c.name),d.name) as name,max(b.PM10) PM10,max(b.PM25) PM25,max(b.SO2) SO2,max(b.NO2) NO2,max(b.CO) CO,max(b.O3) O3,max(b.AQI) AQI,max(b.TEMPERATURE) TEMPERATURE,max(b.HUMIDITY) HUMIDITY,max(b.GUSTSPEED) GUSTSPEED,max(b.GUSTDIR) GUSTDIR,max(b.pressure) pressure,b.receivetime from  "
				+ bString
				+ " b  left join base_station a on a.mn = b.stationcode left join base_site c on c.mn=b.stationcode left join T_bas_gkz d on d.mn=b.stationcode  where b.receivetime>=to_date('"
				+ startTime
				+ "','"
				+ fString
				+ "') and b.receivetime<=to_date('"
				+ endTime
				+ "','"
				+ fString
				+ "') and (a.mn in ("
				+ stationString
				+ ") or c.mn in ("
				+ stationString
				+ ") or d.mn in ("
				+ stationString
				+ ")) group by b.receivetime,nvl(nvl(a.name,c.name),d.name),nvl(nvl(a.mn,c.mn),d.mn) order by b.receivetime) group by to_char(receivetime,'"
				+ sString + "'),name,mn order by name,receivetime";
		System.out.println(sql);
		 List<Map<String, Object>> slist = jdbcTemplate.queryForList(sql);
		 for(int i=0,size = slist.size();i<size;i++){
			 boolean flag = true;
			 for(int j=0;j<nslist.size();j++){
				 if(nslist.get(j).get("mn").equals(slist.get(i).get("mn"))){
					 flag = false;
					 @SuppressWarnings("unchecked")
					 List<Map<String,Object>> dataList = (List<Map<String, Object>>) nslist.get(j).get("dataList");
					 Map<String,Object> map = new HashMap<String,Object>();
					 map.put("time", slist.get(i).get("RECEIVETIME"));
					 map.put("temperature", slist.get(i).get("temperature"));
					 map.put("pressure", slist.get(i).get("pressure"));
					 map.put("speedDirCn", FormatUtil.windDireTransfer(Double.parseDouble(slist.get(i).get("speedDir")==null?"0":slist.get(i).get("speedDir").toString())));
					 map.put("humidity", slist.get(i).get("humidity"));
					 map.put("speed", slist.get(i).get("speed"));
					 map.put("speedDir", slist.get(i).get("speedDir"));
					 map.put("rain",0);
					 dataList.add(map);
				 }
			 }
			 if(flag){
				 Map<String,Object> mapall = new HashMap<String,Object>();
				 mapall.put("stationName", slist.get(i).get("name"));
				 mapall.put("mn", slist.get(i).get("mn"));
				 List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
				 Map<String,Object> map = new HashMap<String,Object>();
				 map.put("time", slist.get(i).get("RECEIVETIME"));
				 map.put("temperature", slist.get(i).get("temperature"));
				 map.put("pressure", slist.get(i).get("pressure"));
				 map.put("speedDirCn", FormatUtil.windDireTransfer(Double.parseDouble(slist.get(i).get("speedDir")==null?"0":slist.get(i).get("speedDir").toString())));
				 map.put("humidity", slist.get(i).get("humidity"));
				 map.put("speed", slist.get(i).get("speed"));
				 map.put("speedDir", slist.get(i).get("speedDir"));
				 map.put("rain",0);
				 dataList.add(map);
				 mapall.put("dataList",dataList);
				 nslist.add(mapall);
			 }
		 }
		 List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
		 for(int i=0,size = nslist.size();i<size;i++){
			 if (i >= ((Integer.parseInt(pageNum)) * 10)
						 && i < ((Integer.parseInt(pageNum) + 1) * 10)){
				 list.add(nslist.get(i));
			 }
		 }
		return list;
	}

}
