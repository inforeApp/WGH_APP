package com.dao;

public interface RealDao {

	Object queryRealDataByMn(String currentId,String mn);

	Object queryStationDetails(String mn);

	Object queryRealDataRank(String factorType,String userId);

	Object searchRealWarning(String userId, String warningLevel,
			String keyword, String type, String pageNum, String warningLevel_1,
			String warningLevel_2, String warningLevel_3, String warningLevel_4);

	Object queryRealWarning(String userId, String warningLevel, String keyword,
			String type, String pageNum);

	Object queryPhotoByMn(String userId, String mn, String time);

	Object queryRecentlyByMn(String userId, String mn);

	Object queryRealDispatchByAQI(String userId, String dataType, String pageNum);

	Object queryRealDispatchBySynthesis(String userId, String dataType,
			String pageNum);

	Object queryRealDispatchByWeather(String userId, String dataType,
			String pageNum);

}
