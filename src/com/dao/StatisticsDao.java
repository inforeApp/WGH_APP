package com.dao;

public interface StatisticsDao {
	public Object  queryContrastAnalyze(String time, String stationString,String userId,String dataType);

	public Object querySearchPointAnalyze(String dataType, String startTime,
			String endTime, String stationString,String userId);

	public Object queryPointAnalyze(String factorType, String time,
			String stationString,String userId);

	public Object queryHisDataRank(String factorType, String time,
			String historytype,String userId);

	public Object queryStationTree(String userId);

	public Object queryDataAnalysis(String dataType, String startTime,
			String endTime, String stationString, String userId);

	public Object queryPointData(String dataType, String userId, String pageNum,String modelType);

	public Object queryWarningData(String userId, String startTime,
			String endTime);

	public Object queryHeatData(String userId, String mn, String time,
			String factorType);

}
