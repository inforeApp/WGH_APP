package com.service;

import com.model.User;

public interface UserService {

	public Object loginCheck(User user);
	public Object findAllUser(String userId);
	public Object queryPersonPlace(String userId);
	public Object queryPlace(String lnt,String lat,String userId,String address,String area);
	public Object queryPollutionStation(String userId);
	public Object queryGrid(String userId);
	public Object getAccessToken();
	public Object queryNewStationTree(String userId);
	public Object queryRealInfoRank(String factorType,String  stationType,String userId);
	public Object queryTrack(String time, String userId);
	public Object queryRecentlyData(String mn);
	public Object queryStationTree(String userId);
}
