package com.service.implement;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.json.AbstractJsonObject;
import com.json.ListObject;
import com.json.StatusCode;
import com.model.User;
import com.service.UserService;
import com.util.SecurityUtils;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	private Cache ehcache;
	
	@Override
	public Object loginCheck(User user) {
		User u  = userDao.findUserByUserName(user.getAccount());
		if(u!=null&&user.getPassword().equals(u.getPassword())){
			String token = SecurityUtils.string2MD5(new Date().toString()+u.getUserId());
			u.setToken(token);
			Element element = new Element(u.getUserId(), token);
			ehcache.put(element);
			ListObject listObject = new ListObject();
			listObject.setStatus(StatusCode.CODE_SUCCESS);
			listObject.setMessage("登录成功");
			Map<String , Object> map = new HashMap<String, Object>();
			map.put("bean", u);
			listObject.setData(map);
			return listObject;
		}else{
			AbstractJsonObject ajObject = new AbstractJsonObject();
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("登录失败");
			return ajObject;
		}
	}


	@Override
	public Object findAllUser(String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取全部人员成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.findAllUser(userId));
		listObject.setData(map);
		return listObject;
	}

	public Object queryPersonPlace(String userId){
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取全部人员成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.queryPersonPlace(userId));
		listObject.setData(map);
		return listObject;
	}

	public Object queryPlace(String lnt,String lat,String userId,String address,String area){
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if (userDao.queryPlace(lnt,lat,userId,address,area)>0) {
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("获取成功");
		}else{
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("获取失败");
		}
		return ajObject;
	}



	@Override
	public Object queryPollutionStation(String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.queryPollutionStation(userId));
		listObject.setData(map);
		return listObject;
	}


	@Override
	public Object queryGrid(String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取网格成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.queryGrid(userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object getAccessToken() {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取视频token成功");
		listObject.setData(userDao.getAccessToken());
		return listObject;
	}

	public Cache getEhcache() {
		return ehcache;
	}

	public void setEhcache(Cache ehcache) {
		this.ehcache = ehcache;
	}


	@Override
	public Object queryNewStationTree(String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.queryNewStationTree(userId));
		listObject.setData(map);
		return listObject;
	}
	@Override
	public Object queryStationTree(String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.queryStationTree(userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryRealInfoRank(String factorType,String  stationType,String userId) {
		try {ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.queryRealInfoRank(factorType, stationType,userId));
		Properties prop = new Properties();
		// 读取属性文件a.properties
		InputStream in = getClass().getResourceAsStream(
				"//YsRadio.properties");
		prop.load(in);
		Map<String , Object> pointmap = new HashMap<String, Object>();
		pointmap.put("lat", Double.parseDouble(prop.get("lat").toString()));
		pointmap.put("lng",  Double.parseDouble(prop.get("lng").toString()));
		map.put("mapCenter", pointmap);
		listObject.setData(map);
		return listObject;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Object queryTrack(String time, String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.queryTrack(time,userId));
		listObject.setData(map);
		return listObject;
	}


	@Override
	public Object queryRecentlyData(String mn) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", userDao.queryRecentlyData(mn));
		listObject.setData(map);
		return listObject;
	}




}