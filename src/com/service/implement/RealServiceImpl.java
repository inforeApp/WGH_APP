package com.service.implement;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.RealDao;
import com.json.ListObject;
import com.json.StatusCode;
import com.service.RealService;
@Service
public class RealServiceImpl implements RealService{
	@Autowired
	private RealDao realDao;

	@Override
	public Object queryRealDataByMn(String currentId,String mn) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("bean", realDao.queryRealDataByMn(currentId,mn));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryStationDetails(String mn) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("bean", realDao.queryStationDetails(mn));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryRealDataRank(String factorType,String userId) {
		if ("1".equals(factorType)) {
			factorType = "PM10";
		} else if ("2".equals(factorType)) {
			factorType = "PM25";
		} else if ("3".equals(factorType)) {
			factorType = "SO2";
		} else if ("4".equals(factorType)) {
			factorType = "NO2";
		} else if ("5".equals(factorType)) {
			factorType = "CO";
		} else if ("6".equals(factorType)) {
			factorType = "O3";
		} else if ("0".equals(factorType)) {
			factorType = "AQI";
		}
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", realDao.queryRealDataRank(factorType,userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object searchRealWarning(String userId, String warningLevel,
			String keyword, String type, String pageNum, String warningLevel_1,
			String warningLevel_2, String warningLevel_3, String warningLevel_4) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", realDao.searchRealWarning(userId, warningLevel,
				keyword, type, pageNum, warningLevel_1,
				warningLevel_2, warningLevel_3,
				warningLevel_4));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryRealWarning(String userId, String warningLevel,
			String keyword, String type, String pageNum) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",realDao.queryRealWarning(userId, warningLevel,
				keyword, type, pageNum));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryPhotoByMn(String userId, String mn, String time) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",realDao.queryPhotoByMn(userId,mn,time));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryRecentlyByMn(String userId, String mn) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",realDao.queryRecentlyByMn(userId,mn));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryRealDispatchByAQI(String userId, String dataType,
			String pageNum) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",realDao.queryRealDispatchByAQI(userId,dataType,pageNum));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryRealDispatchBySynthesis(String userId, String dataType,
			String pageNum) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",realDao.queryRealDispatchBySynthesis(userId,dataType,pageNum));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryRealDispatchByWeather(String userId, String dataType,
			String pageNum) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",realDao.queryRealDispatchByWeather(userId,dataType,pageNum));
		listObject.setData(map);
		return listObject;
	}
	
	
}
