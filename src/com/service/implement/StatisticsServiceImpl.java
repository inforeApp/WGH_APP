package com.service.implement;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.StatisticsDao;
import com.json.ListObject;
import com.json.StatusCode;
import com.service.StatisticsService;
@Service
public class StatisticsServiceImpl implements StatisticsService{
	@Autowired
	private StatisticsDao statisticsDao;

	@Override
	public Object queryContrastAnalyze(String time, String stationString,String userId,String dataType) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList", statisticsDao.queryContrastAnalyze(time,stationString,userId,dataType));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object querySearchPointAnalyze(String dataType, String startTime,
			String endTime, String stationString,String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",statisticsDao.querySearchPointAnalyze(dataType, startTime, endTime,stationString,userId));
		listObject.setData(map);
		return listObject;
	}
	
	@Override
	public Object queryPointData(String dataType, String userId, String pageNum,String modelType) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",statisticsDao.queryPointData(dataType,userId,pageNum,modelType));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryDataAnalysis(String dataType, String startTime,
			String endTime, String stationString,String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",statisticsDao.queryDataAnalysis(dataType, startTime, endTime,stationString,userId));
		listObject.setData(map);
		return listObject;
	}
	@Override
	public Object queryPointAnalyze(String factorType, String time,
			String stationString,String userId) {
		if ("1".equals(factorType)) {
			factorType = "PM10";
		} else if ("2".equals(factorType)) {
			factorType = "PM25";
		} else if ("3".equals(factorType)) {
			factorType = "SO2";
		} else if ("4".equals(factorType)) {
			factorType = "NO2";
		} else if ("5".equals(factorType)) {
			factorType = "CO";
		} else if ("6".equals(factorType)) {
			factorType = "O3";
		} else {
			factorType = "PM10";
		}
		
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",statisticsDao.queryPointAnalyze(factorType, time,stationString,userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryHisDataRank(String factorType, String time,
			String historytype,String userId) {
		if ("1".equals(factorType)) {
			factorType = "PM10";
		} else if ("2".equals(factorType)) {
			factorType = "PM25";
		} else if ("3".equals(factorType)) {
			factorType = "SO2";
		} else if ("4".equals(factorType)) {
			factorType = "NO2";
		} else if ("5".equals(factorType)) {
			factorType = "CO";
		} else if ("6".equals(factorType)) {
			factorType = "O3";
		} else if ("0".equals(factorType)) {
			factorType = "AQI";
		}
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",statisticsDao.queryHisDataRank(factorType, time,historytype,userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryStationTree(String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",statisticsDao.queryStationTree(userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryWarningData(String userId, String startTime,
			String endTime) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",statisticsDao.queryWarningData(userId,startTime,endTime));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryHeatData(String userId, String mn, String time,
			String factorType) {
		if ("1".equals(factorType)) {
			factorType = "PM10";
		} else if ("2".equals(factorType)) {
			factorType = "PM25";
		} else if ("3".equals(factorType)) {
			factorType = "SO2";
		} else if ("4".equals(factorType)) {
			factorType = "NO2";
		} else if ("5".equals(factorType)) {
			factorType = "CO";
		} else if ("6".equals(factorType)) {
			factorType = "O3";
		} else if ("0".equals(factorType)) {
			factorType = "AQI";
		}
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String , Object> map = new HashMap<String, Object>();
		map.put("beanList",statisticsDao.queryHeatData(userId,mn,time,factorType));
		listObject.setData(map);
		return listObject;
	}
	
	
}
