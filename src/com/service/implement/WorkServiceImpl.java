package com.service.implement;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.dao.WorkDao;
import com.json.AbstractJsonObject;
import com.json.ListObject;
import com.json.StatusCode;
import com.service.WorkService;

@Service
public class WorkServiceImpl implements WorkService{
	@Autowired
	private WorkDao workDao;

	@Override
	public Object newQuertionReport(String quertionTitle, String quertionNum,
			String releaseTime, String gridNum, String questionAddress,
			MultipartFile[] file,String questionContent,String userId) {
		int num = workDao.newQuertionReport(quertionTitle, quertionNum, releaseTime, gridNum, questionAddress, file,questionContent,userId);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("新建问题报告失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("新建问题报告成功");
		}
		return ajObject;
	}

	@Override
	public Object queryQuestionReport(String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.queryQuestionReport(userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object searchDischargePollution(String state, String keyword,String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.searchDischargePollution(state,keyword,userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object searchPollutionByMn(String mn) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("bean", workDao.searchPollutionByMn(mn));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object searchDischargePollutionByMn(String mn) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.searchDischargePollutionByMn(mn));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object newMission(String title, String mn, String issuePsonId,
			String issueTime, String lastTime, String handlePsonId,
			String discrib, String isApproved, String approvePson,
			MultipartFile[] voice, MultipartFile[] file,String leaderId) {
		int num = workDao.newMission(title,mn,issuePsonId,issueTime,lastTime,handlePsonId,discrib,isApproved,approvePson,voice,file,leaderId);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("新建任务失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("新建任务成功");
		}
		return ajObject;
	}

	@Override
	public Object queryGtasks(String userId, String type,String keyword,String state,String startTime,String endTime) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.queryGtasks(userId,type,keyword,state,startTime,endTime));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object searchPollution(String keyword, String state,String userId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.searchPollution(keyword,state,userId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object ResearchAskRecord(String RecorderJC, String NotesJC,
			String NotesAsk, String obtainEvidence,
			MultipartFile[] qySignature, MultipartFile[] zfSignature,
			String missionId, String userId, String mn,MultipartFile[]files,String lat,String lng,String address) {
		int num = workDao.ResearchAskRecord(RecorderJC,NotesJC,NotesAsk,obtainEvidence,qySignature,zfSignature,missionId,userId,mn,files,lat,lng,address);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("新建执法询问失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("新建执法询问成功");
		}
		return ajObject;
	}

	@Override
	public Object SceneEvidence(String lat, String lnt, String address,
			String pson, String mn, MultipartFile[] pic, MultipartFile[] voice,
			MultipartFile[] video) {
		int num = workDao.SceneEvidence(lat,lnt,address,pson,mn,pic,voice,video);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("执法取证失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("执法取证成功");
		}
		return ajObject;
	}

	@Override
	public Object checkMission(String mission_id, String state, String describ,
			String time) {
		int num = workDao.checkMission(mission_id,state,describ,time);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("审核失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("审核成功");
		}
		return ajObject;
	}

	@Override
	public Object changeMissionStatus(String mission_id, String userId,String describ) {
		int num = workDao.changeMissionStatus(mission_id,userId,describ);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("修改任务状态失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("修改任务状态成功");
		}
		return ajObject;
	}

	@Override
	public Object queryProcess(String mission_id) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("bean", workDao.queryProcess(mission_id));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object querySupervise(String id) {
		return null;
	}

	@Override
	public Object queryLedger(String userId,String pageNum, String mn, String keyword) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.queryLedger(userId,pageNum,mn,keyword));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryLedgerById(String userId, String ledgerId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("bean", workDao.queryLedgerById(userId,ledgerId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryDetailsById(String id, String state) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("bean", workDao.queryDetailsById(id,state));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object deleteMission(String userId, String missionId) {
		int num = workDao.deleteMission(userId,missionId);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("修改任务状态失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("修改任务状态成功");
		}
		return ajObject;
	}

	@Override
	public Object queryCheckMission(String userId, String state) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.queryCheckMission(userId,state));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object queryRecordList(String userId,String dataType,String pageNum,String recordType) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.queryRecordList(userId,dataType,pageNum,recordType));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object newRecord(String userId, String happenTime, String mn,
			String dangerLevel, String factor, String describ, String dispatcher,String recordTitle) {
		int num = workDao.newRecord(userId,happenTime,mn,dangerLevel,factor,describ,dispatcher,recordTitle);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("新建事件失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("新建事件成功");
		}
		return ajObject;
	}

	@Override
	public Object queryRecordDetails(String userId, String recordId) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("bean", workDao.queryRecordDetails(userId,recordId));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object handleRecord(String userId, String recordId,
			String handleDescrib, MultipartFile[] files,String lat,String lnt,String address) {
		int num = workDao.handleRecord(userId,recordId,handleDescrib,files,lat,lnt,address);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("事件处理失败");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("事件处理成功");
		}
		return ajObject;
	}

	@Override
	public Object queryRecordVerify(String userId, String recordId) {
		int num = workDao.queryRecordVerify(userId,recordId);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("事件已确认或已处理了");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("事件确认成功");
		}
		return ajObject;
	}

	@Override
	public Object deleteRecord(String userId, String recordId) {
		int num = workDao.deleteRecord(userId,recordId);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("事件已处理了");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("事件删除成功");
		}
		return ajObject;
	}

	@Override
	public Object updateRecord(String recordId, String userId,
			String happenTime, String mn, String dangerLevel, String factor,
			String describ, String dispatcher, String deadLine,String recordTitle) {
		int num = workDao.updateRecord(recordId,userId,happenTime,mn,dangerLevel,factor,describ,dispatcher,deadLine,recordTitle);
		AbstractJsonObject ajObject = new AbstractJsonObject();
		if(num==0){
			ajObject.setStatus(StatusCode.CODE_ERROR);
			ajObject.setMessage("事件已处理了");
		}else{
			ajObject.setStatus(StatusCode.CODE_SUCCESS);
			ajObject.setMessage("事件更新成功");
		}
		return ajObject;
	}

	@Override
	public Object handleRecordList(String userId, String pageNum) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.handleRecordList(userId,pageNum));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object sreachRecordList(String userId, String pageNum,
			String keyword, String dataType) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.sreachRecordList(userId,pageNum,keyword,dataType));
		listObject.setData(map);
		return listObject;
	}

	@Override
	public Object sreachHandleRecordList(String userId, String pageNum,
			String keyword) {
		ListObject listObject = new ListObject();
		listObject.setStatus(StatusCode.CODE_SUCCESS);
		listObject.setMessage("获取成功");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("beanList", workDao.sreachHandleRecordList(userId,pageNum,keyword));
		listObject.setData(map);
		return listObject;
	}
	
}
