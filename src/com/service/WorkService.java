package com.service;


import org.springframework.web.multipart.MultipartFile;

public interface WorkService {

	Object newQuertionReport(String quertionTitle, String quertionNum,
			String releaseTime, String gridNum, String questionAddress,
			MultipartFile[] file,String questionContent,String userId);

	Object queryQuestionReport(String userId);

	Object searchDischargePollution(String state, String keyword,String userId);

	Object searchPollutionByMn(String mn);

	Object searchDischargePollutionByMn(String mn);

	Object newMission(String title, String mn, String issuePsonId,
			String issueTime, String lastTime, String handlePsonId,
			String discrib, String isApproved, String approvePson,
			MultipartFile[] voice, MultipartFile[] file,String leaderId);

	Object queryGtasks(String userId, String type,String keyword,String state,String startTime,String endTime);

	Object searchPollution(String keyword, String state,String userId);


	Object SceneEvidence(String lat, String lnt, String address, String pson,
			String mn, MultipartFile[] pic, MultipartFile[] voice,
			MultipartFile[] video);

	Object checkMission(String mission_id, String state, String describ,
			String time);

	Object changeMissionStatus(String mission_id, String userId,String describ);

	Object queryProcess(String mission_id);

	Object ResearchAskRecord(String RecorderJC, String NotesJC,
			String NotesAsk, String obtainEvidence,
			MultipartFile[] qySignature, MultipartFile[] zfSignature,
			String missionId, String userId, String mn,MultipartFile[] file,String lat,String lng,String address);

	Object querySupervise(String id);

	Object queryLedger(String userId,String pageNum, String mn, String keyword);

	Object queryDetailsById(String id, String state);

	Object deleteMission(String userId, String missionId);

	Object queryCheckMission(String userId, String state);

	Object queryLedgerById(String userId, String ledgerId);

	Object queryRecordList(String userId,String dataType,String pageNum,String recordType);

	Object newRecord(String userId, String happenTime, String mn,
			String dangerLevel, String factor, String describ, String dispatcher,String recordTitle);

	Object handleRecord(String userId, String recordId, String handleDescrib,
			MultipartFile[] files,String lat,String lnt,String address);

	Object queryRecordDetails(String userId, String recordId);

	Object queryRecordVerify(String userId, String recordId);

	Object deleteRecord(String userId, String recordId);

	Object updateRecord(String recordId, String userId, String happenTime,
			String mn, String dangerLevel, String factor, String describ,
			String dispatcher, String deadLine,String recordTitle);

	Object handleRecordList(String userId, String pageNum);

	Object sreachRecordList(String userId, String pageNum, String keyword,
			String dataType);

	Object sreachHandleRecordList(String userId, String pageNum,
			String keyword);

}
