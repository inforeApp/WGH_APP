package com.model;

import java.text.ParseException;

import com.util.FormatUtil;


public class NewPoint implements Comparable<NewPoint>{
	private String time;
	private double value;
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	
	public int compareTo(NewPoint o) {
			try {
				String sstring = "";
				String eString ="";
				if(time.contains(" ")){
					sstring = time +":00:00";
					eString = o.getTime()+":00:00";
				}else if(time.length()==7){
					sstring = time +"-01 00:00:00";
					eString = o.getTime()+"-01 00:00:00";
				}else {
					sstring = time +" 00:00:00";
					eString = o.getTime()+" 00:00:00";
				}
				return (FormatUtil.secondG.parse(sstring).getTime()-FormatUtil.secondG.parse(eString).getTime())>0?1:-1;
			} catch (ParseException e) {
				e.printStackTrace();
			}
		return 0;
	}
}
