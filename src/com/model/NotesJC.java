package com.model;

import java.util.List;
import java.util.Map;

/**
 *  检查笔录
 * @author Administrator
 *
 */
public class NotesJC {
	//基本信息
    private String startTime;//时间
    private String endTime;
    private String address;//地点
    private List<Map<String,Object>> inspectPson ;//检查人
    private List<Map<String,Object>> notePson; //记录人
    private String unit; //工作单位
    private String leaderName ; //现场负责人姓名
    private String leaderAge ; //现场负责人年龄
    private String identify ; //身份证
    private String company; //被检查单位
    private String position; //职位
    private String relationship ;//与本案关系
    private String addressInspect ;// 检查地点
    private String postCode ; //邮编
    private String phone;
    private String others; //其他参加人和工作单位
    private int evite ; // 1 清楚了 不回避 0 申请回避
    private String eviteReason; //申请回避的原因
    //笔录模板
    private List<NoteModel> noteModels;  //一问一答的形式
    
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List<Map<String, Object>> getInspectPson() {
		return inspectPson;
	}
	public void setInspectPson(List<Map<String, Object>> inspectPson) {
		this.inspectPson = inspectPson;
	}
	public List<Map<String, Object>> getNotePson() {
		return notePson;
	}
	public void setNotePson(List<Map<String, Object>> notePson) {
		this.notePson = notePson;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getLeaderName() {
		return leaderName;
	}
	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}
	public String getLeaderAge() {
		return leaderAge;
	}
	public void setLeaderAge(String leaderAge) {
		this.leaderAge = leaderAge;
	}
	public String getIdentify() {
		return identify;
	}
	public void setIdentify(String identify) {
		this.identify = identify;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getAddressInspect() {
		return addressInspect;
	}
	public void setAddressInspect(String addressInspect) {
		this.addressInspect = addressInspect;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getOthers() {
		return others;
	}
	public void setOthers(String others) {
		this.others = others;
	}
	public int getEvite() {
		return evite;
	}
	public void setEvite(int evite) {
		this.evite = evite;
	}
	public List<NoteModel> getNoteModels() {
		return noteModels;
	}
	public void setNoteModels(List<NoteModel> noteModels) {
		this.noteModels = noteModels;
	}
	public String getEviteReason() {
		return eviteReason;
	}
	public void setEviteReason(String eviteReason) {
		this.eviteReason = eviteReason;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "NotesJC [startTime=" + startTime + ", endTime=" + endTime
				+ ", address=" + address + ", inspectPson=" + inspectPson
				+ ", notePson=" + notePson + ", unit=" + unit + ", leaderName="
				+ leaderName + ", leaderAge=" + leaderAge + ", identify="
				+ identify + ", company=" + company + ", position=" + position
				+ ", relationship=" + relationship + ", addressInspect="
				+ addressInspect + ", postCode=" + postCode + ", others="
				+ others + ", evite=" + evite + ", eviteReason=" + eviteReason
				+ ", noteModels=" + noteModels + "]";
	}

}
