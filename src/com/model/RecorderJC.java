package com.model;

import java.util.List;
import java.util.Map;

/**
 *	现场监察记录
 * @author Administrator
 *
 */
public class RecorderJC {
	 private String leaderName;//现场负责人
     private String leaderPosition;//负责人职务
     private String leaderPhone;//负责人电话
     private String supervise;//监察内容
     private String produceStatus; //生产状态
     private String examineFail; //未经环评审批的新建项目
     private String threeTogether; //未执行“三同时”建设项目
     private String buildAndAccept;//污染设施建设、验收和运行情况
     private String systemInstall;//自动监控系统安装情况
     private String systemOperate;//自动监控系统运行情况
     private String systemInternet;//自动监控系统联网情况
     private String systemAccept;//自动监控系统验收情况
     private String onlineData;//在线监测数据
     private String waterLet;//废水排放情况
     private String airLet;//废气排放情况
     private String defaultWaste;//一般固废
     private String defaultTransfer;//一般固废暂存转移
     private String dangerWaste;//危险废物
     private String dangerTransfer;//危险固废暂存转移
     private String toteOffice;//环保管理机构
     private String wasteLedger;//污染设施运行台账
     private String emergencyCase;//环境应急预案
     private String conclusion;//现场检查结论
     private String detailRequire;//处理意见及相关要求
     private String wasteLicence; //排污许可证
     private List<Map<String,Object>> inspectPson ;//检查人(多人)n
	@Override
	public String toString() {
		return "RecorderJC [leaderName=" + leaderName + ", leaderPosition="
				+ leaderPosition + ", leaderPhone=" + leaderPhone
				+ ", supervise=" + supervise + ", produceStatus="
				+ produceStatus + ", examineFail=" + examineFail
				+ ", threeTogether=" + threeTogether + ", buildAndAccept="
				+ buildAndAccept + ", systemInstall=" + systemInstall
				+ ", systemOperate=" + systemOperate + ", systemInternet="
				+ systemInternet + ", systemAccept=" + systemAccept
				+ ", onlineData=" + onlineData + ", waterLet=" + waterLet
				+ ", airLet=" + airLet + ", defaultWaste=" + defaultWaste
				+ ", defaultTransfer=" + defaultTransfer + ", dangerWaste="
				+ dangerWaste + ", dangerTransfer=" + dangerTransfer
				+ ", toteOffice=" + toteOffice + ", wasteLedger=" + wasteLedger
				+ ", emergencyCase=" + emergencyCase + ", conclusion="
				+ conclusion + ", detailRequire=" + detailRequire + "]";
	}
	public String getLeaderName() {
		return leaderName;
	}
	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}
	public String getLeaderPosition() {
		return leaderPosition;
	}
	public void setLeaderPosition(String leaderPosition) {
		this.leaderPosition = leaderPosition;
	}
	public String getLeaderPhone() {
		return leaderPhone;
	}
	public void setLeaderPhone(String leaderPhone) {
		this.leaderPhone = leaderPhone;
	}
	public String getSupervise() {
		return supervise;
	}
	public void setSupervise(String supervise) {
		this.supervise = supervise;
	}
	public String getProduceStatus() {
		return produceStatus;
	}
	public void setProduceStatus(String produceStatus) {
		this.produceStatus = produceStatus;
	}
	public String getExamineFail() {
		return examineFail;
	}
	public void setExamineFail(String examineFail) {
		this.examineFail = examineFail;
	}
	public String getThreeTogether() {
		return threeTogether;
	}
	public void setThreeTogether(String threeTogether) {
		this.threeTogether = threeTogether;
	}
	public String getBuildAndAccept() {
		return buildAndAccept;
	}
	public void setBuildAndAccept(String buildAndAccept) {
		this.buildAndAccept = buildAndAccept;
	}
	public String getSystemOperate() {
		return systemOperate;
	}
	public void setSystemOperate(String systemOperate) {
		this.systemOperate = systemOperate;
	}
	public String getSystemInternet() {
		return systemInternet;
	}
	public void setSystemInternet(String systemInternet) {
		this.systemInternet = systemInternet;
	}
	public String getSystemAccept() {
		return systemAccept;
	}
	public void setSystemAccept(String systemAccept) {
		this.systemAccept = systemAccept;
	}
	public String getOnlineData() {
		return onlineData;
	}
	public void setOnlineData(String onlineData) {
		this.onlineData = onlineData;
	}
	public String getWaterLet() {
		return waterLet;
	}
	public void setWaterLet(String waterLet) {
		this.waterLet = waterLet;
	}
	public String getAirLet() {
		return airLet;
	}
	public void setAirLet(String airLet) {
		this.airLet = airLet;
	}
	public String getDefaultWaste() {
		return defaultWaste;
	}
	public void setDefaultWaste(String defaultWaste) {
		this.defaultWaste = defaultWaste;
	}
	public String getDefaultTransfer() {
		return defaultTransfer;
	}
	public void setDefaultTransfer(String defaultTransfer) {
		this.defaultTransfer = defaultTransfer;
	}
	public String getDangerWaste() {
		return dangerWaste;
	}
	public void setDangerWaste(String dangerWaste) {
		this.dangerWaste = dangerWaste;
	}
	public String getDangerTransfer() {
		return dangerTransfer;
	}
	public void setDangerTransfer(String dangerTransfer) {
		this.dangerTransfer = dangerTransfer;
	}
	public String getToteOffice() {
		return toteOffice;
	}
	public void setToteOffice(String toteOffice) {
		this.toteOffice = toteOffice;
	}
	public String getWasteLedger() {
		return wasteLedger;
	}
	public void setWasteLedger(String wasteLedger) {
		this.wasteLedger = wasteLedger;
	}
	public String getEmergencyCase() {
		return emergencyCase;
	}
	public void setEmergencyCase(String emergencyCase) {
		this.emergencyCase = emergencyCase;
	}
	public String getConclusion() {
		return conclusion;
	}
	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}
	public String getDetailRequire() {
		return detailRequire;
	}
	public void setDetailRequire(String detailRequire) {
		this.detailRequire = detailRequire;
	}
	public String getSystemInstall() {
		return systemInstall;
	}
	public void setSystemInstall(String systemInstall) {
		this.systemInstall = systemInstall;
	}
	public String getWasteLicence() {
		return wasteLicence;
	}
	public void setWasteLicence(String wasteLicence) {
		this.wasteLicence = wasteLicence;
	}
	public List<Map<String, Object>> getInspectPson() {
		return inspectPson;
	}
	public void setInspectPson(List<Map<String, Object>> inspectPson) {
		this.inspectPson = inspectPson;
	}
     
}
