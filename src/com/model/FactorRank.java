package com.model;

public class FactorRank  implements Comparable<FactorRank>{
	private String mn;
	private String factorName;
	private double value;
	private String stationName;
	private int factorTypeId;
	private int stationType;
	private boolean isOnline;
	
	public int getFactorTypeId() {
		return factorTypeId;
	}
	public void setFactorTypeId(int factorTypeId) {
		this.factorTypeId = factorTypeId;
	}
	public String getMn() {
		return mn;
	}
	public void setMn(String mn) {
		this.mn = mn;
	}
	public String getFactorName() {
		return factorName;
	}
	public void setFactorName(String factorName) {
		this.factorName = factorName;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	public int compareTo(FactorRank f) {
		if(!this.isOnline()){
			return 1;
		}
		if(!f.isOnline){
			return -1;
		}
		return this.value-f.getValue()>0?-1:1;
	}
	public int getStationType() {
		return stationType;
	}
	public void setStationType(int stationType) {
		this.stationType = stationType;
	}
	public boolean isOnline() {
		return isOnline;
	}
	public void setOnline(boolean isOnline) {
		this.isOnline = isOnline;
	}
	
}
