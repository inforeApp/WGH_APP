package com.model;


public class Evidence {
	private double lnt;
	private double lat;
	private String pson;
	private String address;
	private String mn;
	public double getLnt() {
		return lnt;
	}
	public void setLnt(double lnt) {
		this.lnt = lnt;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public String getPson() {
		return pson;
	}
	public void setPson(String pson) {
		this.pson = pson;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMn() {
		return mn;
	}
	public void setMn(String mn) {
		this.mn = mn;
	}
	@Override
	public String toString() {
		return "Evidence [lnt=" + lnt + ", lat=" + lat + ", pson=" + pson
				+ ", address=" + address + ", mn=" + mn + "]";
	}
	
}
