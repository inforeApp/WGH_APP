package com.model;

public class MapInfo {
		private String mn;
		private double PM25;
		private double PM10;
		private double CO;
		private double O3;
		private double SO2;
		private double NO2;
		private String name;
		private double lnt;
		private double lat;
		private double AQI;
		private String time;
		private String address;
		private boolean isOnline;
		private int stationType;
		
		public int getStationType() {
			return stationType;
		}
		public void setStationType(int stationType) {
			this.stationType = stationType;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getMn() {
			return mn;
		}
		public void setMn(String mn) {
			this.mn = mn;
		}
		public double getPM25() {
			return PM25;
		}
		public void setPM25(double pM25) {
			this.PM25 = pM25;
		}
		public double getPM10() {
			return PM10;
		}
		public void setPM10(double pM10) {
			this.PM10 = pM10;
		}
		public double getCO() {
			return CO;
		}
		public void setCO(double cO) {
			this.CO = cO;
		}
		public double getO3() {
			return O3;
		}
		public void setO3(double O3) {
			this.O3 = O3;
		}
		public double getSO2() {
			return SO2;
		}
		public void setSO2(double sO2) {
			this.SO2 = sO2;
		}
		public double getNO2() {
			return NO2;
		}
		public void setNO2(double nO2) {
			this.NO2 = nO2;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public double getLnt() {
			return lnt;
		}
		public void setLnt(double lnt) {
			this.lnt = lnt;
		}
		public double getLat() {
			return lat;
		}
		public void setLat(double lat) {
			this.lat = lat;
		}
		public double getAQI() {
			return AQI;
		}
		public void setAQI(double aQI) {
			this.AQI = aQI;
		}
		public String getTime() {
			return time;
		}
		public void setTime(String time) {
			this.time = time;
		}
		public boolean isOnline() {
			return isOnline;
		}
		public void setOnline(boolean isOnline) {
			this.isOnline = isOnline;
		}
		
}
