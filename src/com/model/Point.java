package com.model;


import java.util.List;

public class Point {
	private String factorName;
	private List<NewPoint> factorList;
	
	public String getFactorName() {
		return factorName;
	}
	public void setFactorName(String factorName) {
		this.factorName = factorName;
	}
	public List<NewPoint> getFactorList() {
		return factorList;
	}
	public void setFactorList(List<NewPoint> factorList) {
		this.factorList = factorList;
	}
//	public int compareTo(Point o) {
////		try {
////			return (FormatUtil.secondG.parse(time+":00:00").getTime()-FormatUtil.secondG.parse(o.getTime()+":00:00").getTime())>0?1:-1;
////		} catch (ParseException e) {
////			e.printStackTrace();
////		}
//		return 0;
//	}
	
}
