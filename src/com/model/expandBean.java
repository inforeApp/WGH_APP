package com.model;

public class expandBean {
	private int AQI;

    private double CO;

    private double NO2;

    private double PM10;

    private double PM25;

    private double SO2;

    private double o3;

    private double temperature;

    private double humidity;

	public int getAQI() {
		return AQI;
	}

	public void setAQI(int aQI) {
		AQI = aQI;
	}

	public double getCO() {
		return CO;
	}

	public void setCO(double cO) {
		CO = cO;
	}

	public double getNO2() {
		return NO2;
	}

	public void setNO2(double nO2) {
		NO2 = nO2;
	}

	public double getPM10() {
		return PM10;
	}

	public void setPM10(double pM10) {
		PM10 = pM10;
	}

	public double getPM25() {
		return PM25;
	}

	public void setPM25(double pM25) {
		PM25 = pM25;
	}

	public double getSO2() {
		return SO2;
	}

	public void setSO2(double sO2) {
		SO2 = sO2;
	}

	public double getO3() {
		return o3;
	}

	public void setO3(double o3) {
		this.o3 = o3;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}
}
