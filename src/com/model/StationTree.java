package com.model;

import java.util.List;


public class StationTree {
	
	private String id;
	
	private String name;
	
	private String mn;
	
	private List<StationTreeSon> stationList;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMn() {
		return mn;
	}
	public void setMn(String mn) {
		this.mn = mn;
	}
	public List<StationTreeSon> getStationList() {
		return stationList;
	}
	public void setStationList(List<StationTreeSon> stationList) {
		this.stationList = stationList;
	}
	
	
	
}
