package com.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JsonConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.service.RealService;
import com.util.ResponseUtils;


@Controller
public class RealController {
	@Autowired
	private RealService realService;
	
	/**
	 * 根据mn号获取实时数据
	 * @param mn
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRealDataByMn.do", method = RequestMethod.POST)
	public void queryRealDataByMn(
			@RequestHeader("currentId") String currentId, 
			@RequestParam(value = "mn") String mn,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryRealDataByMn(currentId,mn);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 获取站点详情
	 * @param mn
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryStationDetails.do", method = RequestMethod.POST)
	public void queryStationDetails(
			@RequestParam(value = "mn") String mn,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryStationDetails(mn);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 获取实时数据排名
	 * @param factorType
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRealDataRank.do", method = RequestMethod.POST)
	public void queryRealDataRank(
			@RequestHeader(value="currentId") String userId,
			@RequestParam(value = "factorType") String factorType,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryRealDataRank(factorType,userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 查询预警数据
	 * @param userId
	 * @param warningLevel
	 * @param warningLevel_1
	 * @param warningLevel_2
	 * @param warningLevel_3
	 * @param warningLevel_4
	 * @param keyword
	 * @param type
	 * @param pageNum
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/searchRealWarning.do", method = RequestMethod.POST)
	public void searchRealWarning(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "warningLevel",required=false) String warningLevel,
			@RequestParam(value = "warningLevel_1") String warningLevel_1,
			@RequestParam(value = "warningLevel_2") String warningLevel_2,
			@RequestParam(value = "warningLevel_3") String warningLevel_3,
			@RequestParam(value = "warningLevel_4") String warningLevel_4,
			@RequestParam(value = "keyword",required=false) String keyword,
			@RequestParam(value = "type",required=false) String type,
			@RequestParam(value = "pageNum") String pageNum,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.searchRealWarning(userId, warningLevel,
				keyword, type, pageNum, warningLevel_1,
				warningLevel_2, warningLevel_3,
				warningLevel_4);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 实时报警
	 * @param userId
	 * @param warningLevel
	 * @param keyword
	 * @param type
	 * @param pageNum
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRealWarning.do", method = RequestMethod.POST)
	public void queryRealWarning(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "warningLevel") String warningLevel,
			@RequestParam(value = "keyword",required=false) String keyword,
			@RequestParam(value = "type",required=false) String type,
			@RequestParam(value = "pageNum") String pageNum,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryRealWarning(userId, warningLevel,
				keyword, type, pageNum);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 根据mn号获取图片
	 * @param userId
	 * @param mn
	 * @param time
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryPhotoByMn.do", method = RequestMethod.POST)
	public void queryPhotoByMn(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "mn") String mn,
			@RequestParam(value = "time") String time,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryPhotoByMn(userId,mn,time);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 根据mn号获取最近24小时数据
	 * @param userId
	 * @param mn
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRecentlyByMn.do", method = RequestMethod.POST)
	public void queryRecentlyByMn(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "mn") String mn,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryRecentlyByMn(userId,mn);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 获取实时调度AQI数据
	 * @param userId
	 * @param dataType
	 * @param pageNum
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRealDispatchByAQI.do", method = RequestMethod.POST)
	public void queryRealDispatchByAQI(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "dataType") String dataType,
			@RequestParam(value = "pageNum") String pageNum,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryRealDispatchByAQI(userId,dataType,pageNum);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 获取实时调度综合数据
	 * @param userId
	 * @param dataType
	 * @param pageNum
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRealDispatchBySynthesis.do", method = RequestMethod.POST)
	public void queryRealDispatchBySynthesis(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "dataType") String dataType,
			@RequestParam(value = "pageNum") String pageNum,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryRealDispatchBySynthesis(userId,dataType,pageNum);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 获取实时调度气象数据
	 * @param userId
	 * @param dataType
	 * @param pageNum
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRealDispatchByWeather.do", method = RequestMethod.POST)
	public void queryRealDispatchByWeather(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "dataType") String dataType,
			@RequestParam(value = "pageNum") String pageNum,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = realService.queryRealDispatchByWeather(userId,dataType,pageNum);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
}
