package com.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JsonConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.service.WorkService;
import com.util.HttpUtil;
import com.util.ResponseUtils;

@Controller
public class WorkController {
	@Autowired
	private WorkService workService;
	/**
	 * 新建问题报告
	 * @param quertionTitle
	 * @param quertionNum
	 * @param releaseTime
	 * @param gridNum
	 * @param questionAddress
	 * @param questionContent
	 * @param userId
	 * @param file
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/newQuertionReport.do", method = RequestMethod.POST)
	public void newQuertionReport(
			@RequestParam(value = "quertionTitle") String quertionTitle,
			@RequestParam(value = "quertionNum") String quertionNum,
			@RequestParam(value = "releaseTime") String releaseTime,
			@RequestParam(value = "gridNum") String gridNum,
			@RequestParam(value = "questionAddress") String questionAddress,
			@RequestParam(value = "questionContent") String questionContent,
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "pic", required = false) MultipartFile file[],
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.newQuertionReport(quertionTitle,quertionNum,releaseTime,gridNum,questionAddress,file,questionContent,userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] { "missionList" });
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 获取问题报告列表
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryQuestionReport.do", method = RequestMethod.POST)
	public void queryQuestionReport(
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String userId = request.getHeader("userId");
		Object obj = workService.queryQuestionReport(userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 查询排污许可证
	 * @param state
	 * @param keyword
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/searchDischargePollution.do", method = RequestMethod.POST)
	public void searchDischargePollution(
			@RequestParam(value = "state") String state,
			@RequestParam(value = "keyword") String keyword,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String userId = request.getHeader("userId");
		Object obj = workService.searchDischargePollution(state,keyword,userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 根据mn号获取污染源
	 * @param mn
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/searchPollutionByMn.do", method = RequestMethod.POST)
	public void searchPollutionByMn(
			@RequestParam(value = "mn") String mn,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.searchPollutionByMn(mn);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 根据mn号获取排污许可证
	 * @param mn
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/searchDischargePollutionByMn.do", method = RequestMethod.POST)
	public void searchDischargePollutionByMn(
			@RequestParam(value = "mn") String mn,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.searchDischargePollutionByMn(mn);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	@RequestMapping(value = "/servlet/searchPollution.do", method = RequestMethod.POST)
	public void searchPollution(
			@RequestParam(value = "state") String state,
			@RequestParam(value = "keyword") String keyword,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String userId = request.getHeader("userId");
		Object obj = workService.searchPollution(keyword,state,userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 新建执法任务
	 * @param mn
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/newMission.do", method = RequestMethod.POST)
	public void newMission(
			@RequestParam(value = "title",required=false) String title,
			@RequestParam(value = "mn",required=false) String mn,
			@RequestParam(value = "leaderId",required=false) String leaderId,
			@RequestParam(value = "issuePsonId",required=false) String issuePsonId,
			@RequestParam(value = "issueTime",required=false) String issueTime,
			@RequestParam(value = "lastTime",required=false) String lastTime,
			@RequestParam(value = "handlePsonId",required=false) String handlePsonId,
			@RequestParam(value = "discrib",required=false) String discrib,
			@RequestParam(value = "isApproved",required=false) String isApproved,
			@RequestParam(value = "approvePson",required=false) String approvePson,
			@RequestParam(value = "voice",required=false) MultipartFile voice[],
			@RequestParam(value = "file",required=false) MultipartFile file[],
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.newMission(title,mn,issuePsonId,issueTime,lastTime,handlePsonId,discrib,isApproved,approvePson,voice,file,leaderId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 获取执法任务列表
	 * @param userId
	 * @param type
	 * @param keyword
	 * @param state
	 * @param startTime
	 * @param endTime
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryGtasks.do", method = RequestMethod.POST)
	public void queryGtasks(
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "type") String type,
			@RequestParam(value = "keyword",required=false) String keyword,
			@RequestParam(value = "state",required=false) String state,
			@RequestParam(value = "startTime",required=false) String startTime,
			@RequestParam(value = "endTime",required=false) String endTime,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryGtasks(userId,type,keyword,state,startTime,endTime);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 处理任务
	 * @param monitor
	 * @param SurveyAsk
	 * @param Exploration
	 * @param obtainEvidence
	 * @param qySignature
	 * @param zfSignature
	 * @param missionId
	 * @param userId
	 * @param mn
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/handleMission.do", method = RequestMethod.POST)
	public void handleMission(
			@RequestParam(value = "RecorderJC",required=false) String RecorderJC,
			@RequestParam(value = "NotesJC",required=false) String NotesJC,
			@RequestParam(value = "NotesAsk",required=false) String NotesAsk,
			@RequestParam(value = "obtainEvidence",required=false) String obtainEvidence,
			@RequestParam(value = "qySignature",required=false)  MultipartFile qySignature[],
			@RequestParam(value = "zfSignature",required=false)  MultipartFile zfSignature[],
			@RequestParam(value = "files",required=false)  MultipartFile files[],
			@RequestParam(value = "missionId",required=false) String missionId,
			@RequestParam(value = "userId",required=false) String userId,
			@RequestParam(value = "mn",required=false) String mn,
			@RequestParam(value = "lat",required=false) String lat,
			@RequestParam(value = "lng",required=false) String lng,
			@RequestParam(value = "address",required=false) String address,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.ResearchAskRecord(RecorderJC,NotesJC,NotesAsk,obtainEvidence,qySignature,zfSignature,missionId,userId,mn,files,lat,lng,address);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 执法取证
	 * @param lat
	 * @param lnt
	 * @param address
	 * @param pson
	 * @param mn
	 * @param pic
	 * @param voice
	 * @param video
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/SceneEvidence", method = RequestMethod.POST)
	public void SceneEvidence(
			@RequestParam(value = "lat") String lat,
			@RequestParam(value = "lnt") String lnt,
			@RequestParam(value = "address") String address,
			@RequestParam(value = "pson") String pson,
			@RequestParam(value = "mn") String mn,
			@RequestParam(value = "pic", required = false) MultipartFile pic[],
			@RequestParam(value = "voice", required = false) MultipartFile voice[],
			@RequestParam(value = "video", required = false) MultipartFile video[],
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.SceneEvidence(lat,lnt,address,pson,mn,pic,voice,video);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 修改任务状态
	 * @param mission_id
	 * @param userId
	 * @param describ
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/changeMissionStatus", method = RequestMethod.POST)
	public void changeMissionStatus(
			@RequestParam(value = "missionId") String mission_id,
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "describ") String describ,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.changeMissionStatus(mission_id,userId,describ);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	@RequestMapping(value = "/servlet/checkMission", method = RequestMethod.POST)
	public void checkMission(
			@RequestParam(value = "missionId") String mission_id,
			@RequestParam(value = "state") String state,
			@RequestParam(value = "describ") String describ,
			@RequestParam(value = "time",required=false) String time,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.checkMission(mission_id,state,describ,time);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 现场监察记录
	 * @param mission_id
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/querySupervise", method = RequestMethod.POST)
	public void querySupervise(
			@RequestParam(value = "id") String id,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.querySupervise(id);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 调查询问笔录
	 * @param mission_id
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/querySurvey", method = RequestMethod.POST)
	public void querySurvey(
			@RequestParam(value = "missionId") String mission_id,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryProcess(mission_id);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	@RequestMapping(value = "/servlet/queryProspect", method = RequestMethod.POST)
	public void queryProspect(
			@RequestParam(value = "missionId") String mission_id,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryProcess(mission_id);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 执法取证
	 * @param mission_id
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryEvidence", method = RequestMethod.POST)
	public void queryEvidence(
			@RequestParam(value = "missionId") String mission_id,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryProcess(mission_id);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	@RequestMapping(value = "/servlet/queryProcess", method = RequestMethod.POST)
	public void queryProcess(
			@RequestParam(value = "missionId") String mission_id,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryProcess(mission_id);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {"enforcementSignature","companySignature"});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	@RequestMapping(value = "/servlet/queryLedger", method = RequestMethod.POST)
	public void queryLedger(
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "pageNum", required = false) String pageNum,
			@RequestParam(value = "keyWord", required = false) String keyword,
			@RequestParam(value = "mn", required = false) String mn,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryLedger(userId,pageNum,mn,keyword);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {"enforcementSignature","companySignature"});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	@RequestMapping(value = "/servlet/queryLedgerById", method = RequestMethod.POST)
	public void queryLedgerById(
			@RequestParam(value = "userId", required = false) String userId,
			@RequestParam(value = "ledgerId", required = false) String ledgerId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryLedgerById(userId,ledgerId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {"enforcementSignature","companySignature"});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	@RequestMapping(value = "/servlet/queryDetailsById", method = RequestMethod.POST)
	public void queryDetailsById(
			@RequestParam(value = "id") String id,
			@RequestParam(value = "state") String state,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryDetailsById(id,state);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	
	@RequestMapping(value = "/servlet/queryCheckMission", method = RequestMethod.POST)
	public void queryCheckMission(
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "state" ,required=false) String state,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryCheckMission(userId,state);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	
	@RequestMapping(value = "/servlet/deleteMission", method = RequestMethod.POST)
	public void deleteMission(
			@RequestParam(value = "userId") String userId,
			@RequestParam(value = "missionId") String missionId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.deleteMission(userId,missionId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	@RequestMapping(value = "/servlet/newRecord", method = RequestMethod.POST)
	public void newRecord(
			@RequestParam(value = "happenTime") String happenTime,
			@RequestParam(value = "recordTitle") String recordTitle,
			@RequestParam(value = "mn") String mn,
			@RequestParam(value = "dangerLevel") String dangerLevel,
			@RequestParam(value = "factor") String factor,
			@RequestParam(value = "describ") String describ,
			@RequestParam(value = "dispatcher") String dispatcher,
			@RequestHeader(value="currentId") String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		System.out.println("newRecord");
		Object obj = workService.newRecord(userId,happenTime,mn,dangerLevel,factor,describ,dispatcher,recordTitle);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 获取事件列表
	 * @param userId
	 * @param dataType
	 * @param pageNum
	 * @param recordType
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRecordList", method = RequestMethod.POST)
	public void queryRecordList(
			@RequestHeader(value="currentId") String userId,
			@RequestParam(value = "dataType") String dataType,
			@RequestParam(value = "pageNum") String pageNum,
			@RequestParam(value = "recordType", required = false) String recordType,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryRecordList(userId,dataType,pageNum,recordType);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 获取事件详情
	 * @param userId
	 * @param recordId
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRecordDetails", method = RequestMethod.POST)
	public void queryRecordDetails(
			@RequestHeader(value="currentId") String userId,
			@RequestParam(value = "recordId") String recordId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		System.out.println(HttpUtil.getVisitorIp(request));
		Object obj = workService.queryRecordDetails(userId,recordId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {"PICTURE_PATH"});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 事件确认
	 * @param recordId
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRecordVerify", method = RequestMethod.POST)
	public void queryRecordVerify(
			@RequestHeader(value="currentId") String userId,
			@RequestParam(value = "recordId") String recordId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.queryRecordVerify(userId,recordId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {"picture_path"});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	@RequestMapping(value = "/servlet/handleRecord", method = RequestMethod.POST)
	public void handleRecord(
			@RequestParam(value = "recordId") String recordId,
			@RequestParam(value = "handleDescrib") String handleDescrib,
			@RequestParam(value = "file",required = false) MultipartFile[] files,
			@RequestParam(value = "lat") String lat,
			@RequestParam(value = "lnt") String lnt,
			@RequestParam(value = "address") String address,
			@RequestHeader(value="currentId") String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.handleRecord(userId,recordId,handleDescrib,files,lat,lnt,address);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	@RequestMapping(value = "/servlet/deleteRecord", method = RequestMethod.POST)
	public void deleteRecord(
			@RequestParam(value = "recordId") String recordId,
			@RequestHeader(value="currentId") String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.deleteRecord(userId,recordId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {"picture_path"});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	@RequestMapping(value = "/servlet/updateRecord", method = RequestMethod.POST)
	public void updateRecord(
			@RequestParam(value = "happenTime") String happenTime,
//			@RequestParam(value = "deadLine") String deadLine,
			@RequestParam(value = "mn") String mn,
			@RequestParam(value = "dangerLevel") String dangerLevel,
			@RequestParam(value = "factor") String factor,
			@RequestParam(value = "describ") String describ,
			@RequestParam(value = "dispatcher") String dispatcher,
			@RequestParam(value = "recordId") String recordId,
			@RequestParam(value = "recordTitle") String recordTitle,
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		System.out.println("updateRecord");
		String deadLine=null;
		Object obj = workService.updateRecord(recordId,userId,happenTime,mn,dangerLevel,factor,describ,dispatcher,deadLine,recordTitle);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	@RequestMapping(value = "/servlet/handleRecordList", method = RequestMethod.POST)
	public void handleRecordList(
			@RequestParam(value = "pageNum") String pageNum,
			@RequestHeader(value="currentId") String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.handleRecordList(userId,pageNum);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	
	@RequestMapping(value = "/servlet/sreachRecordList", method = RequestMethod.POST)
	public void sreachRecordList(
			@RequestParam(value = "pageNum") String pageNum,
			@RequestParam(value = "keyword") String keyword,
			@RequestParam(value = "dataType") String dataType,
			@RequestHeader(value="currentId") String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.sreachRecordList(userId,pageNum,keyword,dataType);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	@RequestMapping(value = "/servlet/sreachHandleRecordList", method = RequestMethod.POST)
	public void sreachHandleRecordList(
			@RequestParam(value = "pageNum") String pageNum,
			@RequestParam(value = "keyword") String keyword,
			@RequestHeader(value="currentId") String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = workService.sreachHandleRecordList(userId,pageNum,keyword);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
}


