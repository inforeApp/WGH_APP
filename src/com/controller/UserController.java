package com.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JsonConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.User;
import com.service.UserService;
import com.util.ResponseUtils;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	/**
	 * 获取新站点树
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryNewStationTree.do", method = RequestMethod.POST)
	public void queryNewStationTree(
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Object obj = userService.queryStationTree(userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] { "missionList","isUse" });
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 获取新站点树
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryaStationTree.do", method = RequestMethod.POST)
	public void queryStationTree(
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Object obj = userService.queryStationTree(userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] { "missionList","isUse" });
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 用户登录
	 * 
	 * @param password
	 * @param account
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/login.do", method = RequestMethod.POST)
	public void login(@RequestParam(value = "password") String password,
			@RequestParam(value = "account") String account,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		User user = new User();
		user.setPassword(password);
		user.setAccount(account);
		Object obj = userService.loginCheck(user);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] { "missionList" });
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 获取所有人员
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryAllPson.do", method = RequestMethod.POST)
	public void queryAllPson(
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Object obj = userService.findAllUser(userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] { "missionList","token","password","account" });
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 获取人员位置
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryPersonPlace.do", method = RequestMethod.POST)
	public void queryPersonPlace(
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Object obj = userService.queryPersonPlace(userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {"password","token","missionList","account"});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 获取人员地理位置
	 * 
	 * @param lnt
	 * @param lat
	 * @param userId
	 * @param address
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryPlace.do", method = RequestMethod.POST)
	public void queryPlace(@RequestParam(value = "lnt") String lnt,
			@RequestParam(value = "lat") String lat,
			@RequestParam(value = "address") String address,
			@RequestParam(value = "area", required = false) String area,
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		if(!"19b01040b8d24662935f9534feb7e50f".equals(userId)){
		Object obj = userService.queryPlace(lnt, lat, userId, address,area);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
		}
	}

	/**
	 * 获取污染源
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryPollutionStation.do", method = RequestMethod.POST)
	public void queryPollutionStation(
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Object obj = userService.queryPollutionStation(userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 获取网格
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryGrid.do")
	public void queryGrid(
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Object obj = userService.queryGrid(userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 获取视频Token
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/getEZAccessToken.do", method = RequestMethod.POST)
	public void getAccessToken(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Object obj = userService.getAccessToken();
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] { });
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 获取实时数据
	 * 
	 * @param factorType
	 * @param stationType
	 * @param userId
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryRealInfoRank.do", method = RequestMethod.POST)
	public void queryRealInfoRank(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "factorType", required = false) String factorType,
			@RequestParam(value = "stationType", required = false) String stationType,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = userService.queryRealInfoRank(factorType, stationType,
				userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	@RequestMapping(value = "/servlet/queryTrack.do", method = RequestMethod.POST)
	public void queryTrack(
			@RequestParam(value = "time", required = false) String time,
			@RequestParam(value = "userId", required = false) String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = userService.queryTrack(time,
				userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	@RequestMapping(value = "/servlet/queryRecentlyData.do", method = RequestMethod.POST)
	public void queryRecentlyData(
			@RequestParam(value = "mn", required = false) String mn,
			@RequestParam(value = "userId", required = false) String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = userService.queryRecentlyData(mn);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
}
