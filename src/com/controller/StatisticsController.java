package com.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JsonConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.service.StatisticsService;
import com.util.ResponseUtils;

@Controller
public class StatisticsController {
	@Autowired
	private StatisticsService statisticsService;

	/**
	 * 获取同比环比
	 * @param time
	 * @param stationString
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryContrastAnalyze.do", method = RequestMethod.POST)
	public void queryContrastAnalyze(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "time", required = false) String time,
			@RequestParam(value = "dataType", required = false) String dataType,
			@RequestParam(value = "stationString", required = false) String stationString,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
//		Object obj = statisticsService.queryContrastAnalyze("2017-06",
//				"66666660000002,mn20170307135531",userId);
		Object obj = statisticsService.queryContrastAnalyze(time,
				stationString,userId,dataType);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

	/**
	 * 获取查询点位分析
	 * @param dataType
	 * @param startTime
	 * @param endTime
	 * @param stationString
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/querySearchPointAnalyze.do", method = RequestMethod.POST)
	public void querySearchPointAnalyze(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "dataType") String dataType,
			@RequestParam(value = "startTime") String startTime,
			@RequestParam(value = "endTime") String endTime,
			@RequestParam(value = "stationString",required=false) String stationString,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = statisticsService.querySearchPointAnalyze(dataType, startTime, endTime,stationString,userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	@RequestMapping(value = "/servlet/queryDataAnalysis.do", method = RequestMethod.POST)
	public void queryDataAnalysis(
			@RequestParam(value = "dataType") String dataType,
			@RequestParam(value = "startTime") String startTime,
			@RequestParam(value = "endTime") String endTime,
			@RequestParam(value = "stationString",required=false) String stationString,
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = statisticsService.queryDataAnalysis(dataType, startTime, endTime,stationString,userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 获取点位分析
	 * @param factorType
	 * @param time
	 * @param stationString
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryPointAnalyze.do", method = RequestMethod.POST)
	public void queryPointAnalyze(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "factorType") String factorType,
			@RequestParam(value = "time") String time,
			@RequestParam(value = "stationString",required=false) String stationString,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = statisticsService.queryPointAnalyze(factorType, time,stationString,userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 历史数据排名
	 * @param factorType
	 * @param time
	 * @param historytype
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryHisDataRank.do", method = RequestMethod.POST)
	public void queryHisDataRank(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "factorType") String factorType,
			@RequestParam(value = "time") String time,
			@RequestParam(value = "historytype") String historytype,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = statisticsService.queryHisDataRank(factorType, time,historytype,userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	/**
	 * 获取站点树
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryStationTree.do", method = RequestMethod.POST)
	public void queryStationTree(
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = statisticsService.queryStationTree(userId);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	
	@RequestMapping(value = "/servlet/queryPointData.do", method = RequestMethod.POST)
	public void queryPointData(
			@RequestParam(value = "dataType") String dataType,
			@RequestParam(value = "modelType",required=false) String modelType,
			@RequestParam(value = "pageNum") String pageNum,
			@RequestHeader(value="currentId",required=false) String userId,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = statisticsService.queryPointData(dataType,userId,pageNum,modelType);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	
	@RequestMapping(value = "/servlet/queryWarningData.do", method = RequestMethod.POST)
	public void queryWarningData(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "startTime") String startTime,
			@RequestParam(value = "endTime") String endTime,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = statisticsService.queryWarningData(userId,startTime,endTime);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}
	/**
	 * 获取热力分析
	 * @param userId
	 * @param mn
	 * @param time
	 * @param factorType
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/servlet/queryHeatData.do", method = RequestMethod.POST)
	public void queryHeatData(
			@RequestHeader(value="currentId",required=false) String userId,
			@RequestParam(value = "mn",required=false) String mn,
			@RequestParam(value = "time") String time,
			@RequestParam(value = "factorType") String factorType,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Object obj = statisticsService.queryHeatData(userId,mn,time,factorType);
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[] {});
		ResponseUtils.outputJson(response, obj, jsonConfig);
	}

}
